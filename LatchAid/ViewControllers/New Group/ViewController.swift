//
//  ViewController.swift

import UIKit
import FirebaseInstanceID
import FirebaseMessaging
class ViewController: UIViewController {
    
    
    @IBOutlet var objUserViewModel: UserViewModel!
    @IBOutlet weak fileprivate var emailTF:UITextField!
    @IBOutlet weak fileprivate var passwordTF:UITextField!
    
    @IBOutlet var keepLoginBtn: UIButton!
    @IBOutlet weak var googleSingInBTN: CustomButton!
    
    @IBOutlet weak var facebookSignInBtn: CustomButton!
    var messageStr = ""
    @IBOutlet weak var submitBTN: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.Default(boolForKey: "keepLoggin") == true
        {
            self.keepLoginBtn.setImage(UIImage(named: "check_1_a"), for: .normal)
            self.emailTF.text = UserDefaults.Default(valueForKey: "email") as? String
            self.passwordTF.text = UserDefaults.Default(valueForKey: "password") as? String
        }
        
        self.view.backgroundColor = CustomColor.ViewColor
        self.submitBTN.backgroundColor = CustomColor.buttonColor1
        self.submitBTN.tintColor = .white
        
        let existingUser = userModel?.existingUser ?? false
        if existingUser{
            AppDelegate.sharedDelegate.setUpMainController()
        }
        
    }
    
    
    @IBAction func textPrimaryFunctio(_ sender: UITextField) {
        if sender == emailTF {
            passwordTF.becomeFirstResponder()
        }else{
            sender.resignFirstResponder()
            if sender.text?.isEmpty == false
            {
                self.submitBTN.sendActions(for: .touchUpInside)
            }
        }
    }
    
}

//MARK:- Actions
extension ViewController{
    @IBAction func backBtn(_ sender: UIButton) {
        //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func keepLogginBtn(_ sender: UIButton) {
        
        if sender.currentImage == UIImage(named: "check_1")
        {
            sender.setImage(UIImage(named: "check_1_a"), for: .normal)
            UserDefaults.Default(setValue: self.emailTF.text ?? "", forKey: "email")
            UserDefaults.Default(setValue: self.passwordTF.text ?? "", forKey: "password")
            UserDefaults.Default(setValue: true, forKey: "keepLoggin")
        }
        else{
            sender.setImage(UIImage(named: "check_1"), for: .normal)
            UserDefaults.Default(setValue:"", forKey: "email")
            UserDefaults.Default(setValue:"", forKey: "password")
            UserDefaults.Default(setValue: false, forKey: "keepLoggin")
            
        }
    }
    
    //MARK:- Native Login Action
    @IBAction fileprivate func onClickLogin(_ sender: CustomButton) {
        
        var messageStr = ""
        if (emailTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kEmailEmpty)
            
        }else if (passwordTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kPasswordEmpty)
            
        }
        else {
            objUserViewModel.nativeLogin(email:emailTF.text!,password:passwordTF.text!, success: { (message) in
                
                messageStr = message ?? ""
                guard let existUser =  userModel?.existingUser else{return}
                if !existUser
                {
                    self.showAlert(message: message)
                    let storyboard = UIStoryboard.init(name:StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity
                        .KSignUp) as? SignUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
                else
                {
                   
                 
                    AppDelegate.sharedDelegate.setUpMainController()
                }
                
            }, failure:{ (message) in
                self.showAlert(message: message)
            })
        }
    }
    
    
    
    //MARK:- Facebook Login Action
    @IBAction fileprivate func onClickFacebook(_ sender: CustomButton) {
        FacebookManager.shared.facebookLogin(withController:self) { (success,user) in
          // guard let deviceToken = ldDeviceToken else{return}
            let params:[String:Any] = ["firstName":user.firstName ?? "","lastName":user.lastName ?? "","email":user.email ?? "","socialId":user.id ?? "","profilePic":user.profilePic ?? "","gender":"","userName":"\(user.firstName ?? "") " + " \(user.lastName ?? "") ","socialType":"google","deviceToken":ldDeviceToken]
            
            self.objUserViewModel.facebookLogin(params:params, success: { (message) in
                self.messageStr = message ?? ""
                DispatchQueue.main.async {
                    
                    guard let existUser =  userModel?.existingUser else{return}
                    if !existUser
                    {
                        self.showAlert(message: self.messageStr)
                        let storyboard = UIStoryboard.init(name:StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity
                            .KSignUp) as? SignUpVC
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    else
                    {
                      
                        AppDelegate.sharedDelegate.setUpMainController()
                    }
                    
                }
            }, failure: { (message) in
                self.showAlert(message: message)
            })
        }
    }
    
    //MARK:- Google Login Action
    @IBAction fileprivate func onClickGoogle(_ sender: CustomButton) {
        GoogleLoginManager.shared.GIDSignInUI(from: self, onSuccess: { (user) in
           
            var firstName:String = ""
            var lastName:String = ""
            
            let name = user.name
            let whiteSpace = " "
            if let hasWhiteSpace = name?.contains(whiteSpace) {
                var fullNameArr =  name?.components(separatedBy: " ")
                firstName = fullNameArr?[0] ?? ""
                lastName  = fullNameArr?[1] ?? ""
                print ("has whitespace")
            } else {
                firstName = name ?? ""
                lastName = ""
            }
            let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"fullName":user.givenName ?? "","email":user.email ?? "","socialId":user.userGmailID ?? "","profilePic":user.hasImage ?? "","gender":"","socialType":"google","deviceToken":ldDeviceToken]
            
            
            self.objUserViewModel.googleLogin(params:params, success: { (message) in
                self.messageStr = message ?? ""
                guard let existUser =  userModel?.existingUser else{return}
                if !existUser
                {
                    self.showAlert(message: self.messageStr)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentity
                        .KSignUp) as? SignUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
                else
                {
                    FIRMessaging.messaging().subscribe(toTopic: "latchaid_\(userModel?.userId ?? "")")
                    AppDelegate.sharedDelegate.setUpMainController()
                }
                
                
            }, failure: { (message) in
                self.showAlert(message: message)
            })
            
        }, onCancel: { (isCancelled) in
            
        }, onFailure: { (error) in
            
        })
        
    }
}
