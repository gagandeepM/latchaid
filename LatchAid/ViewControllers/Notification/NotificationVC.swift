//
//  NotificationVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 29/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    @IBOutlet var objNotificationViewModel: NotificationViewModel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var onjNotificationViewModel: NotificationViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        //getNotification()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
      
        getNotification()
    }
    
    override func viewDidLayoutSubviews() {
     
    }
    
    func getNotification()
    {
        objNotificationViewModel.getNotfication { (status) in
            if status{
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.tableView.beginUpdates()
                    self.tableView.endUpdates()
                }
            }
        }
        
    }

}
extension NotificationVC:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       // return self.objNotificationViewModel.numberOfRow()
        
        
        var numOfSection: NSInteger = 0
        
        if self.objNotificationViewModel.numberOfRow() > 0 {
            
            self.tableView.backgroundView = nil
            numOfSection = self.objNotificationViewModel.numberOfRow()
              self.tableView.separatorStyle = .singleLine
            
        } else {
            
            
            var noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: self.tableView.bounds.size.height))
            noDataLabel.text = "No Data Available"
            noDataLabel.textColor = UIColor.gray
            noDataLabel.font = UIFont(name: "SF Pro Display", size: 24)
            //UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
            noDataLabel.textAlignment = NSTextAlignment.center
            self.tableView.backgroundView = noDataLabel
            self.tableView.separatorStyle = .none
            
        }
        return numOfSection
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
        cell.obj = self.objNotificationViewModel.cellRowAt(indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let channelId = self.objNotificationViewModel.didSelectForRowAt(at: indexPath)
        
        guard let tabbarController  = rootController as? UITabBarController , let navigationController = tabbarController.selectedViewController as? UINavigationController, let visibleViewController = navigationController.visibleViewController else {
            return  }
        
        tabbarController.selectedIndex = 2
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            
            if let navigationController1 = tabbarController.selectedViewController as? UINavigationController
            {
                
                if let rootVC = navigationController1.visibleViewController as? ChatVC
                {
                    if  rootVC.channelId  == channelId
                    {
                        rootVC.isFromNotification = true
                        rootVC.isWithNoCopyForward = true
                        rootVC.channelId  = channelId
                        rootVC.setUI()
                    }else{
                        rootVC.isFromNotification = true
                        rootVC.isWithNoCopyForward = true
                        rootVC.channelId  = channelId
                        rootVC.viewWillAppear(true)
                    }
                }else{
                    
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                    vc?.channelId = channelId
                    vc?.isFromNotification = true
                    vc?.isWithNoCopyForward = true
                    vc?.setUI()
                    navigationController1.pushViewController(vc!, animated: true)
                }
            }
        }
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = self.objNotificationViewModel.cellRowAt(indexPath)
        
        if data.count != 0 {
            return 100.0
        }else{
            return 100.0
        }
       // return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let label = UILabel()
        label.textColor = .black
        label.text = "You don't have any notifications right now"
        headerView.addSubview(label)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if objNotificationViewModel.numberOfRow() == 0
        {
            return 0.0
        }
        
        return 40.0
    }
}
