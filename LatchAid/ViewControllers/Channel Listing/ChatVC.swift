//
//  ChatVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 16/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import IQKeyboardManagerSwift
class ChatVC: UIViewController,AVAudioRecorderDelegate,UITextViewDelegate,CustomMenuVCDelegate,UIPopoverPresentationControllerDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var channelName_lbl: UILabel!
    @IBOutlet weak var activeUserDetails_lbl: UILabel!
    @IBOutlet weak var audioStartStopBtn: UIButton!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var chatText: UITextView!
    @IBOutlet weak var memberDetailBtn: UIButton!
    @IBOutlet var objChannelList: ChannelsListViewModel!
    @IBOutlet weak var audioView: UIView!
    fileprivate var objChatVIewModel = ChatViewModel.shared
    @IBOutlet weak var joinBtn: CustomButton!
    
    @IBOutlet weak var longPressView: UIView!
    
    @IBOutlet weak var longPressMessageSelectedCount: UILabel!
    @IBOutlet var longGesture: UILongPressGestureRecognizer!
    @IBOutlet weak var replyView: CardView!
    @IBOutlet weak var replierNameLbl: UILabel!
    @IBOutlet weak var replierMessageLbl: UILabel!
    
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var replierImageMessageLbl: UIImageView!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var copyBtn: UIButton!
    @IBOutlet weak var forwardBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var memberOnline="",onlineCount="",channelId="",channelName="",typeOfChannel="",channelImage=""
    var  paRams:[String:Any]  = [:]
    var channelID = ""
    var textViewString = ""
    var messages:ChatResponseModel?
    var imageRef = UIImageView()
    var channelData:ChannelsList?{
        didSet{
            DispatchQueue.main.async {
                if  self.isViewLoaded
                {
                   self.setUI()
                }
            }
        }
    }
    var isWithNoCopyForward = false
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var player = AVAudioPlayer()
    var imageURL = ""
    let picker = UIImagePickerController()
    var userOnlineStatus:String = ""
    var index :IndexPath?
    var isLongpressView = false
    var isFromChannelList = false
    var isReplyMessage = false
    var isFromNotification = false
    var selectedCount :Int = 0
    {
        didSet
        {
            if self.selectedCount == 0
            {
                self.longPressView.isHidden = true
                self.objChatVIewModel.unselectAll()
            }
            else if self.selectedCount == 1 {
                self.longPressView.isHidden = false
               self.longPressMessageSelectedCount.text = "\(self.selectedCount)"
            }
            else{
                self.longPressView.isHidden = false
                self.replyView.isHidden = true
             self.longPressMessageSelectedCount.text = "\(self.selectedCount)"
            }
        }
    }
    var isKeyboardOpen = false
    var isMicPressed = false{
        didSet{
            let icon = isMicPressed ?  #imageLiteral(resourceName: "mic"):#imageLiteral(resourceName: "sent")
            self.sendBtn.setImage(icon, for: .normal)
        }
    }
    var userExsist = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        memberInfo = false
        self.chatText.resignFirstResponder()
        self.setChannalUI()
      
        self.tableView.reloadData()
        SocketIOManager.sharedInstance.socketConnect()
        self.setUIOnExitChannel()
        self.getChannaldetails()
        //remove becouse join alert is display number of time
        self.setUI()
        //self.socketUpdateChanelChat()
        self.longPressCloseOutletBtn.sendActions(for: .touchUpInside)
        
    }
   
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //
        Audio.shareInst.table = self.tableView
        self.chatText.enablesReturnKeyAutomatically = true
        if paRams.keys.count>0{
            sendSocketMsg(params: paRams)
        }
   
        self.joinBtn.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackGroud), name: UIApplication.willResignActiveNotification, object: nil)

    }
    @objc func applicationDidBecomeActive() {
        //self.viewWillAppear(true)
        
        memberInfo = false
        self.setChannalUI()
        self.objChatVIewModel.clearData()
        self.tableView.reloadData()
        SocketIOManager.sharedInstance.socketConnect()
        self.setUIOnExitChannel()
        self.getChannaldetails()
        self.setUI()
        self.longPressCloseOutletBtn.sendActions(for: .touchUpInside)
    }
    
    @objc func applicationDidEnterBackGroud() {
       // self.viewWillDisappear(true)
        Audio.shareInst.isPlayingPrev()
        self.chatText.resignFirstResponder()

        SocketIOManager.sharedInstance.socket.off(LactchEvents.receiveMessage.rawValue)
        let channelDetail = ["token":userModel?.accessToken ?? "","channelId":channelData?.channelId ?? "","userId":userModel?.userId ?? ""]
        SocketIOManager.sharedInstance.socketDisconnect(data: channelDetail)
    }
    

    
    
    override func viewWillDisappear(_ animated: Bool) {
        //if got any crash the remove next line
        self.chatText.resignFirstResponder()
        Audio.shareInst.isPlayingPrev()
            guard  memberInfo == false else {
            return
        }
        
        SocketIOManager.sharedInstance.socket.off(LactchEvents.receiveMessage.rawValue)
        let channelDetail = ["token":userModel?.accessToken ?? "","channelId":channelData?.channelId ?? "","userId":userModel?.userId ?? ""]
        SocketIOManager.sharedInstance.socketDisconnect(data: channelDetail)
        NotificationCenter.default.removeObserver(self)
     
    }
    
    //set view UI with textView
    func setChannalUI(){
        
        self.replyView.isHidden = true
        self.audioView.isHidden = true
        self.chatText.delegate = self
        self.chatText.text = "Message..."
        self.chatText.textColor = UIColor.lightGray
        self.chatText.autocorrectionType = .yes
        self.isMicPressed = true
        
        // *** Listen for keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        longGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressGesture(sender:)))
        longGesture.minimumPressDuration = 0.5 // optional
        longGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longGesture)
        self.tableView.estimatedRowHeight = 200
        self.tableView.rowHeight = UITableView.automaticDimension

    }
    
    
    // get fresh itrative messages from socket
    func socketUpdateChanelChat(){
        SocketIOManager.sharedInstance.socket.off(LactchEvents.receiveMessage.rawValue)
        SocketIOManager.sharedInstance.getMessage(event: .receiveMessage){ (messages) in
            DispatchQueue.main.async {
                self.lastSeenMsg(msg:messages)
                guard !self.objChatVIewModel.chatMessagelist.contains(messages)
                    else {
                        return
                }
                print("--------->",messages)
                if messages.messageType == .audio ||  messages.messageType == .media
                {
                    self.chatText.text = "Message..."
                    self.chatText.textColor = .gray
                }
                self.view.isUserInteractionEnabled = true
                self.objChatVIewModel.chatMessagelist.append(messages)
                self.tableView.reloadData()
                self.tableView.scrollToBottomRow()
            }
        }
        SocketIOManager.sharedInstance.socket.off(LactchEvents.deleteMessage.rawValue)
        SocketIOManager.sharedInstance.deleteMsgUpdateList(event: .deleteMessage){ () in
            DispatchQueue.main.async {
                self.isDeletActive = true
                self.getChatAPIServer()
            }
        }
    }
    
    
    //return rest chat server using rest API's
    var isDeletActive = false
    func getChatAPIServer()
    {
  
        //API CALLE HERE
        self.objChatVIewModel.getMessage(channelId: channelId, success: { (message) in
            DispatchQueue.main.async {
                
                Audio.shareInst.isPlayingPrev()
                self.tableView.scrollToBottomRow()
                self.socketUpdateChanelChat()
                
                self.objChatVIewModel.selectedActionType = .none
                
                self.isReplyMessage = false
                self.galleryBtn.isHidden = false
                self.sendBtn.isHidden = false
                self.chatText.isHidden = false
                
                self.replyView.isHidden = true
                self.audioView.isHidden = true
                self.joinBtn.isHidden = true
                
                self.tableView.reloadData()
                self.view.isUserInteractionEnabled = true
                self.lastSeenMsg(msg:  self.objChatVIewModel.chatMessagelist.last)
                guard !self.isDeletActive else{
                    self.isDeletActive=false
                    return
                }
                self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
                self.textViewString = ""




            }
        }, falier: { (message) in
            self.showAlert(message: message)
        })

    }
    
    //last seen send to server
    func lastSeenMsg(msg:MessageModel?)
    {
       // let messageTime = self.objChatVIewModel.chatMessagelist.last?.createdTime ?? ""
       
        var messageTime = ""
        if msg != nil
        {
            messageTime = msg?.createdTime ?? ""
        }else{
             return
        }
        
        let channelDetail = ["token":userModel?.accessToken ?? "","channelId":self.channelId ?? "","userId":userModel?.userId ?? "","createdAt":messageTime]
        print(channelDetail)
      
        SocketIOManager.sharedInstance.getMessageNotification(notification: channelDetail)
        
    }
    //MARK:initial setup
    func setUI()
    {
        setJoinUI()
        if isFromNotification
        {
            
            let channelDetail = ["token":userModel?.accessToken ?? "","channelId":self.channelId,"userId":userModel?.userId ?? ""]
            setUIForChat(channelDetails: channelDetail,channelId:self.channelId )
            self.isFromNotification = false
            
        }
        else{
            let channelDetail = ["token":userModel?.accessToken ?? "","channelId":self.channelId ?? "","userId":userModel?.userId ?? ""]
            self.channelId = self.channelId ?? ""
            setUIForChat(channelDetails: channelDetail,channelId: self.channelId ?? "")
          
        }
        
        
    }
    func setUIForChat(channelDetails:[String:Any],channelId:String)
    {
//        //soket offline
//        SocketIOManager.sharedInstance.socket.off(LactchEvents.receiveMessage.rawValue)
//        SocketIOManager.sharedInstance.socketDisconnect(data: socketchannelDetail)
//        socketchannelDetail = channelDetails
        
        //socket Online
        SocketIOManager.sharedInstance.socketOn(channelData: channelDetails)
        // notify when any user enter or leaev channel
        SocketIOManager.sharedInstance.userOnline { (memberOnline,onlineCount,channelId,channelName,typeOfChannel,channelImage)  in
            DispatchQueue.main.async {
                self.channelId = channelId
                self.userOnlineStatus = "\(onlineCount)"
                self.activeUserDetails_lbl.text = "\(String(describing: memberOnline)) members ,  \(self.userOnlineStatus ?? "") online "
                self.channelName_lbl.text = channelName
                print(self.userOnlineStatus)
            }
        }
    }
    //return User Join status
    func setJoinUI()
    {
        self.objChatVIewModel.getJointStatus(channelId:channelId, success: { (message) in
            DispatchQueue.main.async {
                //user join update socket agen
                self.getChatAPIServer()
                self.joinBtn.isHidden = true
                self.memberDetailBtn.isUserInteractionEnabled = true
                if self.chatText.text.isEmpty || self.chatText.text == "Message..."
                {
                    self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
                }
                else
                {
                    self.sendBtn.setImage(UIImage(named: "sent"), for: .normal)
                }
            }
        }) { (message) in
            DispatchQueue.main.async {
               
                self.objChatVIewModel.clearData()
                self.tableView.reloadData()
                self.joinBtn.isHidden = false
                self.chatText.isHidden = true
                self.sendBtn.isHidden = true
                self.galleryBtn.isHidden = true
                self.memberDetailBtn.isUserInteractionEnabled = false
            }
            
        }
        
    }
    func setUIOnExitChannel()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(channelExit), name: NSNotification.Name("exitChannel"), object: nil)
    }
    //user leave channel with NSNotification Observer
    @objc func channelExit()
    {
        DispatchQueue.main.async {
            self.objChatVIewModel.clearData()
            self.tableView.reloadData()
            self.joinBtn.isHidden = false
            self.memberDetailBtn.isUserInteractionEnabled = false
        }
        
    }

    @objc func onLongPressGesture(sender:UILongPressGestureRecognizer)
    {
        self.isLongpressView = true
        self.longPressView.isHidden = false
        let locationInView = sender.location(in: tableView)
        guard  let indexPath = tableView.indexPathForRow(at: locationInView) else{return}
        self.index = indexPath
        switch sender.state {
        case .began:
            self.objChatVIewModel.multipleSelection(at: indexPath)
            self.selectedCount = self.objChatVIewModel.numberOfSelectedMessage
            
            updateActionView()
            if let cell = tableView.cellForRow(at: indexPath){
                self.didChangeCellSelection(cell: cell, isRowSelcted: self.objChatVIewModel.cellForRowAt(at: indexPath).isRowSelcted)
            }
        case .ended:

            updateActionView()
            if let cell = tableView.cellForRow(at: indexPath){
                self.didChangeCellSelection(cell: cell, isRowSelcted: self.objChatVIewModel.cellForRowAt(at: indexPath).isRowSelcted)
            }

        default:break
        }

       
    }
    
    
   
    @IBOutlet weak var longPressCloseOutletBtn: UIButton!
    @IBAction func longPressCloseBtn(_ sender: UIButton) {
        
       
        
       longGesture.delaysTouchesBegan = true
        self.isLongpressView = false
        self.objChatVIewModel.unselectAll()
        self.selectedCount = 0
        self.longPressView.isHidden = true
        self.objChatVIewModel.deleteCopy()
        self.objChatVIewModel.selectedActionType = .none
        self.tableView.scrollToBottomRow()
           self.tableView.reloadData()
    }
    
    @IBAction func longPressReplyBtn(_ sender: UIButton) {
        
        self.isReplyMessage = true
        self.sendBtn.setImage(UIImage(named: "sent"), for: .normal)
        self.galleryBtn.isHidden = true
        self.replyView.isHidden = false
        self.chatText.becomeFirstResponder()
        guard let selectedIndex = self.index else {
            return
        }
     
        let message = self.objChatVIewModel.cellForRowAt(at: selectedIndex)
//        let cell = tableView.cellForRow(at: selectedIndex)!
//        self.didChangeCellSelection(cell: cell, isRowSelcted: true)
//        self.tableView.reloadRows(at: [selectedIndex], with: .none)
        self.objChatVIewModel.unselectAll()
        if message.messageType == .text || message.messageType == .reply
        {
        self.replierNameLbl.text = message.senderId?.userName ?? ""
        self.replierMessageLbl.text = message.message ?? ""
        self.replierImageMessageLbl.isHidden = true
        }else if message.messageType ==  .media
        {
            self.replierImageMessageLbl.isHidden = false
            self.replierNameLbl.text =   message.senderId?.userName ?? ""
            self.replierMessageLbl.text = ""//"Uploaded Photo"
            self.replierImageMessageLbl.kf.setImage(with: URL(string: message.message ?? ""))
            
        }
        else{
            self.replierNameLbl.text =  message.senderId?.userName ?? ""
            self.replierMessageLbl.text = "Uploaded Audio ( \(message.message ?? "") )"
            self.replierImageMessageLbl.isHidden = true
            
        }
   
        self.objChatVIewModel.setMessage(at: .reply)
        self.longPressView.isHidden = true
    }
    @IBAction func longPressCopyBtn(_ sender: UIButton) {
 
        if objChatVIewModel.chatMessagelist.filter({$0.isRowSelcted}).first?.messageType == MessageType.text
        {
            self.objChatVIewModel.setMessage(at: .copy)
            self.longPressView.isHidden = true
            self.objChatVIewModel.unselectAll()
            self.tableView.scrollToBottomRow()
            self.tableView.reloadData()
            self.showToast(message: "Content Is Copy", font: UIFont.systemFont(ofSize: 12))
            
        }else{
            alertMessage = "Content Is not copy able, try with Forward "
            self.longPressCloseOutletBtn.sendActions(for: .touchUpInside)
        }
        
        
       
        
    }
    @IBAction func longPressForwardBtn(_ sender: UIButton) {
        //self.objChatVIewModel.selectedActionType = .forword
        self.objChatVIewModel.setMessage(at: .forword)
        self.longPressView.isHidden = true
        self.navigationController?.popViewController(animated: true)
      
    }
    @IBAction func longPressDeleteBtn(_ sender: UIButton) {
        
        //check whater user read delete Agrement or not
        if UserDefaults.Default(valueForKey: KAppDeletMsg) as? Bool ?? false
        {
            //user already read delete alert
           
        }else{
            UserDefaults.Default(setBool: true, forKey: KAppDeletMsg)
            self.showAlert(message: KAppDeletMsgAlert)
            return
        }
        
        self.longPressView.isHidden = true
        
        
        guard ServerManager.shared.CheckNetwork(), objChatVIewModel.actionItems.contains(.delete) else{
            return
        }
        
        let ids = objChatVIewModel.chatMessagelist.filter({$0.isRowSelcted == true}).compactMap({$0.messageId})
        if ids.count == 0 {
            return
        }
        let params:[String:Any] = ["messageId":ids,"userId":userModel?.userId ?? "","channelId":channelId]
        SocketIOManager.sharedInstance.deletSendMsgs(message: params)
        
     
 
    }
    @IBAction func selectImageFromGallery(_ sender: UIButton) {
        
        audioRecorder = nil
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "CustomMenuVC") as? CustomMenuVC
        vc?.delegate = self
        vc?.modalPresentationStyle = .overFullScreen
        
        
        if let popover = vc?.popoverPresentationController {
            
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            popover.sourceRect = viewForSource.bounds
            popover.delegate = self
        }
        self.present(vc!, animated: true, completion: nil)
       
        
    }
    
    // popover methode for above ios 8
    
    
    func adaptivePresentationStyleForPresentationController(PC: UIPresentationController!) -> UIModalPresentationStyle {
        // This *forces* a popover to be displayed on the iPhone
        return .none
    }
    
    //MARK: send button action
    func isEmptyChatView() -> Bool
    {
       return chatText.text.isEmpty || chatText.text == "Message..." || !chatText.text.containsNonWhitespace
    }
    
    @IBAction func sendBtn(_ sender: UIButton) {
        if self.isEmptyChatView(){
            self.isMicPressed = true
            setUIForAudio()
        }
        else
        {
            self.isMicPressed = false
            self.audioView.isHidden = true
            
            sendTextMessage()
        }
    }
    //popover delegate
    
    func imageUpload(image: UIImage) {
        self.uploadingImage(image)
        if textViewString.isEmpty
        {
            self.sendBtn.isHidden = false
            self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
            self.audioView.isHidden = true
        }
        else{
            self.chatText.resignFirstResponder()
            self.sendBtn.isHidden = false
            self.sendBtn.setImage(UIImage(named: "send"), for: .normal)
            self.audioView.isHidden = true
            
        }
    }
    func cancelBtn()
    {
        if textViewString.isEmpty
        {
            self.sendBtn.isHidden = false
            isMicPressed = true
            self.audioView.isHidden = true
        }
        else
        {
            self.sendBtn.isHidden = true
            isMicPressed = false
            self.audioView.isHidden = true
        }
    }
    //MARK:textview delegate
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        self.audioView.isHidden = true
        self.sendBtn.isHidden = false
        self.textViewString = self.chatText.text
        if textView.text.isEmpty
        {
             isMicPressed = false
            self.sendBtn.setImage(#imageLiteral(resourceName: "mic") , for: .normal)
            return
        }
        if !textView.text.containsNonWhitespace {
            isMicPressed = true
        }else {
            isMicPressed = false
        }
        
    }
   
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //textView.text = ""
        let invocation = IQInvocation(self, #selector(didPressOnDoneButton))
        textView.keyboardToolbar.doneBarButton.invocation = invocation
    }
    @objc func didPressOnDoneButton() {
        if self.isEmptyChatView(){
           // self.isMicPressed = true
            //setUIForAudio()
        }
        else
        {
            self.isMicPressed = false
            self.audioView.isHidden = true
            sendTextMessage()
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { timer in
                self.chatText.text = "Message..."
                self.chatText.textColor = UIColor.gray
            }
        }

        

        
        
    }
    
    
    //MARK: send text message
    func sendMessage(content:MessageContent){
        if isReplyMessage
        {
            if let channelId =  self.channelId as? String , let index = self.index {
                
                let data = self.objChatVIewModel.cellForRowAt(at: index)
                
                objChatVIewModel.sendMessage(msg: content, channelId: channelId,messageId: data.messageId ?? "") {
                    
                    
                }
            }
        }
        else{
            
            if let channelId =  self.channelId ?? nil{//self.channelId {
                objChatVIewModel.sendMessage(msg: content, channelId: channelId) {
                    
                    
                }
            }
        }
        
        
        
    }
    func sendTextMessage(){
        
        print(self.chatText.text)
        
       
        
        if chatText.text.length == 0 || !chatText.text.containsNonWhitespace
        {
            self.chatText.text = nil
            return
        }
        
        let string =  self.chatText.text.removeWhiteSpace.trimWhiteSpace
        var content:MessageContent?
       
        if isReplyMessage
        {
            guard let selectedIndex = self.index else {
                return
            }
            let message = self.objChatVIewModel.cellForRowAt(at: selectedIndex)
             content  = MessageContent([string],.reply,"","")
            
            let params = ["msg":string,"msgType":"reply","channelId":channelId,"duration":"","size":"","userId":userModel?.userId ?? "","messageId":message.messageId ?? ""]
            ///////////////
            self.sendSocketMsg(params: params)

        }
        else{
              content  = MessageContent([string],.text,"","")
            let params = ["msg":string,"msgType":"text","channelId":channelId,"duration":"","size":"","userId":userModel?.userId ?? ""]
            //////////
             self.sendSocketMsg(params: params)
       
        }
        
        
        self.objChatVIewModel.selectedActionType = .none
//        self.view.isUserInteractionEnabled = true
//        self.textViewString = ""
//        self.audioView.isHidden = true
//        self.chatText.text = nil
//        self.sendBtn.isHidden = false
//        self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
//        self.tableView.reloadData()
//        self.tableView.scrollToBottomRow()

        
        return // all copy msg treated as New massage so no need to call rest code of block
    
        
        
    }
    
    @IBAction func replyCloseBtn(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.objChatVIewModel.unselectAll()
            self.longPressView.isHidden = true
            self.galleryBtn.isHidden = false
            self.chatText.resignFirstResponder()
            self.tableView.reloadData()
            self.tableView.scrollToBottomRow()
            self.replyView.isHidden = true
            self.isReplyMessage = false
            self.objChatVIewModel.selectedActionType = .none

        }
        
    }
    // set UI for audio message
    
    @IBAction func audioStopBtn(_ sender: UIButton) {
        self.finishRecording(success: true)
        self.audioView.isHidden = true
        self.isMicPressed = false
        self.sendBtn.isHidden = false
    }
    func setUIForAudio()
    {
        self.chatText.resignFirstResponder()
        self.chatText.isUserInteractionEnabled = false
        self.audioView.isHidden = false
        self.sendBtn.isHidden = true
        setAudioMessage()
    }
    func setAudioMessage()
    {
        recordingSession = AVAudioSession.sharedInstance()
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
        do {
            if #available(iOS 10.0, *) {
                try recordingSession.setCategory(.playAndRecord, mode: .default)
            } else {
                // Fallback on earlier versions
            }
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.recordTapped()
                        //self.startRecording()
                    } else {
                        self.sendBtn.setImage( UIImage(named: "mic"), for: .normal)
                        self.sendBtn.isHidden = false
                        self.audioView.isHidden = true
                        return
                    }
                }
            }
        } catch {
            UIApplication.shared.openURL(settingsUrl)
        }
    }
    //MARK:Start recording
    func startRecording() {
        self.tableView.isUserInteractionEnabled = false
        Audio.shareInst.isPlayingPrev()
        let audioFilename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = audioFilename[0].appendingPathComponent("recording.m4a")
 
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: documentsDirectory, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            print(documentsDirectory)
            
            //recordButton.setTitle("Tap to Stop", for: .normal)
            
        } catch {
            finishRecording(success: false)
            
        }
    }
    
    
    
    
    // set your recording button code here
    
    
    func recordTapped() {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        
        if success {
            let audioFilename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = audioFilename[0].appendingPathComponent("recording.m4a")
            self.uploadingAudio("\(documentsDirectory)")
            //self.sendBtn.setImage(UIImage(named: ""), for: .normal)
        } else {
            //self.sendBtn.setImage(UIImage(named: ""), for: .normal)
        }
    }
   
 

    //MARK: NotificationCenter handlers
    //
    @objc func keyboardWillShow(notification: NSNotification) {
        
        
        self.tableView.scrollToBottomRow()
        if self.isEmptyChatView()
        {
            self.chatText.text = ""
            self.chatText.textColor = .black
        }
     
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            _ = keyboardRectangle.height
            self.bottomConstraint?.constant = keyboardRectangle.height/1.3
            self.view.layoutIfNeeded()
            UIView.performWithoutAnimation {
                
                // make tableview scroll bottom
                self.tableView.scrollToBottomRow()
                
                
            }
            
            
        }
    }
    
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        
        
        if self.isEmptyChatView()
        {
            self.chatText.text = "Message..."
            self.chatText.textColor = .gray
        }
    
        
        self.bottomConstraint.constant = 0
        
    }
    //upload image in aws anf post on socket
    func uploadingImage(_ image:UIImage) {
        ServerManager.shared.showHud()
        self.view.isUserInteractionEnabled = false
        let globalPath = "chat_\(ProcessInfo.processInfo.globallyUniqueString)\(kAWSExt)"
        let localPath = ProcessInfo.processInfo.globallyUniqueString + kAWSExt
        let aws = S3Bucket()
        var stringArray = [String]()
        aws.uploadImageToAWS(image, globalPath, localPath) { (result, response) in
            ServerManager.shared.hideHud()
            if result {
                
                self.imageURL = response ?? ""
                stringArray.append(response ?? "")
                let content  = MessageContent(stringArray , .media,"","")
                //self.sendMessage(content: content)
                let params = ["msg":response,"msgType":"media","channelId":self.channelId,"duration":"","size":"","userId":userModel?.userId ?? ""] as [String : Any]
                ////
                
                 self.sendSocketMsg(params: params)
                
                

            }
        }
        
    }
    func uploadingAudio(_ audio:String) {
        ServerManager.shared.showHud()
        self.view.isUserInteractionEnabled = false
        let globalPath = "chat_\(ProcessInfo.processInfo.globallyUniqueString)\(KAWSAudioExt)"
        let localPath = ProcessInfo.processInfo.globallyUniqueString + KAWSAudioExt
        let aws = S3Bucket()
        var stringArray = [String]()
        aws.uploadAudioToAWS(audio: audio, globalPath, localPath,  completion: { (result, response) in
            ServerManager.shared.hideHud()


            if result {
                let fileSize =  self.sizePerMB(url:URL(string: response ?? ""))
                let fileDuration = self.duration(for: response ?? "")
                self.imageURL = response ?? ""
                stringArray.append(response ?? "")
                let content = MessageContent(stringArray,.audio,fileDuration,fileSize)
               // self.sendMessage(content: content)
                let params = ["msg":response,"msgType":"audio","channelId":self.channelId,"duration":fileDuration,"size":fileSize,"userId":userModel?.userId ?? ""] as [String : Any]
                
                self.sendSocketMsg(params:params)
               


            }
            
            
            
        })
        
    }

    func sizePerMB(url: URL?) -> String {
        guard let fileURL = url else{ return "" }
        let data =  try! Data(contentsOf: fileURL)
        let countBytes = ByteCountFormatter()
        countBytes.allowedUnits = [.useKB]
        countBytes.countStyle = .file
        let fileSize = countBytes.string(fromByteCount: Int64(data.count))
        return fileSize
    }
    func duration(for resource: String) -> String {
        let asset = AVURLAsset(url: URL(fileURLWithPath: resource))
        let minutes = asset.duration.durationText
        return minutes
    }
    //MARK: Join user Action for Join requst
    @IBAction func joinBtn(_ sender: UIButton)
    {
       if  sender.tag == 10
       {
        return
        }
        sender.tag = 10
        self.objChannelList.joinChannel(userId: userModel?.userId ?? "", channelId: self.channelId ?? "", success: { (message) in
            self.objChannelList.getChannelDetails(channelId: self.channelId ?? "", success: { (message) in
              
                DispatchQueue.main.async {
                     self.setUI()
                    NotificationCenter.default.post(name: NSNotification.Name("joinChannel"), object: nil)
                    
                    self.memberDetailBtn.isUserInteractionEnabled = true
                    let channelDetail = ["token":userModel?.accessToken ?? "","channelId":self.channelId ?? ""]
                    SocketIOManager.sharedInstance.socketOn(channelData: channelDetail)
                   self.getChannaldetails()
                   let users  = self.objChannelList.usersListData()
                   self.userExsist = users.contains(where: { (user) -> Bool in
                        if userModel?.userId == user.userId
                        {
                            return true
                        }
                        else
                        {
                            return false
                        }
                    })
                    self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
                    self.showAlert(message: "Joined Successfully")
                }
            }, falier: { (message) in
                self.showAlert(message: message)
            })
        
            
        }) { (message) in
            self.showAlert(message: message)
        }
        
         sender.isUserInteractionEnabled = false
        
    }
    func getChannaldetails()
    {
        //get Channal details
        SocketIOManager.sharedInstance.userOnline {
            (memberOnline,onlineCount,channelId,channelName,channelImage,typeOfChannel)  in
            DispatchQueue.main.async {
                self.userOnlineStatus = "\(onlineCount)"
                self.activeUserDetails_lbl.text = "\(String(describing: memberOnline)) members ,  \(self.userOnlineStatus ?? "") online "
                self.memberOnline   =    "\(memberOnline)"
                self.onlineCount  = "\(onlineCount)"
                self.channelId = channelId
                self.channelName = channelName
                self.typeOfChannel = typeOfChannel
                self.channelImage  = channelImage
                
              
            }
        }
        
    }
    //MARK: Join user detail bottom
    var memberInfo = false
    @IBAction func userJoinDetailsBtn(_ sender: UIButton) {
        
        
        let channelDetail = ["token":userModel?.accessToken ?? "","channelId":self.channelId ?? "","userId":userModel?.userId ?? ""]
        
        
        SocketIOManager.sharedInstance.socketOn(channelData: channelDetail)
        self.channelId = self.channelId ?? ""
        
        
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "ChatMemberDetailsVC") as? ChatMemberDetailsVC
        vc?.channelName = self.channelName
        vc?.typeOfChannel = self.typeOfChannel
        vc?.channelImageData = self.channelImage
        vc?.channelMemberCount = "\(self.memberOnline)"
        vc?.channelId = self.channelId
        print(self.isFromNotification)
        vc?.channelDetail =  ["token":userModel?.accessToken ?? "","channelId":channelData?.channelId ?? "","userId":userModel?.userId ?? ""]
        memberInfo = true
        
        
        self.navigationController?.pushViewController(vc!, animated: true)
 
        
    }
    
    //MARK: Back button
    @IBAction func backBtn(_ sender: Any) {
        DispatchQueue.main.async {
            if self.isReplyMessage
            {
                self.objChatVIewModel.setMessage(at: .none)
            }
            self.navigationController?.popViewController(animated: true)
            
        }
    }
     
    //MARK: Date time function
    func dateTime() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = Date()
        let dateTimeString = dateFormatter.string(from: date)
        return dateTimeString
    }
}

//MARK: table view scroll to bottom
extension UITableView {
    
    
    func scrollToBottomRow() {
        DispatchQueue.main.async {
            guard self.numberOfSections > 0 else { return }
            
            // Make an attempt to use the bottom-most section with at least one row
            var section = max(self.numberOfSections - 1, 0)
            var row = max(self.numberOfRows(inSection: section) - 1, 0)
            var indexPath = IndexPath(row: row, section: section)
            
            // Ensure the index path is valid, otherwise use the section above (sections can
            // contain 0 rows which leads to an invalid index path)
            while !self.indexPathIsValid(indexPath) {
                section = max(section - 1, 0)
                row = max(self.numberOfRows(inSection: section) - 1, 0)
                indexPath = IndexPath(row: row, section: section)
                
                // If we're down to the last section, attempt to use the first row
                if indexPath.section == 0 {
                    indexPath = IndexPath(row: 0, section: 0)
                    break
                }
            }
            
            // In the case that [0, 0] is valid (perhaps no data source?), ensure we don't encounter an
            // exception here
            guard self.indexPathIsValid(indexPath) else { return }
            
            self.scrollToRow(at: indexPath, at: .bottom, animated: false)
        }
    }
    
    func indexPathIsValid(_ indexPath: IndexPath) -> Bool {
        let section = indexPath.section
        let row = indexPath.row
        return section < self.numberOfSections && row < self.numberOfRows(inSection: section)
    }
    
    
    
}
extension Date {
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
}
extension CMTime {
    var durationText:String {
        let totalSeconds = CMTimeGetSeconds(self)
        let hours:Int = Int(totalSeconds / 3600)
        let minutes:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)
        let seconds:Int = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}

extension ChatVC:UITableViewDelegate,UITableViewDataSource,SenderChatImageCellDelegate,ReceiverChatImageCellDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objChatVIewModel.numberOfRows(InSection: section)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    
        let data = self.objChatVIewModel.cellForRowAt(at: indexPath)
        
        if data.isIncomming
        {
            if data.messageType == .text
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverCell", for: indexPath) as? ReceiverChatMessageCell
                cell?.messageTime_lbl.text = data.messageTime ?? ""
                if data.senderId?.userImage  == ""
                {
                    cell?.receiverImage.image = UIImage(named: "user")
                }
                else{
                    cell?.receiverImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                }
                cell?.receiverName_lbl.text = data.senderId?.userName ?? ""
                cell?.receiverMessage_lbl.text = data.message ?? ""
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                
                return cell!
            }
            else if data.messageType == .media
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverPhotoCell", for: indexPath) as? ReceiverChatImageCell
                cell?.obj = data.message ?? ""
                cell?.delegate = self
                cell?.receiverName_lbl.text = ""
                cell?.obj = data.message ?? ""
                if data.senderId?.userImage  == ""
                {
                    cell?.receiverImage.image = UIImage(named: "user")
                }
                else{
                    cell?.receiverImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                    
                }
                
                cell?.receiverName_lbl.text = data.senderId?.userName ?? ""
                cell?.receiverImageLoader.startAnimating()
                cell?.messageTime_lbl.text = data.messageTime ?? ""
                cell?.receiverImageMessage.kf.setImage(with: URL(string: data.message ?? ""), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (imamge, error, cache, url) in
                    cell?.receiverImageLoader.stopAnimating()
                    
                })
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                
                return cell!
            }
            else if data.messageType == .audio
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverAudioCell", for: indexPath) as? ReceiverChatAudioCell
                
                cell?.completioncall = {(issuccess,audioUrl) in
                    if issuccess{

                    }
                    
                }
                
                cell?.messageTime.text = data.messageTime ?? ""
                if data.senderId?.userImage  == ""
                {
                    cell?.receiverImage.image = UIImage(named: "user")
                }
                else{
                    cell?.receiverImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                }
                cell?.reciverName_lbl.text = data.senderId?.userName ?? ""
                
                cell?.obj = data
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                return cell!
            }
            else{
                if data.replyDetails?.replyMessageType == "text" || data.replyDetails?.replyMessageType == "reply"
                {
    
       
             
                     let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverReplyCell", for: indexPath) as? ReceiverReplyCell
                    cell?.receiverMessageTimeLbl.text = data.messageTime ?? ""
                    cell?.receiverUserImage.makeRounded()
                    if data.senderId?.userImage  == ""
                    {
                        cell?.receiverUserImage.image = UIImage(named: "user")
                    }
                    else{
                        cell?.receiverUserImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                    }
                    cell?.receiverNameLbl.text = data.senderId?.userName ?? ""
                    cell?.replierMessage.text = data.replyDetails?.message ?? ""
                    cell?.senderMessageLbl.text = data.message ?? ""
                    cell?.replierNameLbl.text = data.replyDetails?.replierName ?? ""
                  //  cell?.sizeText.text = ("\(data.senderId?.userName ?? "") \(data.replyDetails?.message ?? "") \(data.message ?? "")")
                    cell?.replierImageMessage.isHidden = true
                    didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                  
                       return cell!
                    
                }
                else if data.replyDetails?.replyMessageType == "media"{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverReplyCell", for: indexPath) as? ReceiverReplyCell
                    cell?.receiverMessageTimeLbl.text = data.messageTime ?? ""
                    if data.senderId?.userImage  == ""
                    {
                        cell?.receiverUserImage.image = UIImage(named: "user")
                    }
                    else{
                        cell?.receiverUserImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                    }
                    cell?.receiverNameLbl.text = data.senderId?.userName ?? ""
                    cell?.replierNameLbl.text = data.replyDetails?.replierName ?? ""
                    cell?.replierMessage.text =  ""//"Uploaded image"
                    cell?.senderMessageLbl.text = data.message ?? ""
                    cell?.replierImageMessage.isHidden = false
                    cell?.replierImageMessage.kf.setImage(with: URL(string: data.replyDetails?.message ?? ""))
                    didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                    
                    return cell!
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiverReplyCell", for: indexPath) as? ReceiverReplyCell
                    cell?.receiverMessageTimeLbl.text = data.messageTime ?? ""
                    if data.senderId?.userImage  == ""
                    {
                        cell?.receiverUserImage.image = UIImage(named: "user")
                    }
                    else{
                        cell?.receiverUserImage.kf.setImage(with: URL(string: data.senderId?.userImage ?? ""))
                    }
                    cell?.receiverNameLbl.text = data.senderId?.userName ?? ""
                    cell?.replierMessage.text =  "Audio \(data.replyDetails?.duration)"//"Uploaded Audio \(data.replyDetails?.duration)"
                    cell?.senderMessageLbl.text = data.message ?? ""
                    cell?.replierImageMessage.isHidden = false
                    cell?.replierImageMessage.kf.setImage(with: URL(string: data.replyDetails?.message ?? ""))
                    didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                     return cell!
                }
               
             
            }
        }
        else
        {
            if data.messageType == .text
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderCell", for: indexPath) as? SenderChatMessageCell
                cell?.messageTime_lbl.text = data.messageTime ?? ""
                cell?.senderMessage_lbl.text = data.message ?? "12345"
                cell?.messageDeliveredImage.image = UIImage(named: "d_check")
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                return cell!
            }
            else if data.messageType == .media
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderPhotoCell", for: indexPath) as? SenderChatImageCell
                cell?.obj = data.message ?? ""
                cell?.delegate = self
                cell?.messageTime_lbl.text = data.messageTime ?? ""
                //                cell?.senderImage.kf.setImage(with: URL(string: data.message ?? ""))
                cell?.senderImageLoader.startAnimating()
                cell?.messageDeleiveredImage.image =  UIImage(named: "d_check")
                cell?.obj = data.message ?? ""
                cell?.senderImage.kf.setImage(with: URL(string: data.message ?? ""), placeholder: nil, options: nil, progressBlock: nil, completionHandler: { (imamge, error, cache, url) in
                    cell?.senderImageLoader.stopAnimating()
                })
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                return cell!
            }
            else if data.messageType == .audio
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderAudioCell", for: indexPath) as? SenderChatAudioCell
                cell?.messageTime.text = data.messageTime ?? ""
                cell?.messageDeliveredImage.image = UIImage(named: "d_check")
                cell?.obj = data
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                return cell!
            }
            else
            {
                if data.replyDetails?.replyMessageType == "text" || data.replyDetails?.replyMessageType == "reply"
                {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SenderReplyCell", for: indexPath) as? SenderReplyCell
                cell?.messageTimeLbl.text = data.messageTime ?? ""
                
                    if userModel?.userId == data.replyDetails?.userId ?? ""
                    {
                        cell?.replierNameLbl.text = "You"//data.replyDetails?.replierName ?? ""
                    }else{
                        cell?.replierNameLbl.text = data.replyDetails?.replierName ?? ""
                    }
                cell?.replierMessage.text = data.replyDetails?.message ?? ""
                cell?.deliveredImage.image = UIImage(named: "d_check")
                cell?.senderMessageLbl.text = data.message ?? ""
                cell?.replierImageMessage.isHidden  = true
                didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                return cell!
                }
                else if data.replyDetails?.replyMessageType == "media"
                {
                    
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SenderReplyCell", for: indexPath) as? SenderReplyCell
                        cell?.messageTimeLbl.text = data.messageTime ?? ""
                    if userModel?.userId == data.replyDetails?.userId ?? ""
                    {
                        cell?.replierNameLbl.text = "You"//data.replyDetails?.replierName ?? ""
                    }else{
                        cell?.replierNameLbl.text = data.replyDetails?.replierName ?? ""
                    }
                    
                        cell?.replierMessage.text = ""//"Uploaded image"
                        cell?.deliveredImage.image = UIImage(named: "d_check")
                        cell?.senderMessageLbl.text = data.message ?? ""
                        cell?.replierImageMessage.isHidden  = false
                        cell?.replierImageMessage.kf.setImage(with: URL(string: data.replyDetails?.message ?? ""))
                        didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                        return cell!
                   
                }
                else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SenderReplyCell", for: indexPath) as? SenderReplyCell
                    cell?.messageTimeLbl.text = data.messageTime ?? ""
                    if userModel?.userId == data.replyDetails?.userId ?? ""
                    {
                        cell?.replierNameLbl.text = "You"//data.replyDetails?.replierName ?? ""
                    }else{
                         cell?.replierNameLbl.text = data.replyDetails?.replierName ?? ""
                    }
                   
                    cell?.replierMessage.text = "Audio \(data.replyDetails?.duration ?? "")"//"Uploaded Audio \(data.replyDetails?.duration ?? "")"
                    cell?.deliveredImage.image = UIImage(named: "d_check")
                    cell?.senderMessageLbl.text = data.message ?? ""
                    cell?.replierImageMessage.isHidden  = false
                    cell?.replierImageMessage.kf.setImage(with: URL(string: data.replyDetails?.message ?? ""))
                    didChangeCellSelection(cell:cell!,isRowSelcted: data.isRowSelcted)
                    return cell!
                }
                
                
            }
            
        }
        }
        
        
   
    //for sender
    func isBurronPress(buttonPress: Bool,message:String) {
        if buttonPress
        {
            let storyBoard = UIStoryboard.init(name: StoryBoardIdentity.KDashboardTB, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ImageEnlargeVC") as? ImageEnlargeVC
            vc?.image = message ?? ""
        self.navigationController?.pushViewController(vc!, animated: true)
            
        }
        
    }
    //for receiver
    func isBurronPressReceiver(buttonPress: Bool, message: String) {
        if buttonPress
        {
            
            let storyBoard = UIStoryboard.init(name: StoryBoardIdentity.KDashboardTB, bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ImageEnlargeVC") as? ImageEnlargeVC
            vc?.image = message ?? ""
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    func didChangeCellSelection(cell:UITableViewCell,isRowSelcted:Bool){
        cell.backgroundColor = isRowSelcted == true ? #colorLiteral(red: 0.007843137255, green: 0.8242745996, blue: 0.6730399728, alpha: 0.24) : .clear
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = self.objChatVIewModel.cellForRowAt(at: indexPath)
        
//        if data.messageType == .audio {
//            return 110.0
//        }else if data.messageType == .media{
//            return 260.0
//        }
//        else if data.messageType == .reply
//        {
//            return 170.0
//        }
        
        if data.messageType == .media{
            return 250
        }
        
        return UITableView.automaticDimension
    }
    func updateActionView(){
        let actionItems  = objChatVIewModel.actionItems
        self.copyBtn.isHidden = true
        self.replyBtn.isHidden = true
        self.forwardBtn.isHidden = true
        self.deleteBtn.isHidden = true
        actionItems.forEach { (obj) in
            switch obj{
            case .copy:
                copyBtn.isHidden = false
            case .reply:
                replyBtn.isHidden = false
            case .forword:
                forwardBtn.isHidden = false
            case .delete:
                deleteBtn.isHidden = false
            default:
                break
            }
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if isLongpressView
        {
           
            self.objChatVIewModel.multipleSelection(at: indexPath)
            self.selectedCount = self.objChatVIewModel.numberOfSelectedMessage
            
            updateActionView()
            let cell = tableView.cellForRow(at: indexPath) as! UITableViewCell
            self.didChangeCellSelection(cell: cell, isRowSelcted: self.objChatVIewModel.cellForRowAt(at: indexPath).isRowSelcted)
            
            
        }
        else{
            return
        }
       
        DispatchQueue.main.async {
            self.chatText.resignFirstResponder()
        }
        
    }
}

extension ChatVC{
    
    var isPlaceHolder:Bool{
        let string =  self.chatText.text.removeWhiteSpace.trimWhiteSpace
        return string == "Message..."
    }
    
    func showPlaceHolder(){
        let string =  self.chatText.text.removeWhiteSpace.trimWhiteSpace
        if string.length == 0 {
            self.chatText.text = "Message..."
            self.chatText.textColor = .gray
        }
    }
    func clearOnAppearance() {
        DispatchQueue.main.async {
            for indexPath in self.tableView.indexPathsForSelectedRows ?? [] {
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
        }
        
    }
    
    func sendSocketMsg(params:[String:Any]){
        DispatchQueue.main.async {
            
            SocketIOManager.sharedInstance.sendMessageNew(message: params)
            ServerManager.shared.hideHud()
            self.audioView.isHidden = true
            self.isMicPressed = false
            self.sendBtn.isHidden = false
            self.galleryBtn.isHidden = false
            self.isReplyMessage = false
            self.replyView.isHidden = true
            self.sendBtn.isSelected = false
            self.textViewString = ""
            self.chatText.text = ""
            self.chatText.isUserInteractionEnabled = true
            self.tableView.isUserInteractionEnabled = true
            self.sendBtn.setImage(UIImage(named: "mic"), for: .normal)
        }
        
        
        
    }
    
   

    
}

extension String {
    
    var containsNonWhitespace: Bool {
        return !self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
}

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    } }
