//
//  UserDetailsVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 05/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class UserDetailsVC: UIViewController {
    var channelData:String?
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var userLastSeenLbl: UILabel!
    @IBOutlet weak var onlineStatusImageView: UIImageView!
    
    @IBOutlet var objUserDetailsViewModel: UserDetailsViewModel!
    @IBOutlet weak var notificationSwitch: UISwitch!
  
    @IBOutlet var objChannelVM: ChannelsListViewModel!
    var userDetail:UsersList?
    override func viewDidLoad() {
        super.viewDidLoad()
         setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI()
    {
        print(userDetail?.userId)
        if userDetail?.imageUrl == ""
        {
            self.userImage.image = UIImage(named: "user")
        }
        else{
            self.userImage.kf.setImage(with: URL(string: self.userDetail?.imageUrl ?? ""))
        }
        self.userImage.makeRounded()
        self.userName_lbl.text = userDetail?.fullName ?? ""
        if userDetail?.onlineStatus  ?? false
        {
             self.onlineStatusImageView.image = UIImage(named: "IndicatorOnline")
             self.userLastSeenLbl.text = "Online"
        }
        else
        {
             self.onlineStatusImageView.image = UIImage(named: "Indicator")
            self.userLastSeenLbl.text = "last seen \(userDetail?.lastSeen ?? "") "
        }
       
     
    }
    @IBAction func notificationControlBtn(_ sender: UISwitch) {
        
        self.objChannelVM.notificationControl(channelId: self.channelData ?? "", notification: sender.isOn, success: { (message) in
            
        }) { (message) in
            self.showAlert(message: message)
        }
    }
    @IBAction func backBtn(_ sender: UIButton) {
       
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func reportUserBtn(_ sender: UIButton) {
        self.objUserDetailsViewModel.reportUser(userId:userModel?.userId ?? "", reportUserId: self.userDetail?.userId ?? "", channelId:self.channelData ?? "", reason: "user inappopriate", success: { (message) in
            self.showAlertForUserReport(message: message)
        }) { (message) in
             self.showAlert(message: message)
        }
    }
   
    @IBAction func sharedContentBtn(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharedContentVC") as? SharedContentVC
//        vc?.channelData = self.channelData
//        vc?.userData = self.userDetail
//         print(self.userDetail)
//        self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func sharedContent(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharedContentVC") as? SharedContentVC
        vc?.channelData = self.channelData
        vc?.userData = self.userDetail
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}
