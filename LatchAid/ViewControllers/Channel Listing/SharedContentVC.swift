//
//  SharedContentVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import AVFoundation
class SharedContentVC: UIViewController,AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    @IBOutlet weak var mediaBtn: CustomButton!
    @IBOutlet weak var audioBtn: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var objSharedViewModel: SharedContentViewModel!
    var channelData:String?
    var userData:UsersList?
    var buttonTaped:ButtonSelection = .media
    var mediaCount = 0
    var player:AVAudioPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = .none
       Audio.shareInst.table = tableView
        setUI()
        getSharedContent()
    }
    
    func setUI()
    {
        
        
        if buttonTaped == .media
        {
            self.tableView.isHidden = true
            self.collectionView.isHidden = false
            self.mediaBtn.backgroundColor = buttonColor
            self.mediaBtn.setTitleColor(.white, for: .normal)
            self.audioBtn.backgroundColor = .white
            self.audioBtn.setTitleColor(.black, for: .normal)
        }
        else{
            self.tableView.isHidden = false
            self.collectionView.isHidden = true
            self.audioBtn.backgroundColor = buttonColor
            self.audioBtn.setTitleColor(.white, for: .normal)
            self.mediaBtn.setTitleColor(.black, for: .normal)
            self.mediaBtn.backgroundColor = .white
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        Audio.shareInst.isPlayingPrev()
    }
    func getSharedContent()
    {
        print(userData)
        self.objSharedViewModel.getSharedContent(channelId: self.channelData ?? "", userId:userData?.userId ?? "", success: { (message) in
            DispatchQueue.main.async {
                self.setUI()
                self.tableView.reloadData()
                self.collectionView.reloadData()
            }
            
            
        }) { (message) in
            self.showAlert(message: message)
        }
    }
    @IBAction func audioBtnAction(_ sender: CustomButton) {
        self.buttonTaped = .audio
        self.setUI()
    }
    @IBAction func mediaBtnAction(_ sender: CustomButton) {
        self.buttonTaped = .media
       
        self.setUI()
    }
    @IBAction func backBtn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
}
extension SharedContentVC : UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SharedContentTableViewCell
        cell?.index = indexPath
        cell?.obj = self.objSharedViewModel.cellRowIndexAtForAudio(at: indexPath)
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.mediaCount = self.objSharedViewModel.numberOfRowsForAudio(InSection: section)
        return self.objSharedViewModel.numberOfRowsForAudio(InSection: section)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.mediaCount = self.objSharedViewModel.numberOfRowsForMedia(InSection: section)
        return self.objSharedViewModel.numberOfRowsForMedia(InSection: section)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? SharedContentCollectionViewCell
        cell?.obj = self.objSharedViewModel.cellRowIndexAtForMedia(at: indexPath)
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mediaFile = self.objSharedViewModel.didSelectRowAtForMedia(at: indexPath)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ImageEnlargeVC") as? ImageEnlargeVC
        vc?.image = mediaFile
      self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
 
}
