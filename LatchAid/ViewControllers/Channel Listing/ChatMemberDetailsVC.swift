//
//  ChatMemberDetailsVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 19/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class ChatMemberDetailsVC: UIViewController {
    var channelData:ChannelsList?
    var  channelDetail = [String:Any]()
  
    
    
    @IBOutlet weak var isEditabel: UIButton!
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var channelName_lbl: UILabel!
    @IBOutlet weak var channelMemberCount_lbl: UILabel!
    @IBOutlet weak var channelType_lbl: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet var objChannelList: ChannelsListViewModel!
    var channelName = ""
    var typeOfChannel = ""
    var channelMemberCount = ""
    var channelImageData = ""
    var channelId = ""
    @IBOutlet weak var cardVIew: CardView!
    @IBOutlet weak var tableView: UITableView!
   // var oldUserData = [UsersList]()
    var userData = [UsersList]()
    {
        didSet
        {
            self.tableView.reloadData()
           
             self.setAdminAccess()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        setUI()
        getUserDetails()
    }
    func setUI()
    {
        if channelImageData.isEmpty
        {
            self.channelImage.image = UIImage(named: "user_group")
        }
        else{
            self.channelImage.kf.setImage(with: URL(string: channelImageData))
        }
      
        self.channelImage.makeRounded()
        self.channelName_lbl.text = channelName
        self.channelMemberCount_lbl.text = "\(channelMemberCount) members"
        self.channelType_lbl.text = typeOfChannel
        
    }
    override func viewWillAppear(_ animated: Bool) {
//         getUserDetails()
    }
    func setNotificationSwitch (active:Bool)
    {
         self.notificationSwitch.isOn = active
    }
    override func viewWillDisappear(_ animated: Bool) {
        if channelDetail != nil {
            SocketIOManager.sharedInstance.socket.off(LactchEvents.receiveMessage.rawValue)
            SocketIOManager.sharedInstance.socketDisconnect(data: channelDetail)
        }
        
    }
    
    func getUserDetails()
    {
        self.objChannelList.getChannelDetails(channelId: self.channelId, success: { (message) in
                    self.userData = self.objChannelList.usersListData()
           
            
        }) { (message) in
            self.showAlert(message: message)
        }
       
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    @IBAction func notificationControlBtn(_ sender: UISwitch) {
//        if sender.isOn
//        {
//            self.objChannelList.notificationControl(channelId: channelId , notification: true, success: { (message) in
//
//            }) { (message) in
//                 self.showAlert(message: message)
//            }
//        }
//        else{
//            self.objChannelList.notificationControl(channelId:channelId , notification: false, success: { (message) in
//
//            }) { (message) in
//                self.showAlert(message: message)
//            }
//        }
        
        self.objChannelList.notificationControl(channelId:channelId , notification: sender.isOn, success: { (message) in
            print("==========",message)
            
        }) { (message) in
            self.showAlert(message: message)
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        //SocketIOManager.sharedInstance.socketDisconnect(data: [:])
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func exitChannelBtn(_ sender: UIButton) {
        self.objChannelList.exitChannelMember(channelId: self.channelId, success: { (message) in
          
            SocketIOManager.sharedInstance.socket.disconnect()
            NotificationCenter.default.post(name: NSNotification.Name("exitChannel"), object: nil)
            self.navigationController?.popViewController(animated: true)
            
        }) { (message) in
             self.showAlert(message: message)
        }
        
    }
    
    @IBAction func sharedBtn(_ sender: UIButton) {
       
    }
    
    @IBAction func sharedContent(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SharedContentVC") as? SharedContentVC
        vc?.channelData = self.channelId
      self.navigationController?.pushViewController(vc!, animated: true)
    }
   
    
    func setAdminAccess(){
        let user = objChannelList.usersListData().filter({$0.userId == userModel?.userId})
       print(user)
        if user.first?.userType == "admin"
        {
            self.isEditabel.isHidden = false
            self.isEditabel.addTarget(self, action: #selector(selectGropIcon(_:)), for: .touchUpInside)
        }else{
             self.isEditabel.isHidden = true
        }
        if user.first?.userId == userModel?.userId
        {
            self.setNotificationSwitch(active:user.first?.notification! ?? false )
        }
    }

    
}
extension ChatMemberDetailsVC:CustomMenuVCDelegate,UIPopoverPresentationControllerDelegate
{
    @objc func selectGropIcon(_ sender:UIButton)
    {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "CustomMenuVC") as? CustomMenuVC
        vc?.delegate = self
        vc?.modalPresentationStyle = .overFullScreen
        
        
        if let popover = vc?.popoverPresentationController {
            
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            popover.sourceRect = viewForSource.bounds
            popover.delegate = self
        }
        self.present(vc!, animated: true, completion: nil)
    }
    //popover delegate
    
    func imageUpload(image: UIImage) {
        channelImage.image = image
        self.uploadingImage(image)
        print("UPLOADE PRECESS IN NO GOING......")
        
    }
    func cancelBtn()
    {
        print("UPLOADE PRECESS CANCEL")
    }
    
    func uploadingImage(_ image:UIImage) {
        ServerManager.shared.showHud()
        self.view.isUserInteractionEnabled = false
        let globalPath = "GroupIcon_\(ProcessInfo.processInfo.globallyUniqueString)\(kAWSExt)"
        let localPath = ProcessInfo.processInfo.globallyUniqueString + kAWSExt
        let aws = S3Bucket()
        var stringArray = [String]()
        aws.uploadImageToAWS(image, globalPath, localPath) { (result, response) in
            
            self.view.isUserInteractionEnabled = true
            if result {
                print("-------",response ?? "")
                //self.imageURL = response ?? ""
                stringArray.append(response ?? "")
                let content  = MessageContent(stringArray , .media,"","")
                let params = ["imageUrl":response,"channelId":self.channelId] as [String : Any] //,"userId":userModel?.userId ?? ""
                self.uploadToChannalIconOnServer(par: params)
                
                
                
                
            }
        }
        
    }
    func uploadToChannalIconOnServer(par:[String : Any]){
        print("_____>>",par)
        ServerManager.shared.httpPut(request: ApiName.editChannelImgae.api(environment:kApiEnvironment),params: par, headers:ServerManager.shared.apiHeaders, successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            self.view.isUserInteractionEnabled = true
            guard let response = responseData.JKDecoder(JoinChannel.self) else{
                return
                
            }
            print("+++++++++++++ \(String(describing: response))")
            guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                return}
            let message = response.message ?? ""
            switch status{
            case 200:
                
                //self.deleteCopy()
                //success()
                
                alertMessage = message
                break
            default:
                alertMessage = message
                
                break
            }
        }, failureHandler: { (error) in
            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                
            }
        })
        
    }
}


extension ChatMemberDetailsVC :UITableViewDelegate,UITableViewDataSource
{
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userDetailCell", for: indexPath) as? UserDetailCell
        cell?.obj = self.userData[indexPath.row]
       
        cell?.selectionStyle = .none
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailsVC") as? UserDetailsVC
        vc?.channelData = self.channelId
        vc?.userDetail = self.userData[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
