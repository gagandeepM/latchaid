//
//  ChannelListingVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class ChannelListingVC: UIViewController {
    
    @IBOutlet var chatViewModel: ChatViewModel! = ChatViewModel.shared
    @IBOutlet var objSearchChannelList: ChannelsListViewModel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchbar: UISearchBar!
    var searchStringValue = ""
    var isFromNotification = false
    var channelId = ""
    var temp = [ChannelsList]()
    fileprivate var isEnableIQKeyobard:Bool = true{
        didSet{
            IQKeyboardManager.shared.enable = isEnableIQKeyobard
            IQKeyboardManager.shared.enableAutoToolbar = false
            IQKeyboardManager.shared.shouldResignOnTouchOutside = isEnableIQKeyobard
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchbar.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(dataUpdate), name: NSNotification.Name("joinChannel"), object: nil)
        SocketIOManager.sharedInstance.socketConnect()
        
        self.getChannelList()
        
    }
    
    @objc  func dataUpdate()
    {
        getChannelList()
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    //MARK: - get channel list
    // get channellist and show all public and other channels
    func getChannelList()
    {
        objSearchChannelList.getChannelListing(success: { (message) in
            DispatchQueue.main.async {
                
                self.tableView.isHidden = false
                if self.temp.count==0
                {
                    self.temp = self.objSearchChannelList.channelsList
                }
                
                self.tableView.reloadData()
            }
            
            
        }) { (message) in
            self.tableView.isHidden = true
            self.showAlert(message: message)
            
        }
    }
    func getChannelListWithSearchString()
    {
        objSearchChannelList.getChannelListing(searchString: self.searchStringValue, success: { (message) in
            
            
            DispatchQueue.main.async {
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        }) { (errorMsg) in
            self.tableView.isHidden = true
            //remove dont show any alert whene no recode found
            //self.showAlert(message: errorMsg)
            
        }
    }
   
    
    
    
}
extension ChannelListingVC:UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objSearchChannelList.numberOfRow()
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "channelList", for: indexPath) as? ChannelListCell
        cell?.obj = objSearchChannelList.cellRowAt(indexPath)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            let data = self.objSearchChannelList.cellRowAt(indexPath)
            tableView.isUserInteractionEnabled = true
            if  self.chatViewModel.selectedActionType == .none
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                vc?.isFromChannelList = true
                vc?.isWithNoCopyForward = true
                vc?.channelData = data
                vc?.channelId = data.channelId ?? ""
                vc?.setUI()
                
                self.navigationController?.pushViewController(vc!, animated: true)
                
            }else if self.chatViewModel.selectedActionType == .forword {
                guard let channelId  =  data.channelId else{return}
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                vc?.isFromChannelList = true
                vc?.channelData = data
                vc?.channelId = data.channelId ?? ""
                let copyData  = LDPasteboard.shared.copyPaste
                let iDs = LDPasteboard.shared.messageIDs
                
                let params:[String:Any]  = ["channelId": data.channelId ?? "","msg":iDs,"userId":userModel?.userId ?? ""]
                vc?.paRams = params
                // vc?.sendSocketMsg(params:params)
                //vc?.setUI()
                self.navigationController?.pushViewController(vc!, animated: true)
                
                
                
                
            }
            else if self.chatViewModel.selectedActionType == .copy
            {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                vc?.isFromChannelList = true
                vc?.isWithNoCopyForward = true
                vc?.channelData = data
                vc?.channelId = data.channelId ?? ""
                vc?.setUI()
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
            
            
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        setSearchBar()

    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count ?? 0>0
        {
            searchBar.showsCancelButton = true
            setSearchBar()
        }else{
            searchBar.showsCancelButton = false
            
             self.getChannelList()
            //search on search tap
//            DispatchQueue.main.async {
//                guard self.objSearchChannelList.channelsList.count==0 else {return}
//                self.getChannelList()
////                self.objSearchChannelList.channelsList = self.temp
//                self.tableView.reloadData()
//            }
            
        }
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.getChannelList()
        searchBar.showsCancelButton = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
    
    
    

    
    func setSearchBar(){
        let searchText = self.searchbar.text ?? ""
        if searchText == " "
        {
            
            return
        }
        
        searchStringValue = searchText.removeWhiteSpace.trimWhiteSpace.addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) ?? ""
        
        if searchStringValue.isEmpty
        {
            self.getChannelList()
        }
        else
        {
            self.objSearchChannelList.removeAll()
            self.getChannelListWithSearchString()
            
        }
    }
}
