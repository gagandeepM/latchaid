//
//  ImageEnlargeVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 31/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class ImageEnlargeVC: UIViewController {

    @IBOutlet weak var ImageView: UIImageView!
    var image = ""
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.ImageView.kf.setImage(with: URL(string:image))

    }
    

    @IBAction func closeBtn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
