//
//  ChangePasswordVC.swift
//  Dumpya
//
//  Created by Chandan Taneja on 05/11/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    @IBOutlet var objChangePassword: ProfileViewModel!
    @IBOutlet weak fileprivate var oldPasswordTF: CustomTextField!
    @IBOutlet weak fileprivate var newPasswordTF: CustomTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: CustomTextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Actions
    @IBAction func onClickBack(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func textPrimaryFunctio(_ sender: UITextField) {
        if sender == self.oldPasswordTF
        {
            newPasswordTF.becomeFirstResponder()
        }else if sender == newPasswordTF{
            confirmPasswordTF.becomeFirstResponder()
        }else {
            sender.resignFirstResponder()
        }
        
    }
    
    
    @IBAction func onClickChangePassword(_ sender: Any) {
        var messageStr = ""
        guard let oldPassword = oldPasswordTF.text else {return}
        guard let newPassword = newPasswordTF.text else { return }
        guard let confirmPassword = confirmPasswordTF.text else {return}
        if (oldPasswordTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kPasswordEmpty)
            
        }
        else if  (newPasswordTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kPasswordEmpty)
            
        }else if (newPasswordTF.text?.count)! < 6 {
            showAlert(message: FieldValidation.kPasswordLength)
            
        }
            
        else if (confirmPasswordTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kConfirmPasswordEmpty)
            
        }else if  newPasswordTF.text != confirmPasswordTF.text{
            showAlert(message: FieldValidation.kPasswordNotMatch)
            
        }
        else{
            
            objChangePassword.changePassword(oldPassword: oldPassword, password: newPassword, confirmPassword: confirmPassword, onSuccess: { (message) in
                
                
                self.showAlertAction(message: message, onCompletion: { (index) in
                    
                    DispatchQueue.main.async {
                        
                        AppDelegate.sharedDelegate.logoutUser()
                    }
                    
                })
                
                
            }, onFailure: { (message) in
                self.showAlert(message: message)
                
            } )
        }
    }
    
    
}
