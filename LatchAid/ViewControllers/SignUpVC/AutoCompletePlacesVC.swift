//
//  AutoCompletePlacesVC.swift
//  LatchAid
//
//  Created by Sunil Garg on 29/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import GooglePlaces

class AutoCompletePlacesVC: UIViewController{
    
    @IBOutlet weak var backBtn: UIButton!
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x: 0, y: 84.0, width: 350.0, height: 45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        
        definesPresentationContext = true
    }
    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
     
    }
    
}


// Handle the user's selection.
extension AutoCompletePlacesVC:GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        
        searchController?.isActive = false
        // Do something with the selected place.
        
        let dic = ["location":place.formattedAddress]
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "locationSelected"),object: self, userInfo:dic)

  
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
