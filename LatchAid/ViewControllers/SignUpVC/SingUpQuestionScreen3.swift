
//

import UIKit
import CoreLocation
class SingUpQuestionScreen3: UIViewController,CLLocationManagerDelegate {
    
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var nextBtn: CustomButton!
    @IBOutlet weak var locationBtn: UIButton!
    var objectProfileViewModel = ProfileViewModel()
    var isEdit = false
    var isFrom = ""
    var params:[String:Any] = [String:Any]()
    var lat:Double?
    var long:Double?
    var location:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector:  #selector(self.locationUpdate(_:)), name: NSNotification.Name(rawValue: "locationSelected"), object: nil)
        self.locationBtn.setTitle(userModel?.location ?? "Enter your postcode", for: .normal)
        self.view.backgroundColor = CustomColor.ViewColor
        
        if isEdit
        {
            self.locationBtn.setTitle(userModel?.location ?? "Enter your postcode", for: .normal)
            self.stackView.isHidden = true
            self.nextBtn.setImage(#imageLiteral(resourceName: "Done"), for: .normal)

            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    //    MARK: Button actions
    @objc  func locationUpdate(_ notification: NSNotification)
    {
        NotificationCenter.default.removeObserver(self)
        if let location =  notification.userInfo?["location"] as? String
        {
             DispatchQueue.main.asyncAfter(deadline: .now()+1, execute: {
                self.location = location
                self.locationBtn.setTitle( location, for: .normal)
                if let obj = notification.object as? AutoCompletePlacesVC
                {
                    obj.navigationController?.popViewController(animated: true)
                }
            })
        }
        
    }
    
    @IBAction func getLocationBtn(_ sender: UIButton) {
          performSegue(withIdentifier:  SegueIdentity.KAutoCompletePlacesScreen, sender: sender)
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextBtn(_ sender: CustomButton) {
        if location == nil
        {
            self.navigationController?.popViewController(animated: true)
            return
        }
        else if self.locationBtn.titleLabel?.text == nil
        {
            showAlert(message: FieldValidation.kPostcodeSelect)
            return
        }
        else if self.locationBtn.titleLabel?.text == ""
        {
            showAlert(message: FieldValidation.kPostcodeSelect)
            return
        }
        else if self.locationBtn.titleLabel?.text == "Enter your postcode"
        {
            showAlert(message: FieldValidation.kPostcodeSelect)
            return
        }
        if isEdit
        {
            objectProfileViewModel.editMyInfo(location: self.location ?? "", timeForBreastfeeding: userModel?.timeForBreastfeeding ?? "", interestedForBreastfeeding: userModel?.interestedForBreastfeeding ?? "", selectChildAge: userModel?.selectChildAge ?? "", selectAge: userModel?.selectAge ?? "", interestedNumber: userModel?.interestedNumber ?? 0, deliveryDate: userModel?.deliveryDate ?? "", success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                self.showAlert(message: message)
                
            }) { (message) in
                self.showAlert(message: message)
            }
             self.navigationController?.popViewController(animated: true)
        }
        else
        {
            performSegue(withIdentifier: SegueIdentity.KSingUpQuestion4, sender: sender)
        }
        
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion4) {
            
            self.params["location"] = self.location
            self.params["lat"] = self.lat
            self.params["long"] = self.long
            let vc = segue.destination as! SingUpQuestionScreen4
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
            
        else if (segue.identifier == SegueIdentity.KAutoCompletePlacesScreen)
        {
             segue.destination as! AutoCompletePlacesVC
        }
    }
}
