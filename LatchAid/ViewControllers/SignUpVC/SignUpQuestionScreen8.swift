//
//  SignUpQuestionScreen8.swift
//  LatchAid
//
//  Created by Sunil Garg on 03/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class SingUpQuestionScreen8: UIViewController {
    
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var nextBtn: CustomButton!
    @IBOutlet weak var pickerView: UIDatePicker!
    var isEdit = false
    var params:[String:Any] = [String:Any]()
    var isFrom = ""
    var selectedData:String?
    var objectProfileViewModel = ProfileViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        pickerView.datePickerMode = .date
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "MM/dd/yyyy"
        selectedData = dateFormatter.string(from: pickerView.date)
        
        
        pickerView.setValue(UIColor.white, forKeyPath: "textColor")
        if isEdit
        {
            self.stackView.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "MM/dd/yyyy"
            if let date = dateFormatter.date(from: userModel?.deliveryDate ?? "")
            {
                pickerView.date = date
                selectedData = dateFormatter.string(from: date)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let calendar = Calendar(identifier: .gregorian)
        
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.month = 10
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        
        let minDate = currentDate
        
        pickerView.minimumDate = minDate
        pickerView.maximumDate = maxDate
        
    }
    
    //MARK: Button actions
    @IBAction func nextBtn(_ sender: CustomButton) {
        if self.selectedData ==  nil
        {
            showAlert(message: FieldValidation.kQuestionSelect)
            
            return
        }
        if isEdit
        {
            
            objectProfileViewModel.editMyInfo(location: userModel?.location ?? "", timeForBreastfeeding: userModel?.timeForBreastfeeding ?? "", interestedForBreastfeeding: userModel?.interestedForBreastfeeding ?? "", selectChildAge: userModel?.selectChildAge ?? "",selectAge: userModel?.selectAge ?? "", interestedNumber: userModel?.interestedNumber ?? 0, deliveryDate: selectedData ?? "", success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                self.showAlert(message: message)
                
            }) { (message) in
                self.showAlert(message: message)
            }
            self.navigationController?.popViewController(animated: true)
        }
        else{
            performSegue(withIdentifier: SegueIdentity.KSingUpQuestion3, sender: sender)
        }
    }
    @IBAction func backBtn(_ sender: UIButton) {
        
          self.navigationController?.popViewController(animated: true)
    }
    @IBAction func pickerValueChanged(_ sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        selectedData = formatter.string(from: sender.date)
        print(selectedData)
        
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion3) {
            
            self.params["deliveryDate"] = self.selectedData ?? ""
            let vc = segue.destination as! SingUpQuestionScreen3
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
        
    }
    
    
    
    
}
