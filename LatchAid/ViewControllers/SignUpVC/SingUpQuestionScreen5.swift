//
//  SingUpQuestionScreen5.swift
//  LatchAid
//

//

import UIKit

class SingUpQuestionScreen5: UIViewController {
    var questionData:[String] = ["Not much interested","Interested","Very interested"]
    var params:[String:Any] = [String:Any]()
    var questionDataValue:String?
    var isFrom = ""
    var index:Int?
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    @IBOutlet weak var doneBtn: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.doneBtn.backgroundColor = CustomColor.buttonColor1
        self.doneBtn.tintColor = .white
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion5) {
            
            self.params["interestedForBreastfeeding"] = self.questionDataValue ?? ""
            let vc = segue.destination as! SingUpQuestionScreen6
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
    }
    
    //MARK: Button actions
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickDoneBtn(_ sender: CustomButton) {
        if self.questionDataValue ==   nil
        {
            showAlert(message: FieldValidation.kQuestionSelect)
            return
        }
        
        if isFrom == "google"
        {
            
            
            self.objUserViewModel.googleSignUP(params:params, success: { (message) in
                
                DispatchQueue.main.async {
                    self.showAlert(message: message)
                    AppDelegate.sharedDelegate.setUpMainController()
                }
                
                
                
            }, failure: { (message) in
                self.showAlert(message: message)
            })
            
        }
        else if isFrom == "facebook"
        {
            
            
            self.objUserViewModel.facebookSignUP(params:params, success: { (message) in
                DispatchQueue.main.async {
                    
                    
                    AppDelegate.sharedDelegate.setUpMainController()
                    
                    
                    
                }
            }, failure: { (message) in
                self.showAlert(message: message)
            })
            
        }
        else
        {
            objUserViewModel.signUp(params:params, success: { (message) in
                
                DispatchQueue.main.async {
                    AppDelegate.sharedDelegate.setUpMainController()
                }
                
                
            }, failure: { (message) in
                self.showAlert(message: message)
            })
            
        }
        
    }
    
    
    
}
//MARK: Extension for table view
extension SingUpQuestionScreen5:UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "questionCell") as! QuestionCell
        headerCell.headingLabel.text = " How much are you interested in breastfeeding? "
        headerView.addSubview(headerCell)
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionOptionCell", for: indexPath) as? QuestionOptionCell
        if indexPath.row == index {
            cell?.optionImage.image = UIImage(named: "radio_btton")
        }
        else {
            cell?.optionImage.image = UIImage(named: "radio")
        }
        cell!.textDescriptionLabel?.text = self.questionData[indexPath.row] 
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        index = indexPath.row
        self.questionDataValue = self.questionData[indexPath.row]
        
        tableView.reloadData()
        
        
    }
    
}

