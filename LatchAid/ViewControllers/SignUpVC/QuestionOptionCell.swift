//
//  QuestionOptionCell.swift
//  LatchAid
//
//  Created by Anuj Sharma on 10/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class QuestionOptionCell: UITableViewCell {
    
    @IBOutlet weak var textDescriptionLabel: UILabel!
    
    @IBOutlet weak var optionImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
