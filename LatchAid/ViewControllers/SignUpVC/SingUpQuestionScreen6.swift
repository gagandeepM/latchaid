//
//  SingUpQuestionScreen6.swift
//  LatchAid
//
//  Created by Sunil Garg on 10/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class SingUpQuestionScreen6: UIViewController {
    var questionData:[String] = ["Your age","Baby's age","Your location","Your education","Your interest level on breastfeeding"]
    var params:[String:Any] = [String:Any]()
    var questionDataValue:[String] = [String]()
    var index:Int?
    var isFrom = ""
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    @IBOutlet weak var doneBtn: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
    }
    
    
}

//MARK: Extension for table view
extension SingUpQuestionScreen6:UITableViewDelegate,UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "questionCell") as! QuestionCell
        headerCell.headingLabel.text = " How much are you interested in breastfeeding? "
        headerView.addSubview(headerCell)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionOptionCell", for: indexPath) as? QuestionOptionCell
        cell?.optionImage.image = UIImage(named: "Unchecked Checkbox")
        cell!.textDescriptionLabel?.text = self.questionData[indexPath.row] as? String
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = self.tableView.cellForRow(at: indexPath) as! QuestionOptionCell
        
        
        if(indexPath.row == index)
        {
            return
            
        }
        
        if cell.optionImage.image == UIImage(named: "Checked Checkbox")
        {
            cell.optionImage.image = UIImage(named: "Unchecked Checkbox")
            
        }
        else
        {
            cell.optionImage.image = UIImage(named: "Checked Checkbox")
            self.questionDataValue.append(self.questionData[indexPath.row])
        }
        self.params["chatGroup"] = self.questionDataValue
    }
    
    
}
