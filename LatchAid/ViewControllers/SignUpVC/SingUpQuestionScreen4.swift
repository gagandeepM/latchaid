//
//  SingUpQuestionScreen4.swift
//  LatchAid
//
//  Created by Sunil Garg on 10/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class SingUpQuestionScreen4: UIViewController {
    
    @IBOutlet var stackView: UIStackView!
    var isEdit = false
    var questionData:[String] = ["First week","Second week","1-3 months","3-6 months","6+ months"]
    var params:[String:Any] = [String:Any]()
    var questionDataValue:String?
    var index:Int?
    var isFrom = ""
    @IBOutlet weak var nextBtn: CustomButton!
    @IBOutlet weak var tableView: UITableView!
    var objectProfileViewModel = ProfileViewModel()
    override func viewDidLoad() {
        tableView.tableFooterView = UIView()
        super.viewDidLoad()
        self.tableView.isScrollEnabled = true
        self.view.backgroundColor = CustomColor.ViewColor
        self.tableView.delegate = self
        self.tableView.dataSource = self
        if isEdit
        {
            self.stackView.isHidden = true
            
            let data =  questionData.index(of: userModel?.timeForBreastfeeding ?? "")
            index = data!
            self.questionDataValue = self.questionData[index!]
            self.nextBtn.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        }
        
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion5) {
            
            self.params["timeForBreastfeeding"] = self.questionDataValue ?? ""
            let vc = segue.destination as! SignUpQusetionScreen7
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
        
    }
    //MARK: Button actions
    @IBAction func backBtn(_ sender: UIButton) {
        
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtn(_ sender: CustomButton) {
        
        
        if self.questionDataValue == nil
        {
            showAlert(message: FieldValidation.kQuestionSelect)
            return
        }
        if isEdit
        {
            
            
            objectProfileViewModel.editMyInfo(location: userModel?.location ?? "", timeForBreastfeeding: self.questionDataValue ?? "", interestedForBreastfeeding: userModel?.interestedForBreastfeeding ?? "", selectChildAge: userModel?.selectChildAge ?? "", selectAge:userModel?.selectAge ?? "", interestedNumber: userModel?.interestedNumber ?? 0, deliveryDate: userModel?.deliveryDate ?? "", success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                self.showAlert(message: message)
                
            }) { (message) in
                self.showAlert(message: message)
            }
             self.navigationController?.popViewController(animated: true)
        }
        else
        {
            performSegue(withIdentifier: SegueIdentity.KSingUpQuestion5, sender: sender)
        }
        
    }
    
    
}
//MARK: Extension for table view
extension SingUpQuestionScreen4:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "questionOptionCell", for: indexPath) as? QuestionOptionCell
        
        if indexPath.row == index {
            cell?.optionImage.image = UIImage(named: "check_new")
        }
        else {
            cell?.optionImage.image = UIImage(named: "radio")
        }
        
        
        cell!.textDescriptionLabel?.text = self.questionData[indexPath.row]
        return cell!
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return  60
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.questionDataValue = self.questionData[indexPath.row]
        index = indexPath.row
        tableView.reloadData()
        
        
    }
    
    
}
