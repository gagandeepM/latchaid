//
//  SignUpQusetionScreen7.swift
//  LatchAid
//
//  Created by Sunil Garg on 02/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class SignUpQusetionScreen7: UIViewController {
    
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var segmentControlObj: UISegmentedControl!
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    @IBOutlet var valueBtnbj: UIButton!
    @IBOutlet var valueBtnLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var zeroBtnChoice: CustomButton!
    
    @IBOutlet var oneToThreeChoiceBtn: CustomButton!
    
    @IBOutlet var fourToSevenChoiceBtn: CustomButton!
    
    @IBOutlet var eightToTenChoiceBtn: CustomButton!
    var isEdit = false
    var isFrom = ""
    var selectedValue = ""
    var selectedIndex = 0
    var objectProfileViewModel = ProfileViewModel()
    var params:[String:Any] = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        
    }
    func setUpUI()
    {
        if isEdit
        {
            self.setSegment()
        }
        else
        {
            self.segmentControlObj.selectedSegmentIndex = 0
            self.selectedValue = "0 Not Interested"
            self.selectedIndex =  self.segmentControlObj.selectedSegmentIndex
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
            let x = 16
            let y = (self.view.bounds.width-32)/11
            let z = y-15/2
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*1) - z
            self.zeroBtnChoice.backgroundColor = CustomColor.buttonColor1
            
        }
        
        
        
        
        
    }
    //MARK: set segment
    func setSegment()
    {
        let x = 16
        let y = (self.view.bounds.width-32)/11
        let z = y-15/2
        self.segmentControlObj.selectedSegmentIndex = userModel?.interestedNumber ?? 0
        self.valueBtnbj.setTitle(String(userModel?.interestedNumber ?? 0), for: .normal)
        self.selectedIndex = self.segmentControlObj.selectedSegmentIndex
        self.stackView.isHidden = true
        if self.segmentControlObj.selectedSegmentIndex  == 0
        {
            self.selectedValue = self.zeroBtnChoice.titleLabel?.text ?? ""
            self.zeroBtnChoice.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.fourToSevenChoiceBtn.backgroundColor = .white
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if self.segmentControlObj.selectedSegmentIndex  < 4
        {
            self.selectedValue = self.oneToThreeChoiceBtn.titleLabel?.text ?? ""
            self.oneToThreeChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.zeroBtnChoice.backgroundColor = .white
            
            self.fourToSevenChoiceBtn.backgroundColor = .white
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if self.segmentControlObj.selectedSegmentIndex  < 8
        {
            self.selectedValue = self.fourToSevenChoiceBtn.titleLabel?.text ?? ""
            self.fourToSevenChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.zeroBtnChoice.backgroundColor = .white
            
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if self.segmentControlObj.selectedSegmentIndex  < 12
        {
            self.selectedValue = self.eightToTenChoiceBtn.titleLabel?.text ?? ""
            self.eightToTenChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.zeroBtnChoice.backgroundColor = .white
            
            self.fourToSevenChoiceBtn.backgroundColor = .white
            
        }
        
        
        if self.segmentControlObj.selectedSegmentIndex == 0
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*1) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if self.segmentControlObj.selectedSegmentIndex == 1
        {
            
            self.valueBtnLeadingConstraint.constant =  CGFloat(x) + (y*2) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if self.segmentControlObj.selectedSegmentIndex == 2
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*3) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
            
        }else if self.segmentControlObj.selectedSegmentIndex == 3
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*4) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if self.segmentControlObj.selectedSegmentIndex == 4
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*5) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if self.segmentControlObj.selectedSegmentIndex == 5
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*6) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if self.segmentControlObj.selectedSegmentIndex == 6
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*7) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if self.segmentControlObj.selectedSegmentIndex == 7
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*8) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if self.segmentControlObj.selectedSegmentIndex == 8
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*9) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if self.segmentControlObj.selectedSegmentIndex == 9
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*10) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*10) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
            
        }
    }
    //MARK: Button actions
    @IBAction func backBtn(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneBtn(_ sender: CustomButton) {
        self.params["interestedForBreastfeeding"] = self.selectedValue
        self.params["interestedNumber"] = self.selectedIndex
        
        print (self.params)
        if isEdit
        {
            
            
            objectProfileViewModel.editMyInfo(location: userModel?.location ?? "", timeForBreastfeeding: userModel?.timeForBreastfeeding ?? "", interestedForBreastfeeding:  self.selectedValue , selectChildAge: userModel?.selectChildAge ?? "", selectAge: userModel?.selectAge ?? "", interestedNumber: self.selectedIndex, deliveryDate: userModel?.deliveryDate ?? "", success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                self.showAlert(message: message)
                
            }) { (message) in
                self.showAlert(message: message)
            }
           self.navigationController?.popViewController(animated: true)
        }
        else{
            
            var messageStr  = ""
         
            if self.selectedValue.isEmpty
            {
                showAlert(message: FieldValidation.kQuestionSelect)
                
                return
            }
            
            if isFrom == "google"
            {
                
                
                self.objUserViewModel.googleSignUP(params:params, success: { (message) in
                    messageStr = message ?? ""
                    
                    self.showAlert(message: message)
                    DispatchQueue.main.async {
                         AppDelegate.sharedDelegate.setUpLogin()
                       // AppDelegate.sharedDelegate.setUpMainController()
                    }
                    
                    
                }, failure: { (message) in
                    self.showAlert(message:message)
                    AppDelegate.sharedDelegate.setUpLogin()
                })
                
            }
            else if isFrom == "facebook"
            {
                
                
                self.objUserViewModel.facebookSignUP(params:params, success: { (message) in
                    messageStr = message ?? ""
                    self.showAlert(message: message)
                    DispatchQueue.main.async {
                        
                        AppDelegate.sharedDelegate.setUpLogin()
                       // AppDelegate.sharedDelegate.setUpMainController()
                        
                    }
                    
                }, failure: { (message) in
                    self.showAlert(message:message)
                    AppDelegate.sharedDelegate.setUpLogin()
                })
                
            }
            else
            {
                objUserViewModel.signUp(params:params, success: { (message) in
                    messageStr = message ?? ""
                    self.showAlert(message: message)
                    DispatchQueue.main.async {
                        AppDelegate.sharedDelegate.setUpLogin()
                    }
                    
                    
                }, failure: {(message) in
                    self.showAlert(message:message)
                    AppDelegate.sharedDelegate.setUpLogin()
                })
                
            }
        }
    }
    
}
//MARK:Extension for segment 
extension SignUpQusetionScreen7
{
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        let x = 16
        let y = (self.view.bounds.width-32)/11
        let z = y-15/2
        self.selectedIndex = sender.selectedSegmentIndex
        if sender.selectedSegmentIndex == 0
        {
            self.selectedValue = self.zeroBtnChoice.titleLabel?.text ?? ""
            self.zeroBtnChoice.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.fourToSevenChoiceBtn.backgroundColor = .white
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if sender.selectedSegmentIndex < 4
        {
            self.selectedValue = self.oneToThreeChoiceBtn.titleLabel?.text ?? ""
            self.oneToThreeChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.zeroBtnChoice.backgroundColor = .white
            
            self.fourToSevenChoiceBtn.backgroundColor = .white
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if sender.selectedSegmentIndex < 8
        {
            self.selectedValue = self.fourToSevenChoiceBtn.titleLabel?.text ?? ""
            self.fourToSevenChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.zeroBtnChoice.backgroundColor = .white
            
            self.eightToTenChoiceBtn.backgroundColor = .white
        }
        else if sender.selectedSegmentIndex < 12
        {
            self.selectedValue = self.eightToTenChoiceBtn.titleLabel?.text ?? ""
            self.eightToTenChoiceBtn.backgroundColor = CustomColor.buttonColor1
            self.oneToThreeChoiceBtn.backgroundColor = .white
            self.zeroBtnChoice.backgroundColor = .white
            
            self.fourToSevenChoiceBtn.backgroundColor = .white
            
        }
        
        if sender.selectedSegmentIndex == 0
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*1) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if sender.selectedSegmentIndex == 1
        {
            
            self.valueBtnLeadingConstraint.constant =  CGFloat(x) + (y*2) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if sender.selectedSegmentIndex == 2
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*3) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
            
        }else if sender.selectedSegmentIndex == 3
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*4) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if sender.selectedSegmentIndex == 4
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*5) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if sender.selectedSegmentIndex == 5
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*6) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if sender.selectedSegmentIndex == 6
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*7) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if sender.selectedSegmentIndex == 7
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*8) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }else if sender.selectedSegmentIndex == 8
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*9) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else if sender.selectedSegmentIndex == 9
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*10) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
        }
        else
        {
            
            self.valueBtnLeadingConstraint.constant = CGFloat(x) + (y*10) - z
            self.valueBtnbj.setTitle(String(self.segmentControlObj.selectedSegmentIndex), for: .normal)
            
        }
        
    }
    
    
    
}
