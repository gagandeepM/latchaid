//
//  SignUpVC.swift


import UIKit

class SignUpVC: UIViewController {
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    
    @IBOutlet weak var nameTF: CustomTextField!
    @IBOutlet weak fileprivate var emailTF:UITextField!
    @IBOutlet weak fileprivate var passwordTF:UITextField!
    @IBOutlet weak fileprivate var confirmPasswordTF:UITextField!
    
    @IBOutlet var termAndCodtionBtn: UIButton!
    @IBOutlet var googleSignInBtn: UIButton!
    
    @IBOutlet var facebookSignInBtn: UIButton!
    var  objectProfileViewModel = ProfileViewModel()
    var localTimeZoneName: String { return TimeZone.current.identifier }
    @IBOutlet weak var signUpBtn: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        self.termAndCodtionBtn.setTitleColor(CustomColor.buttonColor1, for: .normal)
    }
    
    @IBAction func textPrimaryFunctio(_ sender: UITextField) {
        if sender == self.nameTF
        {
            emailTF.becomeFirstResponder()
        }else if sender == emailTF{
             passwordTF.becomeFirstResponder()
        }else if sender == passwordTF
        {
             confirmPasswordTF.becomeFirstResponder()
        }else{
            sender.resignFirstResponder()
        }
        
    }
}

extension SignUpVC{
    
    //MARK:- Facebook Login Action
    @IBAction fileprivate func onClickFacebook(_ sender: UIButton) {
        FacebookManager.shared.facebookLogin(withController:self) { (success,user) in
            
            let params:[String:Any] = ["firstName":user.firstName ?? "","lastName":user.lastName ?? "","email":user.email ?? "","socialId":user.id ?? "","imageUrl":user.profilePic ?? "","gender":"","userName":"","socialType":"facebook","timeZone":self.localTimeZoneName]
            
            DispatchQueue.main.async {
                
                let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen1) as? SingUpQuestionScreen1
                vc?.params = params
                vc?.isFrom = "facebook"
               self.navigationController?.pushViewController(vc!, animated: true)
            }
        }
    }
    
    
    //MARK:- Google Login Action
    @IBAction fileprivate func onClickGoogle(_ sender: UIButton) {
        GoogleLoginManager.shared.GIDSignInUI(from: self, onSuccess: { (user) in
            var firstName:String = ""
            var lastName:String = ""
            let name = user.name
            let whiteSpace = " "
            if let hasWhiteSpace = name?.contains(whiteSpace) {
                var fullNameArr =  name?.components(separatedBy: " ")
                firstName = fullNameArr?[0] ?? ""
                lastName  = fullNameArr?[1] ?? ""
                print ("has whitespace")
            } else {
                firstName = name ?? ""
                lastName = ""
            }
            let params:[String:Any] = ["firstName":firstName,"lastName":lastName,"fullName":"\(firstName) "+" \(lastName)","email":user.email ?? "","socialId":user.userGmailID ?? "","imageUrl":user.hasImage ?? "","gender":"","socialType":"google","timeZone":self.localTimeZoneName]
            
            DispatchQueue.main.async {
                let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen1) as? SingUpQuestionScreen1
                vc?.params = params
                vc?.isFrom = "google"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
        }, onCancel: { (isCancelled) in
            
        }, onFailure: { (error) in
            
        })
        
    }
    //MARK:Back button
    @IBAction func backBtn(_ sender: UIButton) {
        //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: Simple signup
    @IBAction fileprivate func onClickSignUp(_ sender:CustomButton){
        
        if (self.nameTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.KNameEmpty)
        }else if (self.emailTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kEmailEmpty)
            
        }else if !(self.emailTF.text?.isValidEmail)!{
            showAlert(message: FieldValidation.kEmailFormat)
            
        }else if (self.passwordTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kPasswordEmpty)
        }
        else if ((self.passwordTF.text?.length)! < 6)
        {
            showAlert(message: FieldValidation.kPasswordLength)
            
        }
        else if (self.confirmPasswordTF.text?.isEmpty)! {
            showAlert(message: FieldValidation.kConfirmPasswordEmpty)
            
        }
            
        else if self.confirmPasswordTF.text != self.passwordTF.text {
            showAlert(message: FieldValidation.kPasswordNotMatch)
            
        }
        else {
            
            
            objectProfileViewModel.validateEmail(email: self.emailTF.text?.trimWhiteSpace ?? "",onSuccess: { (message) in
                
                let params:[String:Any] = ["email":self.emailTF.text?.trimWhiteSpace ?? "","password":self.passwordTF.text?.trimWhiteSpace ?? "","fullName":self.nameTF.text?.trimWhiteSpace ?? "","socialType":"normal","timeZone":self.localTimeZoneName]
                
                DispatchQueue.main.async {
                    
                    let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen1) as? SingUpQuestionScreen1
                    vc?.params = params
                    vc?.isFrom = "normal"
                    self.navigationController?.pushViewController(vc!, animated: true)
                }
            }) { (message) in
                self.showAlert(message: message)
            }
            
            
            
            
        }
        
        
    }
}

