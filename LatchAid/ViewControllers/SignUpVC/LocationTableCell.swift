//
//  LocationTableCell.swift
//  LatchAid
//
//  Created by Sunil Garg on 29/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class LocationTableCell: UITableViewCell {
    @IBOutlet var placeTextLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
