

import UIKit

class SingUpQuestionScreen2: UIViewController {
    
    @IBOutlet weak var pregnancyBtn: UIButton!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet weak var nextBtn: CustomButton!
    @IBOutlet weak var pickerView: UIDatePicker!
    
    
    var objectProfileViewModel = ProfileViewModel()
    var params:[String:Any] = [String:Any]()
    var isEdit = false
    var isFrom = ""
    var selectedData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        pickerView.datePickerMode = .date
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat =  "MM/dd/yyyy"
        selectedData = dateFormatter.string(from: pickerView.date)
        
        pickerView.setValue(UIColor.white, forKeyPath: "textColor")
        
        if isEdit
        {
            self.stackView.isHidden = true
            self.pregnancyBtn.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "MM/dd/yyyy"
            if let date = dateFormatter.date(from: userModel?.selectChildAge ?? "")
            {
                pickerView.date = date
                selectedData = dateFormatter.string(from: date)
            }
            self.nextBtn.setImage(#imageLiteral(resourceName: "Done"), for: .normal)

            
        }
        
        
        //picke select only last 10 year date
        let calendar = Calendar.current
        var minDateComponent = calendar.dateComponents([.day,.month,.year], from: Date())
        minDateComponent.day = 01
        minDateComponent.month = 01
        minDateComponent.year = minDateComponent.year!-10
        let minDate = calendar.date(from: minDateComponent)
        pickerView.minimumDate = minDate
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let currentDate: NSDate = NSDate()
        pickerView.maximumDate = currentDate as Date
    }
    
    //MARK: Button actions
    
    @IBAction func nextBtn(_ sender: CustomButton) {
        
        
        if self.selectedData.isEmpty
        {
            showAlert(message: FieldValidation.kQuestionSelect)
            return
        }
        if isEdit
        {
            
            
            objectProfileViewModel.editMyInfo(location: userModel?.location ?? "", timeForBreastfeeding: userModel?.timeForBreastfeeding ?? "", interestedForBreastfeeding: userModel?.interestedForBreastfeeding ?? "", selectChildAge: self.selectedData ?? "", selectAge: userModel?.selectAge ?? "", interestedNumber: userModel?.interestedNumber ?? 0, deliveryDate: userModel?.deliveryDate ?? "", success: { (message) in
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                self.showAlert(message: message)
                
            }) { (message) in
                self.showAlert(message: message)
            }
           self.navigationController?.popViewController(animated: true)
        }
        else
        {
            performSegue(withIdentifier: SegueIdentity.KSingUpQuestion8, sender: sender)
        }
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func pickerValueChanged(_ sender: UIDatePicker) {
        
        let formatter = DateFormatter()
        formatter.dateFormat =  "MM/dd/yyyy"
        selectedData = formatter.string(from: sender.date)
        print(selectedData)
        
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion8) {
            
            self.params["selectChildAge"] = self.selectedData ?? ""
            let vc = segue.destination as! SingUpQuestionScreen3
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
        else if segue.identifier == "pragnantSegue"
            
        {
            
            let vc = segue.destination as! SingUpQuestionScreen8
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
    }
    
    
}





