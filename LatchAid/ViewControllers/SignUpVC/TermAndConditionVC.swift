//
//  TermAndConditionVC.swift
//  LatchAid
//
//  Created by Sunil Garg on 09/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class TermAndConditionVC: UIViewController,UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "http://35.177.246.53/terms.html")
        
        let urlRequest = URLRequest(url: url!)
        self.webView.loadRequest(urlRequest)
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    // show indicator
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        ServerManager.shared.showHud()
    }
    // hide indicator
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        ServerManager.shared.hideHud()
        
    }
    // hide indicator
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        ServerManager.shared.hideHud()
    }
    
    
}
