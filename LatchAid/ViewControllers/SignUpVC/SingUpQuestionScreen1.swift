//
//  SingUpQuestionScreen1.swift
//  LatchAid
//
//  Created by Anuj Sharma on 09/04/19.

//

import UIKit

class SingUpQuestionScreen1: UIViewController {
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet var pickerView: UIPickerView!
    var isEdit = false
    var pickerData: [String] = [String]()
    var params:[String:Any] = [String:Any]()
    var isFrom = ""
    var selectedData:String?
    var objectProfileViewModel = ProfileViewModel()
    
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var backBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        
        // static data for ages
        
        pickerData = ["10-15 Years", "16-20 Years", "21-25 Years", "26-30 Years", "31-35 Years", "36-40 Years","41-45 Years", "46-50 Years", "51-55 Years", "56-60 Years", "61-65 Years", "66-70 Years","71-75 Years","76-80 Years"]
        
        if isEdit
        {
            self.stackView.isHidden = true
            let data =  pickerData.index(of: userModel?.selectAge ?? "")
            pickerView.selectRow(data!, inComponent: 0, animated: true)
            self.continueBtn.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
            self.selectedData = "PreSet"
            
        }else{
            self.stackView.isHidden = true
            let data =  pickerData.index(of: "31-35 Years")
            pickerView.selectRow(data!, inComponent: 0, animated: true)
            selectedData = "31-35 Years"
        }
    }
    //MARK: Button actions
    @IBAction func continueBTNAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            
            
            if self.selectedData == "PreSet"
            {
                self.navigationController?.popViewController(animated: true)
            }
            else if self.selectedData ==  nil
            {
                self.showAlert(message: FieldValidation.kQuestionSelect)
                return
            }else if self.isEdit
            {
                
                
                self.objectProfileViewModel.editMyInfo(location: userModel?.location ?? "", timeForBreastfeeding: userModel?.timeForBreastfeeding ?? "", interestedForBreastfeeding: userModel?.interestedForBreastfeeding ?? "", selectChildAge: userModel?.selectChildAge ?? "", selectAge: self.selectedData ?? "", interestedNumber: userModel?.interestedNumber ?? 0, deliveryDate: userModel?.deliveryDate ?? "", success: { (message) in
                    self.showAlert(message: message)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                }) { (message) in
                    self.showAlert(message: message)
                }
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                self.performSegue(withIdentifier: SegueIdentity.KSingUpQuestion1, sender: sender)
            }
        }
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
    }
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == SegueIdentity.KSingUpQuestion1) {
            
            self.params["selectAge"] = self.selectedData
            let vc = segue.destination as! SingUpQuestionScreen2
            vc.params = self.params
            vc.isFrom = self.isFrom
            print(params)
        }
    }
    
    
}
//MARK: Extension for picker view
extension SingUpQuestionScreen1:UIPickerViewDelegate,UIPickerViewDataSource
{
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 150
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerData.count
    }
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel: UILabel? = (view as? UILabel)
        
        pickerLabel = UILabel()
        pickerLabel?.font = UIFont(name: "Times New Roman", size: 60.0)
        pickerLabel?.textAlignment = .center
        pickerLabel?.textColor = UIColor.white
        
        pickerLabel?.text = pickerData[row]
        //harsh lock it 
//        if row == 0
//        {
//            selectedData = pickerData[row]
//        }
        
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        
        if let dataValue = pickerData[row] as? String
        {
            selectedData = dataValue
        }
    }
    
}
