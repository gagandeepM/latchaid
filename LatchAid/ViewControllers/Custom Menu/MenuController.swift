//
//  MenuController.swift
//  LatchAid
//
//  Created by gagandeepmishra on 23/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

private let reuseIdentifier = "menuCell"

class MenuController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var KArray:[[String:Any]] = [["imageName":  "ic_video"  , "title": "Camera"],
                                 ["imageName": "ic_gallery" , "title": "Gallery"],
                                 ["imageName":  "ic_file" , "title": "Files"]]
    var didSelectMenuItem:((Any)->Void)?
//    var colView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layer.cornerRadius = 10
       
        

    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView!.register(MenuCollectionCell.self, forCellWithReuseIdentifier: reuseIdentifier)
    }
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return KArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? MenuCollectionCell
//        cell?.menuTitle.text = self.KArray[indexPath.row]["title"] as? String ?? ""
//        cell?.menuImage.image = UIImage(named: self.KArray[indexPath.row]["imageName"] as? String ?? "")
        cell?.showData(self.KArray[indexPath.row])
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        self.didSelectMenuItem()
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
