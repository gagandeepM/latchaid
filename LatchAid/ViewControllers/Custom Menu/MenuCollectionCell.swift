//
//  MenuCollectionCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 23/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class MenuCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var menuTitle: UILabel!
    @IBOutlet weak var menuImage: UIImageView!
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showData(_ data: [String:Any]) {
        
        self.menuTitle.text = data["title"] as? String ?? ""
    }
}
