//
//  CustomMenuVC.swift
//  LatchAid
//
//  Created by gagandeepmishra on 25/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
protocol CustomMenuVCDelegate
{
    func imageUpload(image:UIImage)
    func cancelBtn()
    
}
class CustomMenuVC: UIViewController,UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var galleryBtn: UIButton!
    var delegate:CustomMenuVCDelegate?
      let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .clear
        picker.delegate = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func camerBtnAction(_ sender: UIButton) {
    self.openCamera()
      
    }
    
    @IBAction func galleryBtnAction(_ sender: UIButton) {
      self.openGallary()
    
    }
    
    
    @IBAction func cancelBtn(_ sender: UIButton) {
        
        self.delegate?.cancelBtn()
         self.dismiss(animated: true, completion: nil)
    }
    //open document files
    func openFileDocument()
    {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
    //file document delegate methods
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        let url =  try? String(contentsOf: myURL)
        
//        self.delegate?.imageUpload(image: <#T##UIImage#>)
    }
    // open gallery for saved images
    func openGallary(){
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    // open camera for images
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.sourceType = UIImagePickerController.SourceType.camera
            self .present(picker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
}
extension CustomMenuVC {
    
    
    //MARK:UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            self.delegate?.imageUpload(image: selectedImage!)
           
            picker.dismiss(animated: true, completion: nil)
              self.dismiss(animated: true, completion: nil)
            
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            self.delegate?.imageUpload(image: selectedImage!)
            
            picker.dismiss(animated: true, completion: nil)
              self.dismiss(animated: true, completion: nil)
          
        }
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        print("picker cancel.")
        self.dismiss(animated: true, completion: nil)
    }
}
