//
//  ProfileVC.swift
//  LatchAid
//
//  Created by Sunil Garg on 04/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet var objProfileViewModel: ProfileViewModel!
    @IBOutlet var objUserViewModel: UserViewModel!
    @IBOutlet weak var userName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUI()
    }
    func setUI()
    {
        guard let viewControllerCount = navigationController?.viewControllers.count else{return}
        if viewControllerCount > 0
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        DispatchQueue.main.async {
            self.view.backgroundColor = CustomColor.ViewColor
            self.tableView.tableFooterView = UIView(frame: .zero)
            self.userImage.makeRounded()
            if userModel?.userImage?.isEmpty ?? false
            {
                self.userImage.image = UIImage(named: "user")
            }
            else
            {
                self.userImage.kf.setImage(with: URL(string: userModel?.userImage ?? ""))
            }
            self.userEmail.text = userModel?.email
            self.userName.text = userModel?.fullName ?? ""
            print(userModel?.userImage)
            self.userImage.makeRounded()
        }
        
        
    }
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell()
        if indexPath.row == 0 {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "infoCell")
            cell.textLabel?.text = "My Information"
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 1 {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "shareCell")
            cell.textLabel?.text = "Share LatchAid with your friends"
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
              cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 2 {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "feedbackCell")
            cell.textLabel?.text = "Leave your feedback"
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
              cell.selectionStyle = .none
            return cell
        }
        else if indexPath.row == 3 {
            cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "changepasswordCell")
            cell.textLabel?.text = "Change password"
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
              cell.selectionStyle = .none
            return cell
        }
        else
        {
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0:
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyInformationVC") as? MyInformationVC
            
           self.navigationController?.pushViewController(vc!, animated: true)
            break
            
        case 1:
            print("row1")
            break
            
        case 2:
            print("row2")
            break
            
        default:
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentity.KChangePasswordVC) as? ChangePasswordVC
           self.navigationController?.pushViewController(vc!, animated: true)
            break
        }
    }
    
    @IBAction func logoutBtn(_ sender: UIButton) {
        objUserViewModel.logOut(success: { (message) in
            
            self.showAlert(message: message)
            AppDelegate.sharedDelegate.logoutUser()
            
            
        },failure:{(message) in
            
            self.showAlert(message: message)
        } )
    }
    
}
