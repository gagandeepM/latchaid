//
//  MyInformationVC.swift
//  LatchAid
//
//  Created by Sunil Garg on 20/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class MyInformationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var objProfileViewModel: ProfileViewModel!
    @IBOutlet var objUserViewModel: UserViewModel!
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        self.tableView.tableFooterView = UIView(frame: .zero)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateData(_:)), name: NSNotification.Name(rawValue: "updateData"), object: nil)
    }
    
    @objc func updateData(_ notification: NSNotification) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self)
    }
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Age") as? MyInfoCell
            cell?.ageLbl?.text = "Age"
            if let age = userModel?.selectAge
            {
                cell?.ageValueLbl.text = age
            }
            
            cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell!
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BabyAge") as? MyInfoCell
            
            if userModel?.selectChildAge?.isEmpty ?? false
            {
                cell?.babyAgeLbl?.text = "Pregnancy Due Date"
                if let age = userModel?.deliveryDate
                {
                    cell?.babyAgeValueLbl.text = age
                }
            }
            else
            {
                cell?.babyAgeLbl?.text = "Baby's birthday"
                if let age = userModel?.selectChildAge
                {
                    cell?.babyAgeValueLbl.text = age
                }
            }
            cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell!
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Location") as? MyInfoCell
            cell?.locationLbl?.text = "Location"
            if let location = userModel?.location
            {
                cell?.locationValueLbl.text = location
            }
            
            cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell!
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Bexperience") as? MyInfoCell
            cell?.breastfeedingExperienceLbl?.text = "Breastfeeding experience"
            if let bExperience = userModel?.timeForBreastfeeding
            {
                cell?.breastfeedingExperienceValueLbl.text = bExperience
            }
            
            cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell!
        }
        else if indexPath.row == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Binterest") as? MyInfoCell
            cell?.breastfeedingInterestLbl?.text = "Breastfeeding interest"
            if let bInterest = userModel?.interestedForBreastfeeding
            {
                cell?.breastfeedingInterestValueLbl.text = bInterest
            }
            
            cell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            return cell!
            
            
        }
        else
        {
            return UITableViewCell()
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0:
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen1) as? SingUpQuestionScreen1
            vc?.isEdit = true
           self.navigationController?.pushViewController(vc!, animated: true)
            break
            
        case 1:
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            if userModel?.selectChildAge?.isEmpty ?? false
            {
                //Please select your due date
                let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen8) as? SingUpQuestionScreen8
                
                vc?.isEdit = true
               self.navigationController?.pushViewController(vc!, animated: true)
            }
            else
            {
                // Please select your baby's birthday
                let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen2) as? SingUpQuestionScreen2
                vc?.isEdit = true
               self.navigationController?.pushViewController(vc!, animated: true)
            }
            
            break
            
        case 2:
            //location
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen3) as? SingUpQuestionScreen3
            vc?.isEdit = true
           self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 3 :
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen4) as? SingUpQuestionScreen4
            vc?.isEdit = true
          self.navigationController?.pushViewController(vc!, animated: true)
        case 4 :
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen7) as? SignUpQusetionScreen7
            vc?.isEdit = true
            self.navigationController?.pushViewController(vc!, animated: true)
        default:
            
            let storyboard = UIStoryboard.init(name: StoryBoardIdentity.KSingUpQuestionScreen, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KSingUpQuestionScreen1) as? SingUpQuestionScreen1
            vc?.isEdit = true
           self.navigationController?.pushViewController(vc!, animated: true)
            break
        }
    }
    
    
    //MARK: Back button
    @IBAction func backBtn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
}
