//
//  MyInfoCell.swift
//  LatchAid
//
//  Created by Sunil Garg on 20/05/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class MyInfoCell: UITableViewCell {
    
    
    @IBOutlet var ageLbl: UILabel!
    
    @IBOutlet var babyAgeLbl: UILabel!
    
    @IBOutlet var locationLbl: UILabel!
    
    @IBOutlet var breastfeedingExperienceLbl: UILabel!
    
    @IBOutlet var breastfeedingInterestLbl: UILabel!
    @IBOutlet var ageValueLbl: UILabel!
    
    @IBOutlet var babyAgeValueLbl: UILabel!
    
    @IBOutlet var locationValueLbl: UILabel!
    
    @IBOutlet var breastfeedingExperienceValueLbl: UILabel!
    
    @IBOutlet var breastfeedingInterestValueLbl: UILabel!
    
   
    
}
