//
//  EditProfileVC.swift
//  LatchAid
//
//  Created by Anuj Sharma on 12/04/19.



import UIKit

class EditProfileVC: UIViewController {
    
    @IBOutlet var objProfileViewModel: ProfileViewModel!
    @IBOutlet var objUserModel: UserViewModel!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UITextField!
    var image:UIImage?
    var imagePicker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
    }
    func setUI()
    {
        self.view.layoutIfNeeded()
        self.view.backgroundColor = CustomColor.ViewColor
        self.imagePicker.delegate = self
        userImage.layer.masksToBounds = false
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        self.userEmail.text = userModel?.email ?? ""
        self.userName.text = userModel?.fullName ?? ""
        guard let file = userModel?.userImage,let imageURL = URL(string:file) else {return}
        
        self.userImage.kf.setImage(with: imageURL)
        
    }
    //MARK: Button action
    @IBAction func backBtn(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    @IBAction func saveBTn(_ sender: UIButton) {
        
        if image == nil
        {
            if let file = userModel?.userImage,let imageURL = URL(string:file)
            {
                let data = try? Data(contentsOf: imageURL)
                image = UIImage(data: data!)
            }
            else{
                
            }
        }
        
        var messageAlert = ""
        
        
        if (userName.text?.isEmpty)! || (userName.text?.trimWhiteSpace == ""){
            showAlert(message: FieldValidation.KNameEmpty)
        }
        else{
            
            if image == nil
            {
                image = UIImage(named: "user")
                
            }
            
            objProfileViewModel.imageUpload(image: image!  , fullName: userName.text ?? "", success: { (message) in
                
                messageAlert = message ?? ""
                
                self.showAlert(message: message)
                
                
            }, failure:{ (message) in
                self.showAlert(message: message)
            })
            
        }
        
    }
    //MARK: Image upload method
    @IBAction func upLoadImageBtn(_ sender: UIButton) {
        
        var alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            
            self .present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    func openGallary()
    {
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
}
//MARK: Image upload delegate method
extension EditProfileVC:UINavigationControllerDelegate, UIImagePickerControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
        
    {
        
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        self.image = image
        self.userImage.image = image
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
}
