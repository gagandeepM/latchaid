//
//  ProfileTableViewController.swift
//  LatchAid
//
//  Created by Sunil Garg on 11/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileTableViewController: UITableViewController {
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet var objProfileViewModel: ProfileViewModel!
    @IBOutlet var objUserViewModel: UserViewModel!
    @IBOutlet weak var userName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUI()
    }
    
    func setUI()
    {
        self.view.backgroundColor = CustomColor.ViewColor
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.userImage.makeRounded()
        if userModel?.userImage == ""
        {
            self.userImage.image = UIImage(named: "user")
        }
        
        self.userEmail.text = userModel?.email
        self.userName.text = userModel?.fullName ?? "" 
        print(userModel?.userImage)
        guard let file = userModel?.userImage,let imageURL = URL(string:file) else {return}
        
        self.userImage.kf.setImage(with: imageURL)
        
        
    }
    // MARK: - Table view data source
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0:
            print("row0")
            break
            
        case 1:
            print("row1")
            break
            
        case 2:
            print("row2")
            break
        case 3:
            print("row3")
            break
        case 4:
            print("row4")
            break
        case 5:
            print("row5")
            break
        case 6:
            print("row6")
            break
        case 7:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentity.KChangePasswordVC) as? ChangePasswordVC
          self.navigationController?.pushViewController(vc!, animated: true)
            break
        case 8:
            
            
            break
        default:
            objUserViewModel.logOut(success: { (message) in
                
                self.showAlert(message: message)
                AppDelegate.sharedDelegate.logoutUser()
                
            }, failure:{ (message) in
                self.showAlert(message: message)
            })
            break
        }
    }
    @IBAction func editBtn(_ sender: UIBarButtonItem) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: StoryBoardIdentity.KEditProfileVC) as? EditProfileVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func improveAccountBtn(_ sender: UIButton) {
    }
    
}
