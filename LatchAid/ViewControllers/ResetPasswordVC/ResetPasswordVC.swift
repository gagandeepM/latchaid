//
//  ResetPasswordVC.swift



import UIKit

class ResetPasswordVC: UIViewController {
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    //MARK:- Outlets
    @IBOutlet weak fileprivate var otpFirstTF:CustomTextField!
    @IBOutlet weak fileprivate var otpSecondTF:CustomTextField!
    @IBOutlet weak fileprivate var otpThirdTF:CustomTextField!
    @IBOutlet weak fileprivate var otpFourthTF:CustomTextField!
    @IBOutlet weak fileprivate var passwordTF: CustomTextField!
    @IBOutlet weak fileprivate var confirmPasswordTF: CustomTextField!
    @IBOutlet weak fileprivate var otpSuceesImgView: UIImageView!
    @IBOutlet weak fileprivate var resetLoginBTN: CustomButton!
    @IBOutlet weak fileprivate var  createPasswordLBL: UILabel!
    @IBOutlet weak var sendEmailBTN: UIButton!
    @IBOutlet weak fileprivate var sendEmailHeightConstraint: NSLayoutConstraint!
    fileprivate var isEmptyField:Bool = false{
        didSet{
            if isEmptyField {
                otpFirstTF.text =  ""
                otpSecondTF.text = ""
                otpThirdTF.text =  ""
                otpFourthTF.text  =  ""
            }
            
        }
    }
    
    //MARK:- Properties
    var otp:String?
    var userEmail:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction fileprivate func textFieldDidChanged(_ sender: CustomTextField) {
        let text = sender.text
        if text?.utf16.count == 1{
            switch sender{
            case otpFirstTF:
                otpSecondTF.becomeFirstResponder()
            case otpSecondTF:
                otpThirdTF.becomeFirstResponder()
            case otpThirdTF:
                otpFourthTF.becomeFirstResponder()
            case otpFourthTF:
                otpFourthTF.resignFirstResponder()
            default:
                break
            }
            //self.verifyOtp()
            
        }
        else if text?.utf16.count == 0{
            switch sender{
            case otpFirstTF:
                otpFirstTF.resignFirstResponder()
            case otpSecondTF:
                otpFirstTF.becomeFirstResponder()
            case otpThirdTF:
                otpSecondTF.becomeFirstResponder()
            case otpFourthTF:
                otpThirdTF.becomeFirstResponder()
            default:
                break
            }
            //self.verifyOtp()
        }
        else  {
            
        }
    }
    
    
}

//MARK:- User Defined Functions
extension ResetPasswordVC{
    
    fileprivate func resetPasswordEnable(userInteraction:Bool,alpha:CGFloat){
        passwordTF.isUserInteractionEnabled = userInteraction
        confirmPasswordTF.isUserInteractionEnabled = userInteraction
        resetLoginBTN.isUserInteractionEnabled = userInteraction
        passwordTF.alpha = alpha
        confirmPasswordTF.alpha = alpha
        resetLoginBTN.alpha = alpha
        createPasswordLBL.alpha = alpha
    }
    fileprivate func otpFieldDisable(userinteraction:Bool){
        self.otpFirstTF.isUserInteractionEnabled = userinteraction
        self.otpSecondTF.isUserInteractionEnabled = userinteraction
        self.otpThirdTF.isUserInteractionEnabled = userinteraction
        self.otpFourthTF.isUserInteractionEnabled = userinteraction
    }
}

//MARK:- Actions

extension ResetPasswordVC{
    
    @IBAction fileprivate func onClickReset(_ sender: UIButton) {
        
        guard let otp = otp,let confirmPassword = confirmPasswordTF.text,let password = passwordTF.text else{return}
        objUserViewModel.resetPassword(otp: otp, confirmPassword:confirmPassword, password: password, success: { (message) in
            
            self.showAlert(message: message, completion: { (index) in
                self.navigationController?.popToRootViewController(animated: true)
            })
            
            
        }, failure: {(message) in
            self.showAlert(message: message)
        })
    }
    
    
    @IBAction fileprivate func onClickSendEmailAgain(_ sender: UIButton) {
        
        objUserViewModel.forgotPassword(email: userEmail, success: { (message) in
            
            alertMessage = message ?? ""
            
        }, failure: {(message) in
            self.showAlert(message: message)
        })
    }
}
