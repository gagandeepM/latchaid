//
//  ForgotPasswordVC.swift
import UIKit

class ForgotPasswordVC: UIViewController {
    @IBOutlet fileprivate var objUserViewModel: UserViewModel!
    @IBOutlet weak fileprivate var emailTF:UITextField!
    
    @IBOutlet weak var forgotBtn: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = CustomColor.ViewColor
        self.forgotBtn.tintColor = .white
        self.forgotBtn.backgroundColor = CustomColor.buttonColor1
    }
    
    @IBAction func textPrimaryFunctio(_ sender: UITextField) {
        sender.resignFirstResponder()
    }

    
}
//MARK: - Actions
extension ForgotPasswordVC{
    
    @IBAction fileprivate func onClickForgotPassword(_ sender:CustomButton){
        if (emailTF.text?.isEmpty)!{
            showAlert(message: FieldValidation.kEmailEmpty)
            
        }else if !((emailTF.text?.isValidEmail)!){
            showAlert(message: FieldValidation.kEmailFormat)
            
        }else{
            objUserViewModel.forgotPassword(email:emailTF.text ?? "", success: { (message) in
                
//                DispatchQueue.main.async {
//                    if message == "Password reset link has been successfully sent to your email id."
//                    {
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                    self.showAlert(message: message)
//
//                }
                
            }, failure: { (message) in
                
//                self.showAlert(message: message)
                self.showAlert(title: "", message: message, completion: { (Int) in
                    if message == "Password reset link has been successfully sent to your email id."
                    {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                

            })
        }
    }
    @IBAction func backBtn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
}
