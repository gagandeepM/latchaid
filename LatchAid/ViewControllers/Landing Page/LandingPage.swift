//
//  LandingPage.swift
//  LatchAid
//
//  Created by Anuj Sharma on 08/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import FirebaseInstanceID
import FirebaseMessaging
class LandingPage: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var signInBTN: CustomButton!
    
    @IBOutlet weak var signUpBTN: CustomButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        if  let fcmToken = FIRInstanceID.instanceID().token()
        {
             instanceToken = fcmToken
        }
        else{
            instanceToken = FIRInstanceID.instanceID().token() ?? ""
            if instanceToken == ""
            {
                 instanceToken = FIRInstanceID.instanceID().token() ?? ""
            }
        }
        
       self.view.backgroundColor = CustomColor.ViewColor
        self.signInBTN.backgroundColor = CustomColor.buttonColor1
         self.signUpBTN.backgroundColor = CustomColor.buttonColor1
        self.signInBTN.tintColor = .black
        self.signUpBTN.tintColor = .black
      
        
        
    }
    
    @IBAction func btnAction(_ sender: UIButton){
		        if sender.tag == 1
        {
            performSegue(withIdentifier: "signUp" , sender: sender)
        }else if sender.tag == 2{
            performSegue(withIdentifier:  "SignIN", sender: sender)
        }
        
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "signUp") {
            
            segue.destination as! SignUpVC
        }
        else if (segue.identifier == "SignIN")
        {
            segue.destination as! ViewController
        }
    }
    
    
}
