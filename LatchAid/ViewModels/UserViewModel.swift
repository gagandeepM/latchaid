//
//  UserViewModel.swift


import UIKit
typealias OnSuccess = (_ successMessage:String?)-> Void
typealias OnFailure = (_ successMessage:String)-> Void
class UserViewModel: NSObject {
    var userId:String?
    
    //MARK:- Call Native Login API
    func nativeLogin(email:String = "",password:String = "",success:@escaping OnSuccess,failure:@escaping OnFailure){
        
        
        if  ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
           
            let params:[String:Any] = ["email":email,"password":password,"socialType":"normal","deviceToken":ldDeviceToken]
            ServerManager.shared.httpPost(request: ApiName.login.api(environment: kApiEnvironment), params: params, successHandler: {
                (responseData) in
                
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    print(responseData)
                    
                    
                    guard let response = responseData.JKDecoder(LoginReponseModel.self) else{
                           alertMessage = "Error!"
                        return
                        
                    }
                    print("THE RESPONSE IS THE-------------->",response)
                    guard let status = response.code else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        userModel = user
                        if let accessToken = user.accessToken{
                            UserDefaults.Default(setObject: accessToken, forKey: kAuthTokenKey)
                        }
                        guard let userID = response.user?.userId else{return}
                        self.userId = userID
                        success(message)
                        break
                        
                    case 400:
                        alertMessage = response.message ?? ""
                        failure(message)
                        break
                        
                    default:
                        
                        alertMessage = response.message ?? ""
                        failure(message)
                        break
                    }
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
            
        }
    }
    //MARK:- Call Facebook Login API
    
    func facebookLogin(params:[String:Any],success:@escaping OnSuccess,failure:@escaping OnFailure) {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
          
            ServerManager.shared.httpPost(request:ApiName.login.api(environment: kApiEnvironment), params: params, successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{return}
                guard let status = response.code else{return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    guard let user = response.user else{return}
                    userModel = user
                    guard let token = user.accessToken else{return}
                    accessToken = token
                    success(message)
                    break
                    
                    
                default:
                    alertMessage = response.message ?? ""
                    failure(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
            
        }
    }
    
    //MARK:- Call Google Login API
    
    
    
func googleLogin(params:[String:Any],success:@escaping OnSuccess,failure:@escaping OnFailure) {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            
            ServerManager.shared.httpPost(request:ApiName.login.api(environment: kApiEnvironment), params: params, successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{return}
                guard let status = response.code else{return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    guard let user = response.user else{return}
                    userModel = user
                    print(userModel?.fullName)
                    guard let token = user.accessToken else{return}
                    accessToken = token
                    success(message)
                    break
                default:
                    alertMessage = response.message ?? ""
                    failure(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
            
        }
    }
    
    //MARK:- Call Facebook Login API
    
    func facebookSignUP(params:[String:Any],success:@escaping OnSuccess,failure:@escaping OnFailure) {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            ServerManager.shared.httpPost(request:ApiName.signUp.api(environment: kApiEnvironment), params: params, successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{
                    success("SignUP has been completed successfully.")
                    return
                }
                guard let status = response.code else{return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    guard let user = response.user else{
                         success(message)
                        return}
                    userModel = user
                    guard let token = user.accessToken else{return}
                    accessToken = token
                    success(message)
                    break
                    
                    
                default:
                    alertMessage = response.message ?? ""
                    failure(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
            //}
        }
    }
    
    //MARK:- Call Google Login API
    
    func googleSignUP(params:[String:Any],success:@escaping OnSuccess,failure:@escaping OnFailure) {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            
            ServerManager.shared.httpPost(request:ApiName.signUp.api(environment: kApiEnvironment), params: params, successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{
                    
                    return
                }
                guard let status = response.code else{return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    guard let user = response.user else{
                        success(message)
                        return}
                    userModel = user
                    print(userModel?.fullName)
                    guard let token = user.accessToken else{return}
                    accessToken = token
                    success(message)
                    break
                default:
                    alertMessage = response.message ?? ""
                    failure(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
        }
    }
    
    //MARK:- Call Sign UP API
    
    func signUp(params:[String:Any],success:@escaping OnSuccess,failure:@escaping OnFailure)  {
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            ServerManager.shared.httpPost(request:ApiName.signUp.api(environment: kApiEnvironment), params: params, successHandler: { (responseData:Data) in
                
                print(responseData)
                
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{return}
                guard let status = response.code else{
                    print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    success(message)
                    break
                default:
                    alertMessage = message
                    failure(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
        }
        //        }
        
    }
    
    
    //MARK:- Call Forgot Password API
    
    func forgotPassword(email:String,success:@escaping OnSuccess,failure:@escaping OnFailure){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params:[String:Any] = ["email":email]
            ServerManager.shared.httpPost(request:ApiName.forgotPassword.api(environment: kApiEnvironment), params: params, successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(LoginReponseModel.self) else{return}
                    guard let status = response.code else{return}
                    switch status{
                    case 200:
                        success(response.message ?? "")
                        
                        break
                        
                    default:
                        alertMessage = response.message ?? ""
                        failure(response.message ?? "" )
                        break
                    }
                }
                
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
        }
        
    }
    
    
    //MARK:- Call Reset Password API
    func resetPassword(otp:String,confirmPassword:String,password:String,success:@escaping OnSuccess,failure:@escaping OnFailure){
        if password.isEmpty{
            alertMessage = FieldValidation.kPasswordEmpty
        }else if password.count < 6 || password.count > 20{
            alertMessage = FieldValidation.kPasswordLength
        }
            
        else if confirmPassword.isEmpty{
            alertMessage = FieldValidation.kConfirmPasswordEmpty}
        else if password != confirmPassword{
            alertMessage = FieldValidation.kPasswordNotMatch
        }else{
            
            if ServerManager.shared.CheckNetwork(){
                ServerManager.shared.showHud()
                let params:[String:Any] = ["userId":self.userId ?? "","otp":otp,"password":password]
                ServerManager.shared.httpPost(request:ApiName.resetPassword.api(environment:kApiEnvironment), params: params, successHandler: { (response:Data) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(LoginReponseModel.self) else{return}
                        guard let status = response.code else{return}
                        switch status{
                        case 200:
                            success(response.message ?? "")
                            break
                        default:
                            alertMessage = response.message ?? ""
                            failure(response.message ?? "")
                            break
                        }
                    }
                }, failureHandler: { (error) in
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        alertMessage = error?.localizedDescription
                        failure("Something went wrong")
                    }
                })
            }
        }
    }
    
    //MARK:- Verify OTP
    
    func logOut(success:@escaping OnSuccess,failure:@escaping OnFailure){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request: ApiName.logOut.api(environment: kApiEnvironment), params: nil, headers:ServerManager.shared.apiHeaders,successHandler:
                {
                    (response:Data) in
                    
                    
                    DispatchQueue.main.async {
                        ServerManager.shared.hideHud()
                        guard let response = response.JKDecoder(LoginReponseModel.self) else{
                            return
                            
                            
                        }
                        guard let status = response.code else{return}
                        switch status{
                        case 200:
                            
                            success(response.message ?? "")
                            
                            break
                            
                        default:
                            alertMessage = response.message ?? ""
                            failure(response.message ?? "")
                            break
                        }
                    }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    print(error)
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
            
        }
    }
    
}
