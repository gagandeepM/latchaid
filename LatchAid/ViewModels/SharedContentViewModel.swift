//
//  SharedContentViewModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
class SharedContentViewModel:NSObject {

    fileprivate var mediaData:[MediaFiles] = [MediaFiles]()
    fileprivate var audioData:[AudioFiles] = [AudioFiles]()
    func getSharedContent(channelId:String,userId:String,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        print(channelId,userId)
        if ServerManager.shared.CheckNetwork()
        {
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request: ApiName.sharedContent.api(environment:kApiEnvironment) + "\(channelId)" + "&userId=\(userId)" , params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(SharedContent.self) else{
                    return
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.statusCode else{print("+++++++++++++ \(String(describing: response.statusCode))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.mediaData = response.mediaFiles
                    self.audioData = response.audioFiles
                    print(self.mediaData.count,self.audioData.count)
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
    }


    func cellRowIndexAtForMedia(at indexPath:IndexPath) -> MediaFiles
    {
        return mediaData[indexPath.row]
    }
    func cellRowIndexAtForAudio(at indexPath:IndexPath) -> AudioFiles
    {
        return audioData[indexPath.row]
    }
    func numberOfRowsForMedia(InSection section:Int) -> Int {

        return self.mediaData.count
    }
    func numberOfRowsForAudio(InSection section:Int) -> Int {

        return self.audioData.count
    }
    func didSelectRowAtForMedia(at indexPath:IndexPath) -> String {
        return mediaData[indexPath.row].mediaFile ?? ""
    }
}
