//
//  NotificationViewModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 29/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
enum NotificationType:String {
    case chat
    case report
}
class NotificationViewModel:NSObject
{
    fileprivate var notificationData:[Notification]  = [Notification] ()
    let channelData = ChannelsListViewModel()
    override init() {
        super.init()
    }
    var model:LDNotification?
    func parseData(decoder data:Data) {
       
        model =  data.JKDecoder(LDNotification.self)
       
    }
    var channelObj:ChannelsList?{
        guard let id = channelId else {
            return nil
        }
        return ChannelsList(channelId:id)
    }
    
    //    var type:NotificationType?{
    //        return NotificationType(rawValue: self.model?.notiType)
    //    }
    var channelId:String?
    {
        return model?.channelID
    }
  
    func getNotfication(completion: @escaping (_ Successmessage: Bool) -> Void)
    {
        if ServerManager.shared.CheckNetwork()
        {
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request: ApiName.getNotification.api(environment:kApiEnvironment), params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                self.notificationData.removeAll()
                guard let response = responseData.JKDecoder(NotificationResponse.self) else{
                    
                    return
                }
                
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.statusCode else{print("+++++++++++++ \(String(describing: response.statusCode))")
                    return
                }
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.notificationData = response.notifications
                   
                    completion(true)
                    break
                default:
                    self.notificationData = []
                    alertMessage = message
                   completion(false)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    self.notificationData = []
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    completion(false)
                }
            })
        }
    }
    
}
extension NotificationViewModel
{
    func numberOfRow() -> Int
    {
        print(self.notificationData.count)
        return self.notificationData.count
        
    }
    
    func cellRowAt(_ indexPath:IndexPath) -> Notification
    {
        return self.notificationData[indexPath.row]
        
        
    }
    func didSelectForRowAt(at indexPath:IndexPath) -> String {
        
        return self.notificationData[indexPath.row].channelNotification?.channelId ?? ""
    }
    func didSelectForRowAtWithNotificationModel(at indexPath:IndexPath) -> Notification {
        
        return self.notificationData[indexPath.row]
    }
}
struct LDNotification:Mappable {
    //let id:String?
    let channelID:String
    enum CodingKeys:String,CodingKey {
        case channelID = "channelId"
    }
    // let notiType:String
    
}

