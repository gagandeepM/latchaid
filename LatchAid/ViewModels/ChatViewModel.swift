////
////  ChatViewModel.swift
////  LatchAid
////
////  Created by gagandeepmishra on 22/07/19.
////  Copyright © 2019 Chandan Taneja. All rights reserved.
////
//
import Foundation
import UIKit
import SwiftyJSON

typealias MessageContent = (content:[String],messageType:MessageType,duration:String,size:String)

class ChatViewModel:NSObject {
    static let shared = ChatViewModel()
    var chatMessagelist = [MessageModel]()
    var selectedActionType : MessageAction = .none
    fileprivate var senderId:String{
        return userModel?.userId ?? ""
    }
    func removeall()
    {
        self.chatMessagelist.removeAll()
    }
    func sendMessage(msg:MessageContent,channelId:String,messageId:String = "",success:@escaping ()->Void)
    {
        if ServerManager.shared.CheckNetwork()
        {
            ServerManager.shared.showHud()
              var params:[String:Any] = [String:Any]()
            if messageId == ""
            {
                params = ["msg":msg.content,"msgType":msg.messageType.rawValue,"channelId":channelId,"duration":msg.duration,"size":msg.size]
            }
            else{
             params = ["msg":msg.content,"msgType":msg.messageType.rawValue,"channelId":channelId,"duration":msg.duration,"size":msg.size,"messageId":messageId]
            }
          
            ServerManager.shared.httpPost(request: ApiName.sendMessage.api(environment:kApiEnvironment), params: params,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    
                    self.deleteCopy()
                    
                    success()
                    break
                default:
                    alertMessage = message
                    
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
        }
    }
    func messageEvent(channelID id:String,success:@escaping OnSuccess,falier:@escaping OnFailure){
        guard ServerManager.shared.CheckNetwork() else {
            return
        }
        if selectedActionType != .none, selectedActionType != .delete {
            let copyData  = LDPasteboard.shared.copyPaste
            let iDs = LDPasteboard.shared.messageIDs
            
            let params:[String:Any]  = ["channelId":id,"msg":iDs]
            ServerManager.shared.httpPost(request: ApiName.messageEvent.api(environment:kApiEnvironment), params: params,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.deleteCopy()
                    self.selectedActionType = .none
                    self.getMessage(channelId: id, success: { (message) in
                        success(message)
                    }, falier: { (message) in
                        falier(message)
                    })
                    break
                   case 404:
                   falier(message)
                   
                default:
                    alertMessage = message
                    
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    
                }
            })
            
            
        }
    }
    func getMessage(channelId:String,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
           // ServerManager.shared.showHud()
            
            ServerManager.shared.httpGet(request: ApiName.getMessage.api(environment:kApiEnvironment) + "\(channelId)", params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(ChatResponseModel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.statusCode else{print("+++++++++++++ \(String(describing: response.statusCode))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.clearData()
                  
                    self.chatMessagelist =  response.chatModel
                    print(self.chatMessagelist)
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
    }
    func deleteMessage(success:@escaping ()->Void)
    {
        guard ServerManager.shared.CheckNetwork(), self.actionItems.contains(.delete) else{
            return
        }
        
        let ids = self.chatMessagelist.filter({$0.isRowSelcted == true}).compactMap({$0.messageId})
        if ids.count == 0 {
            return
        }
        ServerManager.shared.showHud()
        let params:[String:Any] = ["messageId":ids]
        ServerManager.shared.httpDelete(request: ApiName.deleteMessage.api(environment:kApiEnvironment), params: params,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
            ServerManager.shared.hideHud()
            guard let response = responseData.JKDecoder(JoinChannel.self) else{
                return
                
            }
            print("+++++++++++++ \(String(describing: response))")
            guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                return}
            let message = response.message ?? ""
            switch status{
            case 200:
                alertMessage = message
                success()
                break
            default:
                alertMessage = message
                
                break
            }
        }, failureHandler: { (error) in
            DispatchQueue.main.async {
                ServerManager.shared.hideHud()
                alertMessage = error?.localizedDescription
                
            }
        })
        
    }
    func getJointStatus(channelId:String,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
           // ServerManager.shared.showHud()
            
            ServerManager.shared.httpGet(request: ApiName.joinCheck.api(environment:kApiEnvironment) + "\(channelId)", params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
    }
    
}

extension ChatViewModel{
    var count:Int{
        return chatMessagelist.count
    }
    func numberOfRows(InSection section:Int) -> Int {
        return count
        
    }
    func clearData()
    {
        self.chatMessagelist.removeAll()
    }
    func cellForRowAt(at indexPath:IndexPath) -> MessageModel {
        guard self.chatMessagelist.count > indexPath.row else {
            return self.chatMessagelist[0]
        }
        return self.chatMessagelist[indexPath.row]
    }
    func deleteRow(at indexPath:IndexPath){
        
        self.chatMessagelist.remove(at: indexPath.row)
    }
    func getMessageData() -> [MessageModel]
    {
        return chatMessagelist
    }
    func append(at item:MessageModel){
        self.chatMessagelist.append(item)
    }
    func hideButtonOption(sender:UIButton)
    {
        sender.isHidden = true
    }
    var actionItems:[MessageAction]{
        let filter = self.chatMessagelist.filter({$0.isRowSelcted})
        var actions:[MessageAction] = []
        if filter.count == 1  {
        
            actions = [.copy,.reply,.forword,.delete]
            
        }else if filter.count>1{
            if filter.filter({$0.messageType == .text}).count == filter.count{
                actions = [.copy,.forword,.delete]
                
            }else {
                actions = [.forword,.delete]
                
            }
            
        }
        return actions
        
    }
    func deleteCopy(){
        LDPasteboard.shared.copyPaste = nil
        LDPasteboard.shared.messageIDs.removeAll()
    }
    func setMessage(at actionType:MessageAction){
          deleteCopy()
        self.selectedActionType = actionType
        let filters  = self.chatMessagelist.filter({$0.isRowSelcted})

        switch self.selectedActionType {
        case .copy:
            let onlytextTypes  = filters.compactMap({$0.messageType == .text})
            
            if  onlytextTypes.count == numberOfSelectedMessage {
                LDPasteboard.shared.copyPaste = filters.compactMap({$0.message})
                LDPasteboard.shared.messageIDs = filters.compactMap({$0.messageId})
            }
        case .forword:
            if  actionItems.contains(.forword)  {
                LDPasteboard.shared.messageIDs = self.chatMessagelist.filter({$0.isRowSelcted}).compactMap({$0.messageId})
            }
        case .reply:
            let list  =  actionItems.compactMap({$0 == .reply})
            if  list.count == 1  {
                LDPasteboard.shared.copyPaste = filters.compactMap({$0.message})
                if let id  = filters.compactMap({$0.messageId}).first{
                    LDPasteboard.shared.messageIDs = [id]
                }
                
            }
            
        default:
            break
        }
    }
    func deleteSelectedMessage(table:UITableView){
        var indexPaths:[IndexPath] = []
        
        self.chatMessagelist.filter({$0.isRowSelcted == true}).forEach { (obj) in
            if let index  = self.chatMessagelist.index(of: obj){
                indexPaths.append(IndexPath(row: index, section: 0))
                self.chatMessagelist.remove(at: index)
            }
            
        }
        table.reloadData()
      
    }
    func multipleSelection(at indexPath:IndexPath)
    {
        let obj =  self.chatMessagelist[indexPath.row]
        if obj.isRowSelcted{
            self.chatMessagelist[indexPath.row].isRowSelcted = false
        }else{
            self.chatMessagelist[indexPath.row].isRowSelcted = true
        }
        
    }
    func unselectAll(){
        self.chatMessagelist.filter({$0.isRowSelcted == true}).forEach { (obj) in
            if let index = self.chatMessagelist.index(of: obj){
                self.chatMessagelist[index].isRowSelcted = false
            }
        }
        
    }
    var numberOfSelectedMessage:Int{
        return self.chatMessagelist.filter({$0.isRowSelcted == true}).count
    }
    
    
    
}
