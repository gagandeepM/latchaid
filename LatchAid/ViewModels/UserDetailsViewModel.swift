//
//  UserDetailsViewModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 08/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
class UserDetailsViewModel:NSObject
{
    
    func reportUser(userId:String,reportUserId:String,channelId:String,reason:String,success:@escaping OnSuccess, falier:@escaping OnFailure)
    {
        if  ServerManager.shared.CheckNetwork(){
        ServerManager.shared.showHud()
            let params:[String:Any] = ["userId":userId,"reportUserId":reportUserId,"channelId":channelId,"reason":reason]
            ServerManager.shared.httpPost(request: ApiName.reportUser.api(environment: kApiEnvironment), params: params,headers:ServerManager.shared.apiHeaders,successHandler: {
        (responseData) in
        DispatchQueue.main.async {
        ServerManager.shared.hideHud()
        print(responseData)
        guard let response = responseData.JKDecoder(LoginReponseModel.self) else{return}
        print("THE RESPONSE IS THE-------------->",response)
        guard let status = response.code else{return}
        let message = response.message ?? ""
        switch status{
        case 200:
        
        success(message)
        break
        case 400:
        alertMessage = response.message ?? ""
        falier(message)
        break
        default:
        alertMessage = response.message ?? ""
        falier(message)
        break
        }
        }
        }, failureHandler: { (error) in
        DispatchQueue.main.async {
        ServerManager.shared.hideHud()
        alertMessage = error?.localizedDescription
            falier("Something went wrong")
        }
        })
        
        }
        
        
    }
    
}
