//
//  ChannelsList.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//


///  show channel list and other chat functionality
import Foundation
import Alamofire


class ChannelsListViewModel:NSObject
{
    
     var usersList:[UsersList] = [UsersList]()
     var channelsList:[ChannelsList] = [ChannelsList]()
   
    func removeAll()
    {
        self.channelsList.removeAll()
        self.usersList.removeAll()
    }
    
    // get channel listing
    func getChannelListing( searchString:String = "",page:Int = 0,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request: ApiName.getChannelList.api(environment:kApiEnvironment) + "\(searchString)", params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(ChannelListing.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.channelsList = response.channels
                    print(self.channelsList.count)
                    print(self.usersList.count)
                    success(message)
                    //guard response.channels.count > 0 else {return}
                    //self.channelsList = response.channels
                    break
                default:
                    self.channelsList.removeAll()
                    //alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    self.channelsList.removeAll()
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
        
    }
    // channel details
    func getChannelDetails( channelId:String = "",success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
           // ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request: ApiName.channelDetails.api(environment:kApiEnvironment) + "\(channelId)", params: nil,headers:ServerManager.shared.apiHeaders ,successHandler: { (responseData:Data) in
               // ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(ChannelDetails.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    self.usersList = response.users
                    print(self.usersList.count)
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
        
    }
    // join channel chat
    
    func joinChannel(userId:String,channelId:String,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
            //ServerManager.shared.showHud()
           
           
            let param = ["userId":userId,
                         "channelId":channelId]
            ServerManager.shared.httpPost(request: ApiName.joinChat.api(environment:kApiEnvironment), params: param,headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                //ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                let count = "\(response.count ?? 0)"
                switch status{
                case 200:
                   
                    success(count)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
        
    }
    
    // exit from channel group
    func exitChannelMember(channelId:String,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
            ServerManager.shared.showHud()
            
            
            let param = ["channelId":channelId]
            ServerManager.shared.httpPost(request: ApiName.exitChannel.api(environment:kApiEnvironment), params: param,headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
        
    }
    // Notification controlfor individual channel group
    func notificationControl(channelId:String,notification:Bool,success:@escaping OnSuccess,falier:@escaping OnFailure)
    {
        if ServerManager.shared.CheckNetwork()
        {
           // ServerManager.shared.showHud()
            
            let param:[String:Any] = ["channelId":channelId, "notification":notification]
            ServerManager.shared.httpPost(request: ApiName.notificationControl.api(environment:kApiEnvironment), params: param,headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(JoinChannel.self) else{
                    return
                    
                }
                print("+++++++++++++ \(String(describing: response))")
                guard let status = response.code else{print("+++++++++++++ \(String(describing: response.code))")
                    return}
                let message = response.message ?? ""
                switch status{
                case 200:
                    
                    success(message)
                    break
                default:
                    alertMessage = message
                    falier(message)
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    falier("Something went wrong")
                }
            })
        }
        
    }
    func numberOfRow() -> Int
    {
        print(self.channelsList.count)
        return self.channelsList.count
        
    }
    
    func cellRowAt(_ indexPath:IndexPath) -> ChannelsList
    {
        return self.channelsList[indexPath.row]
    }
    func didSelectForRowAt(at indexPath:IndexPath) -> [UsersList] {
        
        return self.channelsList[indexPath.row].users
    }
    
    func usersListData() -> [UsersList]
    {
        return self.usersList
    }
    
    
    
}
