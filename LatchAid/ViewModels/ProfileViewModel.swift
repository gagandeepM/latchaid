//
//  ProfileViewModel.swift
//  LatchAid
//
//  Created by Anuj Sharma on 12/04/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import Alamofire

class ProfileViewModel:NSObject
{
    
    //MARK:- Get User Profile
    
    func getProfileData( userId:String = "",onSuccess:@escaping()->Void,onFailure:@escaping()->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpGet(request:ApiName.getProfile.api(environment: kApiEnvironment) + "\(userId)", params:nil,headers:ServerManager.shared.apiHeaders , successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(LoginReponseModel.self) else{return}
                    guard let status = response.code else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        
                        guard var user  =  response.user else{return}
                        if let  model = userModel{
                            user.userId = model.userId
                            UserDefaults.Default(set: user, forKey: kUserDataKey)
                            alertMessage = message
                            
                        } else{
                            UserDefaults.Default(set: user, forKey: kUserDataKey)
                        }
                        onSuccess()
                        break
                        
                    default:
                        alertMessage = message
                        onFailure()
                        break
                    }
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure()
                }
            })
            
        }
    }
    // Validate Email
    
    func validateEmail( email:String = "",onSuccess:@escaping(_ message:String)->Void,onFailure:@escaping(_ message:String)->Void){
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            ServerManager.shared.httpPost(request: ApiName.validateEmail.api(environment: kApiEnvironment), params: ["email":email], successHandler: { (response:Data) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    guard let response = response.JKDecoder(LoginReponseModel.self) else{return}
                    guard let status = response.code else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        //alertMessage = message
                        onSuccess(message)
                        break
                    case 400 :
                        alertMessage = message
                        onFailure(message)
                        break;
                    default:
                        alertMessage = message
                        onFailure(message)
                        break
                    }
                }
            }) { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure("Something went wrong")
                }
            }
            
        }
    }
    
    //MARK:- Change Password
    
    func changePassword(oldPassword: String,password:String,confirmPassword:String,onSuccess : @escaping(_ message:String)->Void,onFailure:@escaping(_ message:String)->Void) {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            
            let params:[String:Any] =
                [
                    "oldPassword" : oldPassword,
                    "newPassword" : password
            ]
            ServerManager.shared.httpPut(request:ApiName.changePassword.api(environment: kApiEnvironment), params: params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                guard let response = responseData.JKDecoder(LoginReponseModel.self) else{return}
                guard let status = response.code else{return}
                switch status{
                case 200:
                    
                    onSuccess(response.message ?? "")
                    break
                case 400:
                    onFailure(response.message ?? "")
                default:
                    alertMessage = response.message ?? ""
                    onFailure(response.message ?? "")
                    break
                }
            }, failureHandler: { (error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    onFailure("Something went wrong")
                }
            })
        }
        
    }
    
    //MARK: Edit myinfo
    
    func
        editMyInfo(location:String,timeForBreastfeeding:String,interestedForBreastfeeding:String,selectChildAge:String,selectAge:String,interestedNumber:Int,deliveryDate:String,success:@escaping OnSuccess,failure:@escaping OnFailure)
    {
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let params = ["location":location,"timeForBreastfeeding":timeForBreastfeeding,"interestedForBreastfeeding":interestedForBreastfeeding,"selectChildAge":selectChildAge,"selectAge":selectAge,"interestedNumber":interestedNumber,"deliveryDate":deliveryDate] as [String : Any]
            
            ServerManager.shared.httpPut(request: ApiName.updateMyInfo.api(environment: kApiEnvironment), params:params, headers:ServerManager.shared.apiHeaders,successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    print(responseData)
                    guard let response = responseData.JKDecoder(LoginReponseModel.self) else{
                        return
                        
                    }
                    print("THE RESPONSE IS THE-------------->",response)
                    guard let status = response.code else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        
                        userModel = user
                        
                        success(message)
                        break
                    default:
                        alertMessage = response.message ?? ""
                        failure(response.message ?? "")
                        break
                    }
                }
            }, failureHandler: {(error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
        }
        
    }
    
    
    fileprivate func set(multipart image:UIImage)->[MultipartData]{
        return [MultipartData(medaiObject: image, mediaKey: "image",quality:.medium)]
    }
    
    //MARK: Image Upload
    func imageUpload(image:UIImage,fullName:String = "",success:@escaping OnSuccess,failure:@escaping OnFailure){
        
        
        if ServerManager.shared.CheckNetwork(){
            ServerManager.shared.showHud()
            let array = set(multipart: image)
            print(array)
            let params = ["fullName":fullName]
            ServerManager.shared.httpUpload(request: ApiName.updateProfile.api(environment: kApiEnvironment), params:params, headers:ServerManager.shared.apiHeaders,multipartObject: array, successHandler: { (responseData:Data) in
                ServerManager.shared.hideHud()
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    print(responseData)
                    guard let response = responseData.JKDecoder(LoginReponseModel.self) else{
                        return
                        
                    }
                    print("THE RESPONSE IS THE-------------->",response)
                    guard let status = response.code else{return}
                    let message = response.message ?? ""
                    switch status{
                    case 200:
                        guard let user = response.user else{return}
                        userModel = user
                        
                        success(message)
                        break
                    default:
                        alertMessage = response.message ?? ""
                        failure(response.message ?? "")
                        break
                    }
                }
            }, failureHandler: {(error) in
                DispatchQueue.main.async {
                    ServerManager.shared.hideHud()
                    alertMessage = error?.localizedDescription
                    failure("Something went wrong")
                }
            })
        }
        
    }
    
}
