//
//  ShraedContentTableViewCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 16/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
class SharedContentTableViewCell:UITableViewCell,AVAudioRecorderDelegate, AVAudioPlayerDelegate
{
    @IBOutlet weak var audioFileNameLbl: UILabel!
    @IBOutlet weak var audioFileInfoLbl: UILabel!
    @IBOutlet weak var audioPlayBtn: UIButton!
    
   var player:AVAudioPlayer!
    var urlString = ""
    var obj:AudioFiles?
    {
        didSet{
            self.urlString = obj?.audioFile ?? ""
            self.audioFileNameLbl.text = obj?.duration ?? ""
            self.audioFileInfoLbl.text = "\(obj?.size ?? "") \( obj?.createdAt ?? "")"
            
        }
    }
    @IBAction func audioPlayBtn(_ sender: UIButton) {
        if audioPlayBtn.currentImage == UIImage(named: "play-1")
        {
            
            guard let  url = URL(string: urlString) else{
                return
            }
            self.downloadSound(url: url)
          
            
        }
        else{
            player.stop()
            DispatchQueue.main.async {
                 self.audioPlayBtn.setBackgroundImage(UIImage(named: "play-1"), for: .normal)
            }
           
        }
    }
    func downloadSound(url:URL){
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch _ {
        }
        let docUrl:URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let desURL = docUrl.appendingPathComponent(url.lastPathComponent)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URLData, response, error) -> Void in
            
            do{
                let isFileFound:Bool? = FileManager.default.fileExists(atPath: desURL.path)
                if isFileFound == true{
                    print(desURL) //delete tmpsong.m4a & copy
                } else {
                    try FileManager.default.copyItem(at: URLData!, to: desURL)
                }
                let sPlayer = try AVAudioPlayer(contentsOf: desURL)
                
                self?.player = sPlayer
                self?.player.delegate = self
                self?.player.prepareToPlay()
                self?.player.play()
                
                DispatchQueue.main.async {
                   self?.audioPlayBtn.setBackgroundImage(UIImage(named: "ic_pause"), for: .normal)
                }
                
                
            }catch let err {
                print(err.localizedDescription)
            }
            
        })
        downloadTask.resume()
    }
    //MARK:Audio player delegate
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
         self.audioPlayBtn.setBackgroundImage(UIImage(named: "play"), for: .normal)
    }
    
    private func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    private func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder!, error: NSError!) {
        print("Audio Record Encode Error")
    }
   

    
}
class SharedContentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var sharedImage: UIImageView!
    @IBOutlet weak var activityIndigator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityIndigator.color = UIColor(ColorRGB: 2.0/255.0, Grean: 210.0/255.0, Blue: 172.0/255.0, alpha: 1.0)
        self.activityIndigator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        self.activityIndigator.hidesWhenStopped = true
    }
    
    var obj:MediaFiles?
    {
        didSet{
            self.activityIndigator.startAnimating()
            self.sharedImage.kf.setImage(with: URL(string: self.obj?.mediaFile ?? ""), placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                self.activityIndigator.stopAnimating()
            }
           
        }
    }
}
