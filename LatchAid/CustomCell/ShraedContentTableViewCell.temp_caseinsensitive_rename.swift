//
//  ShraedContentTableViewCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 16/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class SharedContentTableViewCell:UITableViewCell,AVAudioRecorderDelegate
{
    @IBOutlet weak var audioFileNameLbl: UILabel!
    @IBOutlet weak var audioFileInfoLbl: UILabel!
    @IBOutlet weak var audioPlayBtn: UIButton!
    var urlString = ""
    var isPlayingAlready = false
    var index:IndexPath?
    var tempIndex = 0
    var obj:AudioFiles?
    {
        didSet{
            self.urlString = obj?.audioFile ?? ""
            self.audioFileNameLbl.text = obj?.name ?? ""
            self.audioFileInfoLbl.text = "\(obj?.size ?? "") \( obj?.createdAt ?? "")"
           
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUI()
        
    }
    func setUI()
    {
       
        self.audioPlayBtn.setImage(UIImage(named: "play-1"), for: .normal)
        
    }
    @IBAction func audioPlayBtn(_ sender: UIButton) {
        
        
       Audio.shareInst.urlAudioPlay(urlStr: self.urlString, cell: self)

    }

}
class SharedContentCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var sharedImage: UIImageView!
    @IBOutlet weak var activityIndigator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.activityIndigator.color = UIColor(ColorRGB: 2.0/255.0, Grean: 210.0/255.0, Blue: 172.0/255.0, alpha: 1.0)
        self.activityIndigator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        self.activityIndigator.hidesWhenStopped = true
    }
    
    var obj:MediaFiles?
    {
        didSet{
            self.activityIndigator.startAnimating()
            self.sharedImage.kf.setImage(with: URL(string: self.obj?.mediaFile ?? ""), placeholder: nil, options: nil, progressBlock: nil) { (image, error, cache, url) in
                self.activityIndigator.stopAnimating()
            }
           
        }
    }
}
