//
//  NotificationCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 29/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userMessageLbl: UILabel!
    var obj:Notification?
    {
        didSet{
           
            if obj?.count != 0
            {
                if obj?.channelNotification?.imageUrl == ""
                {
                    self.userImage.image = UIImage(named: "user_group")
                }
                else{ self.userImage.kf.setImage(with: URL(string: obj?.channelNotification?.imageUrl ?? ""))}
                self.userName.text  = obj?.channelNotification?.channelName ?? ""
                self.userMessageLbl.text = "You have \(obj?.count ?? 0) new messages"
            }else{
                if obj?.channelNotification?.imageUrl == ""
                {
                    self.userImage.image = UIImage(named: "user_group")
                }
                else{ self.userImage.kf.setImage(with: URL(string: obj?.channelNotification?.imageUrl ?? ""))}
                self.userName.text  = obj?.channelNotification?.channelName ?? ""
                self.userMessageLbl.text = "You have \(obj?.count ?? 0) new messages"
            }
            
       
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
