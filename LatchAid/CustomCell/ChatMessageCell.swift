//
//  ChatCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 16/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit
import AVFoundation
import SwiftySound
class SenderChatMessageCell: UITableViewCell {
    
    @IBOutlet weak var messageDeliveredImage: UIImageView!
    @IBOutlet weak var messageTime_lbl: UILabel!
    @IBOutlet weak var senderMessage_lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
class ReceiverChatMessageCell: UITableViewCell {
   
    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var messageTime_lbl: UILabel!
    @IBOutlet weak var receiverMessage_lbl: UILabel!
    @IBOutlet weak var receiverName_lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.receiverImage.makeRounded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
protocol SenderChatImageCellDelegate {
    func isBurronPress(buttonPress:Bool,message:String)
}
class SenderChatImageCell:UITableViewCell
{
    @IBOutlet weak var senderImage: UIImageView!
    @IBOutlet weak var imageEnlargeBtn: UIButton!
    @IBOutlet weak var senderImageLoader: UIActivityIndicatorView!
    @IBOutlet weak var messageTime_lbl: UILabel!
    @IBOutlet weak var messageDeleiveredImage: UIImageView!
     var delegate:SenderChatImageCellDelegate?
      var obj:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.senderImageLoader.color = UIColor(ColorRGB: 2.0/255.0, Grean: 210.0/255.0, Blue: 172.0/255.0, alpha: 1.0)
        self.senderImageLoader.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        self.senderImageLoader.hidesWhenStopped = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func imageEnlargeBtn(_ sender: UIButton) {
        self.delegate?.isBurronPress(buttonPress: true, message: obj ?? "")
       
    }
    
    
    
}
protocol ReceiverChatImageCellDelegate {
    func isBurronPressReceiver(buttonPress:Bool,message:String)
}
class ReceiverChatImageCell:UITableViewCell
{
    @IBOutlet weak var receiverImageLoader: UIActivityIndicatorView!
    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var receiverName_lbl: UILabel!
    @IBOutlet weak var messageTime_lbl: UILabel!
    @IBOutlet weak var receiverImageMessage: UIImageView!
    var obj:String?
    var delegate:ReceiverChatImageCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        self.receiverImage.makeRounded()
        self.receiverImageLoader.color = UIColor(ColorRGB: 2.0/255.0, Grean: 210.0/255.0, Blue: 172.0/255.0, alpha: 1.0)
        self.receiverImageLoader.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        self.receiverImageLoader.hidesWhenStopped = true
    }
    @IBAction func imageEnlargeBtn(_ sender: UIButton) {
        self.delegate?.isBurronPressReceiver(buttonPress: true, message: obj ?? "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
class SenderChatAudioCell:UITableViewCell,AVAudioRecorderDelegate, AVAudioPlayerDelegate
{
   
    
    
    var player = AVAudioPlayer()
    @IBOutlet weak var messageDeliveredImage: UIImageView!
    @IBOutlet weak var indigator: UIActivityIndicatorView!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var audioPlayBtn: UIButton!
    @IBOutlet weak var senderAudioSlider: UISlider!
    @IBOutlet weak var timeDurationLbl: UILabel!
    var isBtnPressed = true
    var urlString = ""
    var obj:MessageModel?
    {
        didSet{
            urlString = obj?.message ?? ""
            self.messageTime.text = obj?.messageTime ?? ""
            self.timeDurationLbl.text = obj?.duration ?? ""
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
       self.indigator.isHidden = true
        setUI()
        
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUI()
    {
        
        self.audioPlayBtn.setImage(UIImage(named: "ic_play"), for: .normal)
        
        
    }

    
    @IBAction func audioPlayBtn(_ sender: Any) {
        Audio.shareInst.urlAudioPlay(urlStr: urlString, cell: self)
        
    }
    //MARK:Audio player delegate
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.audioPlayBtn.setImage(UIImage(named: "ic_play"), for: .normal)
    }
    
    private func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    private func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder!, error: NSError!) {
        print("Audio Record Encode Error")
    }
    @objc func updateTime(_ timer: Timer) {
        senderAudioSlider.value = Float(player.currentTime)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        if sender.value == 0.0 {
            self.audioPlayBtn.setImage(UIImage(named: "ic_play"), for: .normal)
        }

    }
    

    
}


class ReceiverChatAudioCell:UITableViewCell,AVAudioRecorderDelegate,AVAudioPlayerDelegate
{
    var completioncall:((_ issuccess:Bool,_ url:String)->Void)?
    
    @IBOutlet weak var refview: UIView!
    var player:AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    @IBOutlet weak var receiverImage: UIImageView!
    @IBOutlet weak var timeDurationLbl: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    @IBOutlet weak var reciverName_lbl: UILabel!
    @IBOutlet weak var receiverAudioBtn: UIButton!
    @IBOutlet weak var receiverAudioSlider: UISlider!
    @IBOutlet weak var indigator: UIActivityIndicatorView!
    var isBtnPressed = true
    var urlString = ""
    var obj:MessageModel?
    {
        didSet{
            urlString = obj?.message ?? ""
            self.timeDurationLbl.text = obj?.duration ?? ""
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.indigator.isHidden = true
        setUI()
        self.receiverImage.makeRounded()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func setUI()
    {
        self.receiverAudioBtn.setImage(UIImage(named: "ic_play"), for: .normal)
    }
    @IBAction func audioPlayBtn(_ sender: Any) {
   
        Audio.shareInst.urlAudioPlay(urlStr: urlString, cell: self)
    }
   
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        if sender.value == 0.0 {
            self.receiverAudioBtn.setImage(UIImage(named: "ic_play"), for: .normal)
        }
    }
    @objc func updateTime(_ timer: Timer) {
        receiverAudioSlider.value = Float(player.currentTime)
    }
    //MARK:Audio player delegate
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
       self.receiverAudioBtn.setImage(UIImage(named: "ic_play"), for: .normal)
    }
    
    private func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    private func audioRecorderEncodeErrorDidOccur(recorder: AVAudioRecorder!, error: NSError!) {
        print("Audio Record Encode Error")
    }
}
class SenderReplyCell:UITableViewCell
{
    
    @IBOutlet weak var replierNameLbl: UILabel!
    @IBOutlet weak var replierMessage: UILabel!
    @IBOutlet weak var replierImageMessage: UIImageView!
    @IBOutlet weak var senderMessageLbl: UILabel!
    @IBOutlet weak var deliveredImage: UIImageView!
    @IBOutlet weak var messageTimeLbl: UILabel!
    
    
    
    
}
class ReceiverReplyCell:UITableViewCell
{
    @IBOutlet weak var receiverUserImage: UIImageView!
    @IBOutlet weak var receiverNameLbl: UILabel!
    @IBOutlet weak var receiverMessageTimeLbl: UILabel!
    @IBOutlet weak var replierNameLbl: UILabel!
    @IBOutlet weak var replierMessage: UILabel!
    @IBOutlet weak var replierImageMessage: UIImageView!
    @IBOutlet weak var senderMessageLbl: UILabel!
    
    @IBOutlet weak var sizeText: UILabel!
    
    
    
}
class testCell:UITableViewCell
{
    @IBOutlet weak var lblTest: UILabel!
}



//MARK:AUDIO PLAY FUNCTION
class Audio{
    var table = UITableView()
    var playerAudio = AVPlayer()
    static var shareInst = Audio()
    var currentCell = UITableViewCell()
    
    
    
    func urlAudioPlay(urlStr:String,cell:UITableViewCell)
    {
        self.table.isUserInteractionEnabled = false
        self.table.isScrollEnabled = false
        
        if self.isPlayingPrev()
        {
            
            if currentCell == cell
            {
                self.table.isUserInteractionEnabled = true
                self.table.isScrollEnabled = true
                self.currentCell = UITableViewCell()
                return
            }
            
            
        }
        
        
        self.currentCell = cell
        if let reciveCell = self.currentCell as? ReceiverChatAudioCell
        {
            reciveCell.indigator.isHidden = false
            reciveCell.indigator.startAnimating()
            reciveCell.receiverAudioBtn.setImage(UIImage(named: "ic_pause"), for: .normal)
            
        }else if let senderCell = self.currentCell as? SenderChatAudioCell{
            senderCell.indigator.isHidden = false
            senderCell.indigator.startAnimating()
            senderCell.audioPlayBtn.setImage(UIImage(named: "ic_pause"), for: .normal)
        }else if let senderCell = self.currentCell as? SharedContentTableViewCell{
            senderCell.audioPlayBtn.setImage(UIImage(named: "stop_green"), for: .normal)
        }
        
        let url = urlStr
        let playerItem = AVPlayerItem( url:NSURL( string:url )! as URL )
        self.playerAudio = AVPlayer(playerItem:playerItem)
        self.playerAudio.rate = 1.0;
        self.playerAudio.volume = 0.5
        self.playerAudio.play()
        //SET AUDIO SOUND OUTPUT
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        
        self.playerAudio.actionAtItemEnd =  .pause
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(self.updateTime1(_:)), userInfo: nil, repeats: true)
        
        
        
        
    }
    func isPlayingPrev() -> Bool
    {
        if self.playerAudio.timeControlStatus == .playing  ||  self.playerAudio.timeControlStatus == .waitingToPlayAtSpecifiedRate
        {
            self.playerAudio.cancelPendingPrerolls()
            self.playerAudio.pause()
            
            if ((currentCell as? ReceiverChatAudioCell) != nil)
            {
                let cell = currentCell as! ReceiverChatAudioCell
                cell.indigator.isHidden = true
                cell.receiverAudioBtn.setImage(UIImage(named: "ic_play"), for: .normal)
                cell.receiverAudioSlider.value = 0.0
                
            }else if ((currentCell as? SenderChatAudioCell) != nil){
                let cell = currentCell as! SenderChatAudioCell
                cell.indigator.isHidden = true
                cell.audioPlayBtn.setImage(UIImage(named: "ic_play"), for: .normal)
                cell.senderAudioSlider.value = 0.0
            }else if ((currentCell as? SharedContentTableViewCell) != nil){
            
                let cell = currentCell as! SharedContentTableViewCell
                cell.audioPlayBtn.setImage(UIImage(named: "play-1"), for: .normal)
                
            }
            
            return true
        }else{
            return false
        }
        
    }
    @objc func updateTime1(_ timer: Timer) {
        
        guard let currentPlay = playerAudio.currentItem?.asset.duration else {
            timer.invalidate()
            return}
        
        let time : Float64 = CMTimeGetSeconds(self.playerAudio.currentTime());
        let seconds : Float64 = CMTimeGetSeconds(currentPlay)
        
        if let reciveCell = currentCell as? ReceiverChatAudioCell
        {
            
            reciveCell.indigator.isHidden = true
            reciveCell.receiverAudioSlider.value = Float ( time )
            reciveCell.receiverAudioSlider.maximumValue = Float(seconds)
            if playerAudio.timeControlStatus == .paused
            {
                reciveCell.receiverAudioBtn.setImage(UIImage(named: "ic_play"), for: .normal)
                reciveCell.receiverAudioSlider.value = 0.0
                reciveCell.receiverAudioSlider.maximumValue = 0.0
            }
            
        }else if let senderCell = currentCell as? SenderChatAudioCell{
            senderCell.indigator.isHidden = true
            
            
            senderCell.senderAudioSlider.value = Float ( time )
            senderCell.senderAudioSlider.maximumValue = Float(seconds)
            if playerAudio.timeControlStatus == .paused
            {
                senderCell.audioPlayBtn.setImage(UIImage(named: "ic_play"), for: .normal)
                senderCell.senderAudioSlider.value = 0.0
                senderCell.senderAudioSlider.maximumValue = 0.0
                
            }
            
            
        }else if let SharedContentCell = currentCell as? SharedContentTableViewCell{
            
           
            if playerAudio.timeControlStatus == .paused
            {
                SharedContentCell.audioPlayBtn.setImage(UIImage(named: "play-1"), for: .normal)
                
                
            }
        }
        
        if playerAudio.timeControlStatus == .paused
        {
            timer.invalidate()
            self.table.isScrollEnabled = true
            
        }else if playerAudio.timeControlStatus == .playing
        {
            self.table.isUserInteractionEnabled = true
        }
        
    }
}

