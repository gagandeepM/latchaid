//
//  ChannelListCell.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import UIKit

class ChannelListCell: UITableViewCell {
    
    @IBOutlet weak var channelImage: UIImageView!
    @IBOutlet weak var channelName_lbl: UILabel!
    @IBOutlet weak var channelType_lbl: UILabel!
    @IBOutlet weak var channelTypeSymbol: UIImageView!
    var obj:ChannelsList?{
        didSet{
            self.channelName_lbl.text = obj?.channelName ?? ""
            self.channelType_lbl.text = obj?.typeOfChannel ?? ""
            if obj?.typeOfChannel == "Public channel"
            {
                self.channelTypeSymbol.image = UIImage(named: "public")
            }else if obj?.typeOfChannel == "expert"
            {
                self.channelTypeSymbol.image = UIImage(named: "expert")
            }
            else
            {
                self.channelTypeSymbol.image = UIImage(named: "admin")
            }
            if obj?.channelImage == ""
            {
                 self.channelImage.image = UIImage(named: "user_group")
            }
            else{
                self.channelImage.kf.setImage(with: URL(string: obj?.channelImage ?? ""))
             self.channelImage.makeRounded()
        
        }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class UserDetailCell:UITableViewCell
{
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName_lbl: UILabel!
    @IBOutlet weak var userType_lbl: UILabel!
    @IBOutlet weak var userTypeSymbol: UIImageView!
      @IBOutlet weak var userTypeName: UILabel!
    @IBOutlet weak var onlineStatusView: UIImageView!
    var obj:UsersList?{
        didSet{
            userTypeName.text =  ""
            self.userName_lbl.text = obj?.fullName ?? ""
            self.onlineStatusView.makeCircular()
            if obj?.onlineStatus ??  false
            {
                self.onlineStatusView.image = UIImage(named: "IndicatorOnline")
                self.userType_lbl.text = "Online"
            }
            else
            {
               self.onlineStatusView.image = UIImage(named: "Indicator")
                self.userType_lbl.text = obj?.lastSeen ?? ""
            }
            
//            if obj?.userType == "public"
//            {
//                self.userTypeSymbol.image = UIImage(named: "public")
//            }else if obj?.userType == "expert"
//            {
//                self.userTypeSymbol.image = UIImage(named: "expert")
//            }
//            else if obj?.userType == "admin"
//            {
//                self.userTypeSymbol.image = UIImage(named: "Vector")
//                //userTypeName.text = obj?.userType ?? ""
//                userTypeName.text = "admin"
//            }else{
//                // self.userTypeSymbol.isHidden = true
//                //self.userTypeName.isHidden = true
//            }
//            print("********",userTypeName.text)
           
            
          
            
            if obj?.imageUrl == ""
            {
                self.userImage.image = UIImage(named: "user")
            }
            else{
                self.userImage.kf.setImage(with: URL(string: obj?.imageUrl ?? ""))
            }
            
            
            self.userImage.makeRounded()
            
            self.userTypeSymbol.image = nil
            userTypeName.text =  ""
            guard obj?.userType == "admin" else {
                return
            }
            
            self.userTypeSymbol.image = UIImage(named: "Vector")
            userTypeName.text = obj?.userType ?? ""
            
        }
    }
}
extension UIView
{
    func makeCircular() {
        self.layer.cornerRadius = min(self.frame.size.height, self.frame.size.width) / 2.0
        self.clipsToBounds = true
    }
}
