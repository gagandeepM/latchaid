//
//  SharedContentModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 16/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
struct SharedContent:Mappable
{
    let statusCode:Int?
    let message:String?
    var mediaFiles:[MediaFiles] = [MediaFiles]()
    var audioFiles:[AudioFiles] = [AudioFiles]()
    enum CodingKeys:String,CodingKey {
        case statusCode = "code"
        case message = "message"
        case mediaFiles = "media"
        case audioFiles = "audio"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        guard let mediaData = try values.decodeIfPresent([MediaFiles].self, forKey: .mediaFiles) else{return}
        for mediaFile in mediaData
        {
            self.mediaFiles.append(mediaFile)
        }
        guard let audioData = try values.decodeIfPresent([AudioFiles].self, forKey: .audioFiles) else{return}
        for audioFile in audioData
        {
            self.audioFiles.append(audioFile)
        }
    }
}
struct MediaFiles:Mappable {
    
    let mediaId:String?
    let mediaFile:String?
    let createdAt:String?
    enum CodingKeys:String,CodingKey {
        case mediaId = "_id"
        case mediaFile = "msg"
        case createdAt = "createdAt"
    }
    
    init (from decorder:Decoder) throws
    {
        let values = try decorder.container(keyedBy: CodingKeys.self)
        mediaId = try values.decodeIfPresent(String.self, forKey: .mediaId)
        mediaFile = try values.decodeIfPresent(String.self, forKey: .mediaFile)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
       
    }
}
struct AudioFiles:Mappable{
    
    let audioId:String?
    let audioFile:String?
    let createdAt:String?
    let duration:String?
    let size:String?
    let name:String?
    enum CodingKeys:String,CodingKey {
        case audioId = "_id"
        case audioFile = "msg"
        case createdAt = "createdAt"
        case duration = "duration"
        case size = "size"
        case name = "audioName"
    }
    
    init (from decorder:Decoder) throws
    {
        let values = try decorder.container(keyedBy: CodingKeys.self)
        audioId = try values.decodeIfPresent(String.self, forKey: .audioId)
        audioFile = try values.decodeIfPresent(String.self, forKey: .audioFile)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        size = try values.decodeIfPresent(String.self, forKey: .size)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
    }
}
