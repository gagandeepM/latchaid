//
//  ChannelListingModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 15/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation

struct ChannelListing:Mappable {
    
    let code : Int?
    let message : String?
    var channels:[ChannelsList] = [ChannelsList]()
    enum CodingKeys: String, CodingKey {
        case code  = "code"
        case message = "message"
        case channels    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        guard let channelsData = try values.decodeIfPresent([ChannelsList].self, forKey: .channels) else{return}
        for channel in channelsData
        {
            self.channels.append(channel)
        }
        
        
    }
}
struct ChannelDetails:Mappable {
    
    let code : Int?
    let message : String?
    var users:[UsersList] = [UsersList]()
    enum CodingKeys: String, CodingKey {
        case code  = "code"
        case message = "message"
        case users    = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        guard let usersData = try values.decodeIfPresent([UsersList].self, forKey: .users) else{return}
        for user in usersData
        {
            self.users.append(user)
        }
        
        
    }
}
struct JoinChannel:Mappable
{
    let code : Int?
    let message : String?
    let count :Int?
    
    enum CodingKeys: String, CodingKey {
        case code  = "code"
        case message = "message"
        case count = "count"
     
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(message, forKey: .message)
        try container.encode(count, forKey: .count)
       
    }
}
struct ChannelsList: Mappable {
    var users : [UsersList] = [UsersList]()
    let typeOfChannel:String?
    // let admin: String?
    let channelId: String?
    
    
    let channelName:String?
    let location:String?
    let channelImage:String?
    enum CodingKeys: String,CodingKey {
        case users = "users"
        case typeOfChannel = "typeOfChannel"
        //case admin = "admin"
        case channelId = "_id"
        case channelName = "channelName"
        case location = "location"
        case channelImage = "imageUrl"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        typeOfChannel = try values.decodeIfPresent(String.self, forKey: .typeOfChannel)
        //admin = try values.decodeIfPresent(String.self, forKey: .admin)
        channelId = try values.decodeIfPresent(String.self, forKey: .channelId)
        channelName = try values.decodeIfPresent(String.self, forKey: .channelName)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        channelImage = try values.decode(String.self, forKey: .channelImage)
        guard let usersList = try values.decodeIfPresent([UsersList].self, forKey: .users) else{return}
        for userList in usersList
        {
            self.users.append(userList)
        }
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(users, forKey: .users)
        try container.encode(typeOfChannel, forKey: .typeOfChannel)
        // try container.encode(admin, forKey: .admin)
        try container.encode(channelId, forKey: .channelId)
        try container.encode(channelName, forKey: .channelName)
        try container.encode(location, forKey: .location)
    }
    init(channelId:String) {
        self.channelId = channelId
        typeOfChannel =  ""
        self.channelName = ""
        location = ""
        channelImage = ""
       
    }
}

struct UsersList:Mappable {
    let fullName:String?
    let imageUrl:String?
    let userId:String?
    let userType:String?
    let notification:Bool?
    let lastSeen:String?
    let onlineStatus:Bool?
    let joinedChannelOn:String?
    enum CodingKeys:String,CodingKey {
        case fullName = "fullName"
        case imageUrl = "imageUrl"
        case userId = "_id"
        case userType = "userType"
        case notification = "notification"
        case lastSeen = "lastSeen"
        case onlineStatus = "onlineStatus"
        case joinedChannelOn = "joinedChannelOn"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        userType = try values.decodeIfPresent(String.self, forKey: .userType)
        notification  = try values.decodeIfPresent(Bool.self, forKey: .notification)
        lastSeen = try values.decodeIfPresent(String.self, forKey: .lastSeen)
        onlineStatus = try values.decodeIfPresent(Bool.self, forKey: .onlineStatus)
        joinedChannelOn = try values.decodeIfPresent(String.self, forKey: .joinedChannelOn)
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(fullName, forKey: .fullName)
        try container.encode(imageUrl, forKey: .imageUrl)
        try container.encode(userId, forKey: .userId)
        try container.encode(userType, forKey: .userType)
        try container.encode(notification, forKey: .notification)
        try container.encode(lastSeen, forKey: .lastSeen)
        try container.encode(onlineStatus, forKey: .onlineStatus)
        try container.encode(joinedChannelOn, forKey: .joinedChannelOn)
    }
}



