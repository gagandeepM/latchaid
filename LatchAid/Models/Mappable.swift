//
//  Mappable.swift
//  Dumpya
//
//  Created by Chander on 21/08/18.
//  Copyright © 2018 Chander. All rights reserved.
//

import Foundation
extension Encodable {
    
    func JKEncoder() -> Data? {
        return JSN.JSNEncoder(self)
    }
    var jsonObject:Any?{
        return self.JKEncoder()?.jsonObject
    }
    var jsonString:String?{
        return self.JKEncoder()?.jsonString
    }
}

extension Data{
    
    func JKDecoder<T>(_ type:T.Type) ->T? where T:Decodable{
        return JSN.JSNDecoder(T.self, from: self)
    }
    var jsonString:String?{
        return String(bytes: self, encoding: .utf8)
    }
    var jsonObject:Any?{
        do{
            return try JSONSerialization.jsonObject(with: self, options: .allowFragments)
        }catch let error{
            print("json object conversion error%@",error.localizedDescription)
            return nil
        }
        
    }
    
}
struct JSN{
    static func JSNDecoder<T>(_ type:T.Type,from data:Data)  ->T? where T:Decodable{
        let obj  = try? JSONDecoder().decode(T.self, from: data)
        return obj
    }
    static func JSNEncoder<T>(_ value: T)  -> Data? where T : Encodable{
        let encodeData  = try? JSONEncoder().encode(value)
        return encodeData
    }
}


typealias Mappable = Codable

