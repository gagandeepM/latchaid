//
//  ChatModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 22/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

//"__v" = 0;
//"_id" = 5d386dc5a132211e22a6be36;
//channelId = 5d2e35cd49f42652ac8638cd;
//createdAt = "2019-07-24T14:40:05.733Z";
//msg = " Hello";
//msgType = text;
//updatedAt = "2019-07-24T14:40:05.733Z";
//userId = 5d2e36ce49f42652ac8638d0;

import Foundation
import SwiftyJSON
struct ChatModel:Mappable {
    let conversationId:String?
    
    enum CodingKeys:String,CodingKey {
        case conversationId = "_id"
        
        
        
    }
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        conversationId = (values.contains(.conversationId) == true) ? try values.decodeIfPresent(String.self, forKey: .conversationId) : ""
    }
}



struct ChatResponseModel:Mappable {
    let statusCode:Int?
    let message:String?
    var chatModel:[MessageModel] = [MessageModel]()
    
    enum CodingKeys:String,CodingKey {
        case statusCode = "code"
        case message = "message"
        case chatModel = "data"
    }
    
    init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy:CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        
        guard let chatMessages = try values.decodeIfPresent([MessageModel].self, forKey: .chatModel) else{return}
        
        for message in chatMessages
        {
            self.chatModel.append(message)
        }
    }
}




struct MessageModel:Mappable{
    var isRowSelcted: Bool = false
    var messageId:String?
    var message:String?
    var type:String?
    var senderId:UserInfo?
    var receiverId:String?
    var createdTime:String?
    var messageTime:String?
    var messageType:MessageType = .text
    var size:String?
    var duration:String?
    var replyDetails:ReplyDetails?

    
    
    
    enum CodingKeys:String,CodingKey {
        case messageId      = "_id"
        case message        = "msg"
        case type           = "msgType"
        case senderId       = "userId"
        case receiverId     = "channelId"
        case createdTime    = "createdAt"
        case messageTime    =  "messageTime"
        case size = "size"
        case duration = "duration"
        case replyDetails = "messageId"
        
    }
    
    init(from decoder:Decoder) throws {
        
        let values = try decoder.container(keyedBy:CodingKeys.self)
        messageId = try values.decodeIfPresent(String.self, forKey: .messageId)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        senderId = try values.decodeIfPresent(UserInfo.self, forKey: .senderId)
        receiverId = try values.decodeIfPresent(String.self, forKey: .receiverId)
        createdTime = try values.decodeIfPresent(String.self, forKey: .createdTime)
        messageTime = try values.decodeIfPresent(String.self,forKey:.messageTime)
        messageType = MessageType(rawValue: type ?? "") ?? .text
      
        size = try values.decodeIfPresent(String.self, forKey: .size)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        replyDetails = try values.decodeIfPresent(ReplyDetails.self, forKey: .replyDetails)
    }
    var isIncomming:Bool{
        if let ownerId = userModel?.userId, !ownerId.isEmpty, let senderId = self.senderId?.userId, !senderId.isEmpty,ownerId != senderId {
            return true
        }else{
            return false
        }
    }
  
}
extension MessageModel:Equatable{
    static func == (lhs:MessageModel,rhs:MessageModel)->Bool{
        return lhs.messageId == rhs.messageId && lhs.message == rhs.message && lhs.type == rhs.type && lhs.senderId == rhs.senderId && lhs.receiverId == rhs.receiverId && lhs.createdTime == rhs.createdTime && lhs.messageTime == rhs.messageTime && lhs.messageType == rhs.messageType && lhs.size == rhs.size && lhs.duration == rhs.duration && lhs.isRowSelcted == rhs.isRowSelcted
    }
}
struct ReplyDetails:Mappable
{
    let replyMessageId:String?
    let  message:String?
    let userId:String?
    let replierName:String?
    let replyMessageType:String?
    let size:String?
    let duration:String?
    enum CodingKeys:String,CodingKey {
        case replyMessageId = "_id"
        case message = "msg"
        case userId = "userId"
        case replierName = "fullName"
        case replyMessageType = "msgType"
        case size = "size"
        case duration = "duration"
    }
    init(from decoder:Decoder) throws {
         let values = try decoder.container(keyedBy:CodingKeys.self)
        replyMessageId = try values.decodeIfPresent(String.self, forKey: .replyMessageId)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userId = try values.decodeIfPresent(String.self, forKey: .userId)
        replierName = try values.decodeIfPresent(String.self, forKey: .replierName)
        replyMessageType = try values.decodeIfPresent(String.self, forKey: .replyMessageType)
        size = try values.decodeIfPresent(String.self, forKey: .size)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
    }
}
struct UserInfo:Mappable
{
    let userId:String?
    var userImage:String?
    var userName:String?
    
    enum CodingKeys:String,CodingKey {
        case userId = "_id"
        case userImage = "imageUrl"
        case userName = "fullName"
    }
    init(from decoder:Decoder) throws {
        
        let values = try decoder.container(keyedBy:CodingKeys.self)
        userId = try values.decodeIfPresent(String.self,forKey:.userId)
        userImage = try values.decodeIfPresent(String.self,forKey:.userImage)
        userName = try values.decodeIfPresent(String.self,forKey:.userName)
    }
}
extension UserInfo:Equatable{
    static func == (lhs:UserInfo,rhs:UserInfo)->Bool{
        return lhs.userId == rhs.userId && lhs.userImage == rhs.userImage && lhs.userName == rhs.userName
    }
}
