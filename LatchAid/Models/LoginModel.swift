//
//  LoginModel.swift

import Foundation

struct LoginReponseModel:Mappable {
    let code : Int?
    let message : String?
    let user:UserModel?
    enum CodingKeys: String, CodingKey {
        case code  = "code"
        case message = "message"
        case user    = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message =   try values.decodeIfPresent(String.self, forKey: .message)
        user =  values.contains(.user) == true ? try values.decodeIfPresent(UserModel.self, forKey: .user) : nil
        
        
    }
}

struct UserModel:Mappable{
    var userId : String?
    var fullName:String?
    var email : String?
    var accessToken : String?
    var firstName :String?
    var lastName :String?
    var existingUser:Bool?
    var isProfileUpdate:Bool?
    var socialType:String?
    var selectAge:String?
    var selectChildAge:String?
    var location:String?
    var lat:Double?
    var long:Double?
    var timeForBreastfeeding:String?
    var interestedForBreastfeeding:String?
    var chatGroup:ChatGroupModel?
    var socialId:String?
    var userImage:String?
    var password:String?
    var interestedNumber:Int?
    var deliveryDate:String?
    enum CodingKeys: String, CodingKey {
        case fullName      = "fullName"
        case userId        = "_id"
        case email         = "email"
        case accessToken   = "accessToken"
        case firstName     = "firstName"
        case lastName      = "lastName"
        case existingUser    = "existingUser"
        case socialType    = "socialType"
        case isProfileUpdate = "isProfileUpdate"
        case  selectAge = "selectAge"
        case  selectChildAge = "selectChildAge"
        case location = "location"
        case lat = "lat"
        case long = "long"
        case timeForBreastfeeding = "timeForBreastfeeding"
        case interestedForBreastfeeding = "interestedForBreastfeeding"
        case chatGroup = "chatGroup"
        case socialId = "socialId"
        case userImage = "imageUrl"
        case password = "password"
        case interestedNumber = "interestedNumber"
        case deliveryDate = "deliveryDate"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        userId         =  (values.contains(.userId)        == true) ? try values.decodeIfPresent(String.self, forKey: .userId) : ""
        
        fullName         =  (values.contains(.fullName)        == true) ? try values.decodeIfPresent(String.self, forKey: .fullName) : ""
        
        socialType  =  (values.contains(.socialType)        == true) ? try values.decodeIfPresent(String.self, forKey: .socialType) : ""
        
        accessToken    =  (values.contains(.accessToken)   == true) ? try values.decodeIfPresent(String.self, forKey: .accessToken) : ""
        
        email          =  (values.contains(.email)         == true) ? try values.decodeIfPresent(String.self, forKey: .email) : ""
        firstName      =  (values.contains(.firstName)     == true) ? try values.decodeIfPresent(String.self , forKey: .firstName) : ""
        lastName       =  (values.contains(.lastName)      == true) ? try values.decodeIfPresent(String.self , forKey: .lastName) : ""
        existingUser       =  (values.contains(.existingUser)      == true) ? try values.decodeIfPresent(Bool.self , forKey: .existingUser) : false
        isProfileUpdate  =  (values.contains(.isProfileUpdate)      == true) ? try values.decodeIfPresent(Bool.self , forKey: .isProfileUpdate) : false
        selectAge = (values.contains(.selectAge) == true) ? try values.decodeIfPresent(String.self, forKey: .selectAge) : ""
        selectChildAge = (values.contains(.selectChildAge) == true) ? try values.decodeIfPresent(String.self, forKey: .selectChildAge) : ""
        location = (values.contains(.location) == true) ? try values.decodeIfPresent(String.self, forKey: .location) : ""
        lat = (values.contains(.lat) == true) ? try values.decodeIfPresent(Double.self, forKey: .lat) : 0.0
        long = (values.contains(.long) == true) ? try values.decodeIfPresent(Double.self, forKey: .long) : 0.0
        timeForBreastfeeding = (values.contains(.timeForBreastfeeding) == true) ? try values.decodeIfPresent(String.self, forKey: .timeForBreastfeeding) : ""
        interestedForBreastfeeding = (values.contains(.interestedForBreastfeeding) == true) ? try values.decodeIfPresent(String.self, forKey: .interestedForBreastfeeding) : ""
        chatGroup = (values.contains(.chatGroup) == true) ? try values.decodeIfPresent(ChatGroupModel.self, forKey: .chatGroup) : nil
        socialId = (values.contains(.socialId) == true) ? try values.decodeIfPresent(String.self, forKey: .socialId) : ""
        userImage = (values.contains(.userImage) == true) ? try values.decodeIfPresent(String.self, forKey: .userImage) : ""
        password  = (values.contains(.password) == true) ? try values.decodeIfPresent(String.self, forKey: .password) : ""
        interestedNumber = (values.contains(.interestedNumber) == true) ? try values.decodeIfPresent(Int.self, forKey: .interestedNumber) :0
        deliveryDate = (values.contains(.deliveryDate) == true) ? try values.decodeIfPresent(String.self, forKey: .deliveryDate) : ""
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(userId, forKey: .userId)
        try container.encode(socialType, forKey: .socialType)
        try container.encode(email, forKey: .email)
        try container.encode(accessToken, forKey: .accessToken)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(existingUser, forKey: .existingUser)
        try container.encode(isProfileUpdate, forKey: .isProfileUpdate)
        try container.encode(selectAge, forKey: .selectAge)
        try container.encode(selectChildAge, forKey: .selectChildAge)
        try container.encode(location, forKey: .location)
        try container.encode(lat, forKey: .lat)
        try container.encode(long, forKey: .long)
        try container.encode(timeForBreastfeeding, forKey: .timeForBreastfeeding)
        try container.encode(interestedForBreastfeeding, forKey: .interestedForBreastfeeding)
        try container.encode(chatGroup, forKey: .chatGroup)
        try container.encode(socialId, forKey: .socialId)
        try container.encode(userImage, forKey: .userImage)
        try container.encode(password, forKey: .password)
        try container.encode(fullName, forKey: .fullName)
        try container.encode(interestedNumber, forKey: .interestedNumber)
        try container.encode(deliveryDate, forKey: .deliveryDate)
    }
}

struct ChatGroupModel:Mappable {
    var age:String?
    enum CodingKeys: String, CodingKey {
        case age    = "chatGroup"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        age         =  (values.contains(.age)        == true) ? try values.decodeIfPresent(String.self, forKey: .age) : ""
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(age, forKey: .age)
    }
}

