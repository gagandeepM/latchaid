//
//  NotificationModel.swift
//  LatchAid
//
//  Created by gagandeepmishra on 29/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation

struct NotificationResponse:Mappable
{
    var notifications:[Notification] = [Notification]()
    let statusCode:Int?
    let message:String?
    enum CodingKeys:String,CodingKey {
        case statusCode = "code"
        case message = "message"
        case notifications = "data"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        guard let notificationsData = try values.decodeIfPresent([Notification].self, forKey: .notifications)else{return}
        for notification in notificationsData
        {
            self.notifications.append(notification)
        }
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(notifications, forKey: .notifications)
        try container.encode(statusCode, forKey: .statusCode)
        try container.encode(message, forKey: .message)
    }
}
struct Notification:Mappable
{
    let notificationId:String?
    let count:Int?
    let channelNotification:ChannelNotification?
    enum CodingKeys:String,CodingKey {
        case notificationId = "_id"
        case count = "count"
        case channelNotification = "channelId"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        notificationId = try values.decodeIfPresent(String.self, forKey: .notificationId)
        channelNotification = try values.decodeIfPresent(ChannelNotification.self, forKey: .channelNotification)
       
    }
     func encode(to encoder: Encoder) throws {
          var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(notificationId, forKey: .notificationId)
        try container.encode(count, forKey: .count)
        try container.encode(channelNotification, forKey: .channelNotification)
    }
}
struct ChannelNotification:Mappable {
    let channelId:String?
    let channelName:String?
    let imageUrl:String?
    enum CodingKeys:String,CodingKey {
        case channelId = "_id"
        case channelName = "channelName"
        case imageUrl = "imageUrl"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        channelId = try values.decodeIfPresent(String.self, forKey: .channelId)
        channelName = try values.decodeIfPresent(String.self, forKey: .channelName)
        imageUrl = try values.decodeIfPresent(String.self, forKey: .imageUrl)
        
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(channelId, forKey: .channelId)
        try container.encode(channelName, forKey: .channelName)
        try container.encode(imageUrl, forKey: .imageUrl)
    }
    
}
