//
//  S3Manager.swift
//  LatchAid
//
//  Created by gagandeepmishra on 23/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import AWSS3
import AWSCore
class S3Bucket{
    
let accessKey = "AKIAI5LR4JLCELID6H4A"
let secretKey = "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"

    typealias successBlock =  (_ result: Bool,_ response: String?) -> Void
    
    func uploadImageToAWS(_ image:UIImage, _ globalPath:String, _ localPath:String ,completion: @escaping successBlock) {
        
        let accessKey = kAWSAccessKey
        let secretKey = kAWSSecretKey
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.EUWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(localPath)
        let data = image.jpegData(compressionQuality: 0.75)
        do {
            try data?.write(to: fileURL)
        }
        catch {}
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = globalPath
        uploadRequest.bucket = kAWSBucketName
        uploadRequest.contentType = kAWSContentType
        uploadRequest.acl = .publicRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith {(task) -> Any? in
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
                completion(false, error.localizedDescription)
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    completion(true, absoluteString)
                }
            }
            return nil
        }
    }
    func uploadAudioToAWS(audio:String, _ globalPath:String, _ localPath:String ,completion: @escaping successBlock) {
        
        let accessKey = kAWSAccessKey
        let secretKey = kAWSSecretKey
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.EUWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
//        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(localPath)
       
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = URL(string:audio)!
        uploadRequest.key = globalPath
        uploadRequest.bucket = kAWSBucketName
        uploadRequest.contentType = "audio/mp4"
        uploadRequest.acl = .publicRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith {(task) -> Any? in
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
                completion(false, error.localizedDescription)
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    completion(true, absoluteString)
                }
            }
            return nil
        }
    }
    
    func uploadFilesToAWS(file:String, _ globalPath:String, _ localPath:String ,completion: @escaping successBlock) {
        
        let accessKey = kAWSAccessKey
        let secretKey = kAWSSecretKey
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USWest2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        let fileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(localPath)
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()!
        uploadRequest.body = fileURL
        uploadRequest.key = globalPath
        uploadRequest.bucket = kAWSBucketName
        uploadRequest.contentType = "string"
        uploadRequest.acl = .publicRead
        
        let transferManager = AWSS3TransferManager.default()
        
        transferManager.upload(uploadRequest).continueWith {(task) -> Any? in
            
            if let error = task.error {
                print("Upload failed with error: (\(error.localizedDescription))")
                completion(false, error.localizedDescription)
            }
            
            if task.result != nil {
                let url = AWSS3.default().configuration.endpoint.url
                let publicURL = url?.appendingPathComponent(uploadRequest.bucket!).appendingPathComponent(uploadRequest.key!)
                if let absoluteString = publicURL?.absoluteString {
                    completion(true, absoluteString)
                }
            }
            return nil
        }
    }
    //    func contentType(localPath:String) -> String {
    //
    //        if (localPath as NSString).pathExtension.lowercased() == "mp4" {
    //            return "video/mp4"
    //        }else if (localPath as NSString).pathExtension.lowercased() == "png" {
    //            return "image/png"
    //        }else if ((localPath as NSString).pathExtension.lowercased() == "jpg" || (localPath as NSString).pathExtension.lowercased() == "jpeg"){
    //            return "image/jpeg"
    //        }
    //            return "binary/octet-stream"
    //    }
    
   
}
