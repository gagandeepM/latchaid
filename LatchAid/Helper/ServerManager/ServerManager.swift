//
//  ServerManager.swift

import UIKit
import CFNetwork
import Alamofire
import MobileCoreServices

typealias ServerSuccessCallBack = (_ data:Data)->Void
typealias ServerFailureCallBack=(_ error:Error?)->Void
typealias ServerProgressCallBack = (_ progress:Double?) -> Void
typealias ServerNetworkConnectionCallBck = (_ reachable:Bool) -> Void

class ServerManager: NSObject {
    var hud:CustomProgressHUD!
    var networkManager:NetworkReachabilityManager{
        struct  Singlton{
            static let instance = NetworkReachabilityManager()
        }
        return Singlton.instance!
    }
    
    override init() {
        super.init()
    }
    var currentRequest: Alamofire.Request? = nil{
        didSet{
            cancelledRequest()
        }
    }
    func cancelledRequest(){
        backgroundManager.session.getAllTasks(completionHandler: {$0.forEach({ (task) in
            if let request = task.currentRequest , let current = self.currentRequest?.request , request.url! != current.url!{
                task.cancel()
            }
        })})
    }
    class var shared:ServerManager{
        struct  Singlton{
            static let instance = ServerManager()
        }
        return Singlton.instance
    }
    //MARK:- documentsDirectoryURL -
    lazy var documentsDirectoryURL : URL = {
        
        let documents = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        return documents
    }()
    //MARK:- backgroundManager -
    private lazy var backgroundManager: Alamofire.SessionManager = {
        let bundleIdentifier = Bundle.main.bundleIdentifier
        let configure  = URLSessionConfiguration.background(withIdentifier: bundleIdentifier! + ".background")
        // configure.timeoutIntervalForRequest = 30
        
        let session = Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier: bundleIdentifier! + ".background"))
        session.startRequestsImmediately = true
        return session
    }()
    //MARK:- sessionManager -
    private lazy var sessionManager: Alamofire.SessionManager = {
        
        let configure  = URLSessionConfiguration.default
        configure.timeoutIntervalForRequest = 120
        configure.httpMaximumConnectionsPerHost = 20
        //configure.timeoutIntervalForResource = 60
        
        return Alamofire.SessionManager(configuration: configure)
    }()
    //MARK:- backgroundCompletionHandler -
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return backgroundManager.backgroundCompletionHandler
        }
        set {
            backgroundManager.backgroundCompletionHandler = newValue
        }
        
    }
    
    //MARK:- showProgressHud-
    func showHud(inView view:UIView = AppDelegate.sharedDelegate.window!,message title:String = ""){
        
        self.hideHud()
        hud = CustomProgressHUD.showProgressHud(inView: view, titleLabel: title)
        hud.lineColor = CustomColor.DeepOrange
        hud.setNeedsLayout()
        
    }
    //MARK:- showProgressBarHud-
    func showBarHud(inView view:UIView = AppDelegate.sharedDelegate.window! ,message title:String = "",progressMode mode:CustomProgressHUDMode = .DefaultHorizontalBar) ->CustomProgressHUD {
        
        hud = CustomProgressHUD.showProgressHud(inView: view, progressMode: mode, titleLabel: title)
        hud.lineColor = CustomColor.DeepOrange
        return hud
        
    }
    func updateProgress(value:Float,title:String = ""){
        if (hud != nil), value < 1.0  {
            hud.progress = value
            if !hud.messageString.isEmpty , !title.isEmpty {
                let percentage = value*100
                hud.messageString = "\(title) \(percentage)"
            }
            
        }
        
    }
    //MARK:- hideHud-
    func hideHud(){
        
        if (hud != nil) {
            hud.hideHud(animated: true)
            hud.removeFromSuperview()
        }
        
    }
    
    var isNetworkActivity:Bool = false{
        didSet{
            UIApplication.shared.isNetworkActivityIndicatorVisible = isNetworkActivity
        }
    }
    //MARK: - checkNetworkConnetion-
    func CheckNetwork() -> Bool
    {
        let isReachable = self.networkManager.isReachable//Reachability.isConnectedToNetwork()
        if !isReachable {
            DispatchQueue.main.async {
                self.hideHud()
                if let controller = currentController{
                    controller.showAlertAction(title: "No Internet Connection!", message: "The Internet connection appears to be offline.", cancelTitle: "OK", otherTitle: "Settings!", onCompletion: { (index) in
                        if index == 2{
                            AppDelegate.sharedDelegate.openSettings()
                            
                        }
                    })
                    
                }
            }
        }
        return isReachable
        
    }
    
    var apiHeaders: HTTPHeaders? {
        return ["Authorization":"\(accessToken)"]
        
        
    }
    var contentHeader:HTTPHeaders?
    {
        return ["Content-Type":"multipart/form-data"]
    }
    
    //MARK:- onResponseBlock
    fileprivate func onResponseBlock(response:DataResponse<Any>,onSuccessHandler:@escaping ServerSuccessCallBack,onFailureHandler:@escaping ServerFailureCallBack){
        if let error  = response.error {
            onFailureHandler(error)
        }else if let data = response.data{
            onSuccessHandler(data)
        }
        
    }
    
    //MARK:- requestTask
    fileprivate func requestTask(url:URLConvertible,method:HTTPMethod,isBackgroundTask isBackground:Bool = false,params:[String:Any]?, headers: HTTPHeaders? = nil,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        print("service name =\(url)")
        print("service params =\(String(describing: params))")
        print("Header Params = \(String(describing: headers))")
        if isBackground {
            
            currentRequest =  backgroundManager.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { (response:DataResponse<Any>) in
                
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        print("response\(response)")
                        self.onResponseBlock(response: response, onSuccessHandler: successHandler,onFailureHandler: failureHandler)
                        
                    }
                    break
                    
                case .failure(_):
                    if response.result.error!._code == NSURLErrorTimedOut {
                        ServerManager.shared.hideHud()
                        return
                    }
                    failureHandler(response.result.error!)
                    
                    break
                    
                }
            })
            currentRequest?.resume()
            self.backgroundManager.delegate.sessionDidFinishEventsForBackgroundURLSession = {
                session in
                // record the fact that we're all done moving stuff around
                // now, call the saved completion handler
                self.backgroundCompletionHandler?()
                self.backgroundCompletionHandler = nil
            }
            
            self.backgroundManager.backgroundCompletionHandler = {
                // finshed task
            }
        }else{
            currentRequest = sessionManager.request (url, method:method, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response:DataResponse<Any>) in
                switch(response.result) {
                case .success(_):
                    if response.result.value != nil{
                        print("response\(response)")
                        self.onResponseBlock(response: response, onSuccessHandler: successHandler,onFailureHandler: failureHandler)
                        
                    }
                    break
                    
                case .failure(_):
                    if response.result.error!._code == NSURLErrorTimedOut {
                        ServerManager.shared.hideHud()
                        return
                    }
                    failureHandler(response.result.error!)
                    
                    break
                    
                }
            }
            currentRequest?.resume()
        }
    }
    
    //MARK:-uploadTask
    fileprivate func uploadTask(url:URLConvertible,method:HTTPMethod,params:[String:Any]?, isBackgroundTask isBackground:Bool = false,headers: HTTPHeaders? = nil ,multipartObject :[MultipartData]?,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack,progressHandler:ServerProgressCallBack? = nil){
        
        print(url)
        print(method)
        print(params)
        print(multipartObject)
        print(headers)
        
        if (multipartObject != nil) , multipartObject!.count > 0{
            if isBackground {
                backgroundManager.upload(multipartFormData: { (multipartFormData) in
                    if let mediaList  = multipartObject
                    {
                        for object in mediaList
                        {
                            multipartFormData.append(object.media, withName: object.mediaUploadKey, fileName: object.fileName, mimeType: object.mimType)
                        }
                    }
                    if (params != nil){
                        for (key, value) in params! {
                            multipartFormData.append("\(value)".data(using:.utf8)!, withName: key)
                        }
                    }
                }, to: "\(url)",headers:headers, encodingCompletion: { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        self.currentRequest = upload
                        upload.uploadProgress(closure: { (Progress) in
                            print("Upload Progress: \(Progress.fractionCompleted)")
                            if let progressHandler = progressHandler{
                                progressHandler(Progress.fractionCompleted)
                            }
                        })
                        upload.resume()
                        upload.responseJSON { response in
                            if response.result.value != nil{
                                print("response\(response)")
                                self.onResponseBlock(response: response, onSuccessHandler: successHandler,onFailureHandler: failureHandler)
                                
                            }
                        }
                        
                    case .failure(let encodingError):
                        
                        failureHandler(encodingError as NSError )
                        
                    }
                })
                backgroundManager.delegate.sessionDidFinishEventsForBackgroundURLSession = {
                    session in
                    // record the fact that we're all done moving stuff around
                    // now, call the saved completion handler
                    self.backgroundCompletionHandler?()
                    self.backgroundCompletionHandler = nil
                }
                backgroundManager.backgroundCompletionHandler = {
                    // finshed task
                }
            }else{
                sessionManager.upload(multipartFormData: { (multipartFormData) in
                    if let mediaList  = multipartObject
                    {
                        for object in mediaList
                        {
                            multipartFormData.append(object.media, withName: object.mediaUploadKey, fileName: object.fileName, mimeType: object.mimType)
                        }
                    }
                    if let params = params {
                        for (key, value) in params {
                            multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                        }
                    }
                }, to: url,headers:headers, encodingCompletion: { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        self.currentRequest = upload
                        upload.uploadProgress(closure: { (Progress) in
                            print("Upload Progress: \(Progress.fractionCompleted)")
                            if let progressHandler = progressHandler {
                                progressHandler(Progress.fractionCompleted)
                            }
                            
                        })
                        upload.resume()
                        
                        upload.responseJSON { response in
                            if response.result.value != nil{
                                print("response\(response)")
                                self.onResponseBlock(response: response, onSuccessHandler: successHandler,onFailureHandler: failureHandler)
                            }
                        }
                        
                    case .failure(let encodingError):
                        
                        failureHandler(encodingError)
                        
                    }
                })
            }
            
        }else{
            self.requestTask(url: url, method: method, isBackgroundTask: isBackground, params: params, headers: headers, successHandler: successHandler, failureHandler: failureHandler)
        }
        
        
    }
    
    //MARK:- httpBackgroundTaskRequest-
    func httpBackgroundRequest(request api:String,params:[String:Any]? ,method :HTTPMethod = .post,headers: HTTPHeaders? = nil,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        self.requestTask(url: "\(api)", method: method, params: params, successHandler: successHandler, failureHandler: failureHandler)
        
    }
    
    //MARK: - httpDelete -
    func httpDelete(request api:String,params:[String:Any]?, isBackgroundTask isBackground:Bool = false, headers: HTTPHeaders? = nil,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        
        self.requestTask(url: "\(api)", method: .delete,isBackgroundTask: isBackground, params: params, headers: headers,successHandler: successHandler, failureHandler: failureHandler)
        
    }
    // MARK:- httpPut
    func httpPut(request api:String,params:[String:Any]?, isBackgroundTask isBackground:Bool = false,headers: HTTPHeaders? = nil,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        self.requestTask(url: "\(api)", method: .put, isBackgroundTask: isBackground, params: params, headers: headers, successHandler: successHandler, failureHandler: failureHandler)
    }
    //MARK:- httpPost
    func httpPost(request api:String,params:Parameters?, isBackgroundTask isBackground:Bool = false,headers: HTTPHeaders? = nil,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        self.requestTask(url: "\(api)", method: .post, isBackgroundTask: isBackground, params: params, headers: headers, successHandler: successHandler, failureHandler: failureHandler)
    }
    //MARK:- httpGetRequest
    func httpGet(request api:String,params:[String:Any]?, isBackgroundTask isBackground:Bool = false,headers: HTTPHeaders? = nil ,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack){
        
        self.requestTask(url: "\(api)", method: .get, isBackgroundTask: isBackground, params: params, headers: headers, successHandler: successHandler, failureHandler: failureHandler)
    }
    //MARK:- httpUploadRequest
    func httpUpload(request api:String,params:[String:Any]?,  isBackgroundTask isBackground:Bool = false,headers: HTTPHeaders? = nil ,multipartObject :[MultipartData]?,successHandler:@escaping ServerSuccessCallBack,failureHandler:@escaping ServerFailureCallBack,progressHandler:ServerProgressCallBack? = nil){
        self.uploadTask(url: "\(api)", method: .put, params: params, isBackgroundTask: isBackground, headers: headers, multipartObject: multipartObject, successHandler: successHandler, failureHandler: failureHandler, progressHandler: progressHandler)
        
    }
    //MARK:- httpDownloadRequest
    func httpDownload(request api:String ,isBackgroundTask isBackground:Bool = false ,successHandler:@escaping (_ destinationURL:URL?)->Void,failureHandler:ServerFailureCallBack?,progressHandler:ServerProgressCallBack? = nil){
        print("\(api)")
        guard let fileUrl  = URL(string: "\(api)") else {
            
            if (failureHandler != nil){
                
                let errorTemp = SMError(localizedTitle: "file url incorrect", localizedDescription: "file url incorrect", code: 500)
                failureHandler!(errorTemp )
            }
            return
        }
        let request = URLRequest(url: fileUrl)
        let destination: DownloadRequest.DownloadFileDestination = { filePath,response in
            
            let directory : NSURL = (self.documentsDirectoryURL as NSURL)
            
            let fileURL =   directory.appendingPathComponent(response.suggestedFilename!)!
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        if isBackground {
            backgroundManager.download(request, to: destination).response(completionHandler: { (response:DefaultDownloadResponse) in
                
                if response.error != nil{
                    if let failureHandler = failureHandler  {
                        failureHandler(response.error! as NSError )
                    }
                }
                else{
                    if  response.response?.statusCode == 200{
                        successHandler(response.destinationURL)
                    }else if response.response?.statusCode == 401{
                        
                        if let failureHandler = failureHandler {
                            let localizedString = HTTPURLResponse.localizedString(forStatusCode: 401)
                            let error  = SMError(localizedTitle: nil, localizedDescription: localizedString, code: 401)
                            failureHandler(error  as NSError )
                        }
                        
                        
                    }
                }
                
            }).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
                if let onProgress = progressHandler {
                    onProgress(progress.fractionCompleted)
                }
                }.resume()
            
            backgroundManager.delegate.sessionDidFinishEventsForBackgroundURLSession = {
                session in
                // record the fact that we're all done moving stuff around
                // now, call the saved completion handler
                self.backgroundCompletionHandler?()
                self.backgroundCompletionHandler = nil
            }
            backgroundManager.backgroundCompletionHandler = {
                // finshed task
            }
            
        }else{
            
            sessionManager.download(request, to: destination).validate(statusCode: 200..<300).response(completionHandler: { (response:DefaultDownloadResponse) in
                
                if response.error != nil{
                    if let failureHandler = failureHandler{
                        failureHandler(response.error! as NSError )
                    }
                }
                else{
                    print("response\(response)")
                    if  response.response?.statusCode == 200{
                        
                        successHandler(response.destinationURL)
                        
                    }else if response.response?.statusCode == 401{
                        if let failureHandler = failureHandler{
                            let localizedString = HTTPURLResponse.localizedString(forStatusCode: 401)
                            let error  = SMError(localizedTitle: nil, localizedDescription: localizedString, code: 401)
                            failureHandler(error  as NSError )
                        }
                        
                    }
                }
                
            }).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
                if let progressHandler = progressHandler {
                    progressHandler(progress.fractionCompleted)
                }
                }.resume()
            
            
        }
        
        
        
    }
}
//MARK: - AppUtility -

class AppUtility:NSObject{
    
    //MARK:- mimeTypeForPath-
    class func mimeType(forPath filePath:URL)->String
    {
        var  mimeType:String;
        
        let fileExtension:CFString = filePath.pathExtension as CFString
        let UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil);
        let str = UTTypeCopyPreferredTagWithClass(UTI!.takeUnretainedValue(), kUTTagClassMIMEType);
        if (str == nil) {
            mimeType = "application/octet-stream";
        } else {
            mimeType = str!.takeUnretainedValue() as String
        }
        return mimeType
    }
    //MARK:- filename-
    class func filename(Prefix:String , fileExtension:String)-> String
    {
        let dateformatter=DateFormatter()
        dateformatter.dateFormat="MddyyHHmmss"
        let dateInStringFormated=dateformatter.string(from: Date() )
        return "\(Prefix)_\(dateInStringFormated).\(fileExtension)"
    }
    
    
}

public enum ImageQuality : Int {
    case `default`
    case highest
    case high
    case medium
    case low
    case lowest
}
//MARK:- MultipartData
class MultipartData: NSObject
{
    var media:Data!
    var mediaUploadKey:String!
    var fileName:String!
    var mimType:String!
    var pathExtension:String!
    init(medaiObject object:Any!,fileName:String? = nil,mediaKey uploadKey:String!,quality:ImageQuality = .default) {
        if (object != nil) , (uploadKey != nil) {
            
            if let image = object as? UIImage {
                switch quality{
                case .highest:
                    self.media  = image.highestQualityJPEGNSData
                case .high:
                    self.media = image.highQualityJPEGNSData
                case .medium:
                    self.media  = image.mediumQualityJPEGNSData
                case .low:
                    self.media  = image.lowQualityJPEGNSData
                case .lowest:
                    self.media  = image.lowestQualityJPEGNSData
                default:
                    self.media  = image.uncompressedPNGData
                    
                }
                self.mimType =  quality == .default ? "image/png" : "image/jpeg"
                self.pathExtension = quality == .default ? "png" : "jpeg"
                if let filename = fileName{
                    self.fileName = filename
                    
                }else{
                    self.fileName = AppUtility.filename(Prefix: "image", fileExtension:  self.pathExtension)
                    
                }
                
                
                
            }else{
                
                if  let filepath = object as? String{
                    
                    let url = NSURL.fileURL(withPath: filepath)
                    self.pathExtension = url.pathExtension
                    self.fileName = url.lastPathComponent
                    self.media = try! Data(contentsOf: url) //NSData(contentsOf: url)
                    self.mimType = AppUtility.mimeType(forPath: url )
                    
                }else if  let fileurl = object as? URL {
                    self.pathExtension = fileurl.pathExtension
                    self.fileName = fileurl.lastPathComponent
                    
                    self.media = try! Data(contentsOf: fileurl) //NSData(contentsOf: url)
                    self.mimType = AppUtility.mimeType(forPath: fileurl )
                }
                
            }
            self.mediaUploadKey = uploadKey
        }
        
    }
    
    override init()
    {
        super.init()
        
    }
    required init(coder aDecoder: NSCoder) {
        
        if let media = aDecoder.decodeObject(forKey: "mediaData") as? Data {
            
            self.media = media
        }
        if let mediaUploadKey = aDecoder.decodeObject(forKey: "mediaUploadKey") as? String {
            
            self.mediaUploadKey = mediaUploadKey
        }
        if let fileName = aDecoder.decodeObject(forKey: "fileName") as? String {
            
            self.fileName = fileName
        }
        if let mimType = aDecoder.decodeObject(forKey: "mimType") as? String {
            
            self.mimType = mimType
        }
        
    }
    open func encodeWithCoder(_ aCoder: NSCoder)
    {
        if let media = self.media{
            aCoder.encode(media, forKey: "media")
        }
        if let mediaUploadKey = self.mediaUploadKey {
            aCoder.encode(mediaUploadKey, forKey: "mediaUploadKey")
        }
        if let fileName = self.fileName {
            aCoder.encode(fileName, forKey: "fileName")
        }
        if let mimType = self.mimType {
            aCoder.encode(mimType, forKey: "mimType")
        }
        
    }
    
}

extension AppDelegate{
    func openSettings(){
        let shared = UIApplication.shared
        let url = URL(string: UIApplication.openSettingsURLString)!
        if #available(iOS 10.0, *) {
            shared.open(url)
            
        } else {
            shared.openURL(url)
        }
    }
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        let bundleIdentifier = Bundle.main.bundleIdentifier!+".background"
        if application.backgroundRefreshStatus == .available , identifier == bundleIdentifier{
            ServerManager.shared.backgroundCompletionHandler = completionHandler
        }else{
            alertMessage = "Please enable the background mode before refreshing the data in background."
        }
        
    }
}
protocol OurErrorProtocol: Error {
    
    var localizedTitle: String { get }
    var localizedDescription: String { get }
    var code: Int { get }
}
struct SMError: OurErrorProtocol {
    
    var localizedTitle: String
    var localizedDescription: String
    var code: Int
    
    init(localizedTitle: String?, localizedDescription: String, code: Int) {
        self.localizedTitle = localizedTitle ?? "Error"
        self.localizedDescription = localizedDescription
        self.code = code
    }
}
