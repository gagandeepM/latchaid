//
//  Socket.swift
//  LatchAid
//
//  Created by gagandeepmishra on 22/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import SocketIO
enum LactchEvents:String {
    case receiveMessage = "getMessage"
    case readMessage = "messageRead"
    case sendMessage = "sendMessage"
    case socketRegister = "connect_user"
    case userOnlineStatus = "chatWith"
    case disconnect = "disconnect"
    case notification = "notification"
    case updateMessage = "getMessage1"
    case deleteMessage = "deleteMessage"
    
    
}
class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    lazy var manager:SocketManager = {
        return SocketManager(socketURL:  URL(string:KSocket)!, config: [.log(true), .forceWebsockets(true), .compress])
        
    }()
    var socket:SocketIOClient!
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func socketConnect(completion:(()->())? = nil) {
        
        if socket.status == .notConnected{
            socket.on(clientEvent: .connect) { (data, ack) in
                print("socket connected",data)
                completion?()
            }
        }
        if socket.status == .connected {
         //   getMessage()
        }
        socket.connect()
    }
  
     
 
        func socketDisconnect(data: [String:Any]) {
            socket.emit(LactchEvents.disconnect.rawValue,data)
            if socket.status == .connected{
                socket.on(clientEvent: .disconnect) { (data, ack) in
                    print("socket disconnect",data)
                   
                }
                   // socket.removeAllHandlers()
                   // manager.defaultSocket.disconnect()
                    socket.disconnect()
                
            }
        }
    func socketReconnect(){
        if socket.status == .disconnected {
            socket.on(clientEvent: .reconnectAttempt) { (data, ack) in
                print("socket Reconnect attempt **** ",data)
            }
            socket.on(clientEvent: .reconnect) { (data, ack) in
                print("socket starting reconnect **** ",data)
            }
        }
        
    }
    //    func userOnlineStatus(_ userInfo: [String: Any])
    //    {
    //         socket.emit(LactchEvents.socketRegister.description, userInfo)
    //    }
    
    //call on msg delet in list
    func deleteMsgUpdateList(event:LactchEvents,completion:@escaping()->Void)
    {
          socket.on(event.rawValue) { (list, ack) in
            
            completion()
        }
    }
    
    func getMessage(event:LactchEvents,completion:@escaping(MessageModel)->Void){
        //socket.once(event.rawValue) { (list, ack) in
        
        socket.on(event.rawValue) { (list, ack) in
            print("receiveMessage*****",list)
            let object  = list[0] as! NSDictionary
            
            print("socket*Chanel*Resp---->",object)
            guard object.value(forKey: "code")as? Int != 400 else{return}
            
            
            if let data1 = object.value(forKey: "data")as? [String:Any]
            {
                do{
                    let data =  try JSONSerialization.data(withJSONObject: data1, options:.prettyPrinted)//prettyPrinted
                    
                    if  let dataModel =  data.JKDecoder(MessageModel.self)
                    {
                        completion(dataModel)
                    }
                    
                }
                catch{
                    print("Error")
                }
                
            }else if let data2 = object.value(forKey: "data")as? [[String:Any]]
            {
                for msg in data2{
                    do{
                        let data =  try JSONSerialization.data(withJSONObject: msg, options:.prettyPrinted)//prettyPrinted
                        
                        if  let dataModel =  data.JKDecoder(MessageModel.self)
                        {
                            completion(dataModel)
                        }
                        
                    }
                    catch{
                        print("Error")
                    }
                }
            }
            
        }
        socket.on(LactchEvents.readMessage.rawValue) { (data, ack) in
            print("readMsg*****",data)
        }
        print("chat Update")
        
    }

    func sendMessageNew(message: [String:Any]) {
      
           socket.emit(LactchEvents.sendMessage.rawValue,message)
        
        
    }
    func deletSendMsgs(message: [String:Any]) {
        
        socket.emit(LactchEvents.deleteMessage.rawValue,message)
        
        
    }
    
    func userOnline(completionHandler: @escaping (_ memberOnline: Int, _ onlineCount:Int,_ channelId:String,_ channelName:String,_ channelImage:String,_ typeOfChannel:String) -> Void) {
        
        socket.on(LactchEvents.socketRegister.rawValue) { (results, socketAck) -> Void in

             for result in results
            {
                print(result)
                if let count = result as? [String:Any]
                {
                    print(count)
                   let onlineCount =  count["onlineCount"] as? Int
                   let memberOnline = count["memberCount"] as? Int
                    let channelName = count["channelName"] as? String
                    let channelImage = count["channelImage"] as? String
                    let typeOfChannel = count["typeOfChannel"] as? String
                    let channelId = count["channelId"] as? String
                    
                     //   self.reset(LactchEvents.socketRegister)
                    completionHandler(memberOnline ?? 0,onlineCount ?? 0,channelId ?? "",channelName ?? "",channelImage ?? "",typeOfChannel ?? "")
                }
            }

        }
      
//        self.receiveObject(.socketRegister) { (res) in
//            if let count = res as? Int
//            {
//                self.reset(LactchEvents.socketRegister)
//                completionHandler(count)
//            }
//        }
        
    }
    func getMessageNotification(notification: [String:Any])
    {
        socket.emit(LactchEvents.notification.rawValue,notification)
    }
    func getNotification(userId:String)
    {
        socket.emit(LactchEvents.notification.rawValue,userId)
    }
    func getChatMsgFor(notification: [String:Any])
    {
        socket.emit(LactchEvents.updateMessage.rawValue,notification)
    }
    func socketOn(channelData:[String:Any]) {
        
        socket.emit(LactchEvents.socketRegister.rawValue,channelData)
        
        
    }
    func readMessage(completionHandler: @escaping (_ messageInfo: [String: Any]) -> Void){
        socket.on(LactchEvents.readMessage.rawValue) { (result, socketAck) -> Void in
            print(result)
            let receiveData = result[0] as? [String:Any] ?? [:]
            
            completionHandler(receiveData)
        }
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: MessageModel) -> Void) {
        self.receiveJSON(.receiveMessage, completion: completionHandler)
      
    }
    func getChatMsg(completionHandler: @escaping (_ messageInfo: [MessageModel]) -> Void)
    {
        socket.on(LactchEvents.updateMessage.rawValue) { (lists, ack) in
            
          for list in lists
          {
            
            
            }
        }
    }
    func reset(_ event:LactchEvents)
    {
        socket.off(event.rawValue)
    }
}
extension SocketIOManager{
    func sendObject(_ event:LactchEvents, param:[String:Any]){
        socket.emit(event.rawValue, param)
        
    }
    func sendJSON<T:Mappable>(_ event:LactchEvents, param:T){
        if let params = param.jsonObject as? [String:Any]{
            socket.emit(event.rawValue, params)
            
        }
        
    }
    func sendString(_ event:LactchEvents, value:String){
        socket.emit(event.rawValue, value)
        
    }
    func sendData(_ event:LactchEvents, data:Data){
        socket.emit(event.rawValue, data)
        
    }
    
    func receiveObject(_ event:LactchEvents,completion:@escaping(Any)->Void){
        socket.on(event.rawValue) { (list, ack) in
            completion(list[0])
        }
        
    }
    func receiveJSON<T:Mappable>(_ event:LactchEvents,completion:@escaping(T)->Void){
        
        socket.on(event.rawValue) { (list, ack) in
            
            
            let object  = list[0] as! NSDictionary
            
            print("socket*Chanel*Resp---->",object)
            guard object.value(forKey: "code")as? Int != 400 else{return}
            if let data1 = object.value(forKey: "data")as? [String:Any]
            {
            do{
                let data =  try JSONSerialization.data(withJSONObject: data1, options:.prettyPrinted)//prettyPrinted
                
                if  let dataModel =  data.JKDecoder(T.self)
                {
                    if  event == .receiveMessage{
                        self.reset(event)
                    }
                    
                    completion(dataModel)
                }
                
                
            }
            catch{
                print("Error")
            }
            
            }
            
            //            if let msgData = object.value(forKey: "data")as? NSArray
            //            {
            //                print("Socket*msgCount----->",msgData.count)
            //                guard msgData.count>0 else{return}
            //
            //                for da in msgData
            //                {
            //                    do{
            //                        let data =  try JSONSerialization.data(withJSONObject: da, options:.prettyPrinted)//prettyPrinted
            //
            //                        if  let dataModel =  data.JKDecoder(T.self)
            //                        {
            //                            if  event == .receiveMessage {
            //                                self.reset(event)
            //                            }
            //                            completion(dataModel)
            //                        }
            //                    }
            //                    catch{
            //                        print("Error")
            //                    }
            //                }
            //            }else{
            //                print("Socket*msg----->",object)
            //                do{
            //                    let data =  try JSONSerialization.data(withJSONObject: object, options:.prettyPrinted)//prettyPrinted
            //
            //                    if  let dataModel =  data.JKDecoder(T.self)
            //                    {
            //                        if  event == .receiveMessage{
            //                            self.reset(event)
            //                        }
            //
            //                        completion(dataModel)
            //                    }
            //
            //
            //                }
            //                catch{
            //                    print("Error")
            //                }
            //            }
        }
        
    }
   
}
