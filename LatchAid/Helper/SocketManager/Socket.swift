//
//  Socket.swift
//  LatchAid
//
//  Created by gagandeepmishra on 22/07/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//

import Foundation
import SocketIO
enum LactchEvents:String {
    case receiveMessage = "receiveMessage"
    case readMessage = "messageRead"
    case sendMessage = "sendMessage"
    case socketRegister = "connect_user"
    case userOnlineStatus = "chatWith"
    case disconnect = "disconnect"
    
}
class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    lazy var manager:SocketManager = {
        return SocketManager(socketURL:  URL(string:KSocket)!, config: [.log(true), .forceWebsockets(true), .compress])
        
    }()
    var socket:SocketIOClient!
    
    override init() {
        super.init()
        socket = manager.defaultSocket
    }
    
    func socketConnect(completion:(()->())? = nil) {
        
        if socket.status == .notConnected{
            socket.on(clientEvent: .connect) { (data, ack) in
                print("socket connected",data)
                completion?()
            }
            
            
        }
        if socket.status == .connected {
            getMessage()
        }
        socket.connect()
        
        
    }
    
        func socketDisconnect(data: [String:Any]) {
            socket.emit(LactchEvents.disconnect.rawValue,data)
            if socket.status == .connected{
                socket.on(clientEvent: .disconnect) { (data, ack) in
                    print("socket disconnect",data)
                   
                }
                socket.disconnect()
            }
    
    
        }
    func socketReconnect(){
        if socket.status == .disconnected {
            socket.on(clientEvent: .reconnectAttempt) { (data, ack) in
                print("socket Reconnect attempt **** ",data)
            }
            socket.on(clientEvent: .reconnect) { (data, ack) in
                print("socket starting reconnect **** ",data)
            }
        }
        
    }
    //    func userOnlineStatus(_ userInfo: [String: Any])
    //    {
    //         socket.emit(LactchEvents.socketRegister.description, userInfo)
    //    }
    func getMessage(){
        socket.on(LactchEvents.receiveMessage.rawValue) { (data, ack) in
            
        }
        socket.on(LactchEvents.readMessage.rawValue) { (data, ack) in
            
        }
    }
    func sendMessageNew(message: [String:Any]) {
      
           socket.emit(LactchEvents.sendMessage.rawValue,message)
        
        
    }
    
    func userOnline(completionHandler: @escaping (_ messageInfo: Int) -> Void) {
        
        socket.on(LactchEvents.socketRegister.rawValue) { (results, socketAck) -> Void in
            
            for result in results
            {
                print(result)
                if let count = result as? Int
                {
                    print(count)
                    completionHandler(count ?? 0)
                }
            }
            
        }
        
    }
    
    func socketOn(channelData:[String:Any]) {
        
        socket.emit(LactchEvents.socketRegister.rawValue,channelData)
        
        
    }
    func readMessage(completionHandler: @escaping (_ messageInfo: [String: Any]) -> Void){
        socket.on(LactchEvents.readMessage.rawValue) { (result, socketAck) -> Void in
            print(result)
            let receiveData = result[0] as? [String:Any] ?? [:]
            
            completionHandler(receiveData)
        }
    }
    func getChatMessage(completionHandler: @escaping (_ messageInfo: MessageModel) -> Void) {
        
        socket.on(LactchEvents.receiveMessage.rawValue) { (dataArray, socketAck) -> Void in
            
            guard let receiveData = dataArray[0] as? [String:Any]   else{return}
            do{
            let data =  try JSONSerialization.data(withJSONObject: receiveData, options:.prettyPrinted)
                if  let dataModel =  data.JKDecoder(MessageModel.self)
                {
                    self.reset(.receiveMessage)
                    completionHandler(dataModel)
                }
            }
            catch{
                print("Error")
            }
          
            
           
           
            
           
        }
        
        
    }
    func reset(_ event:LactchEvents)
    {
        socket.off(event.rawValue)
    }
}
extension Socket{
    
}
