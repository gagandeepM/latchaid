//
//  Enum.swift


import Foundation
import UIKit
enum ApiEnvironment{
    case testing
    case stagging
    case production
    case socket
    var baseUrl:String{
        switch self {
        case .testing:
            return "http://45.33.68.242:4003/"
        case .production:
            return "http://45.33.68.242:4003/"
        case .socket:
            return "http://35.177.246.53:7000"
        default:
            return "http://35.177.246.53:7000/api/"
        }
    }
}
enum  LServiceType : Int{
    case none = 0
    case pagging = 1
    
    
}
enum ApiName {
    case login
    case signUp
    case forgotPassword
    case resetPassword
    case changePassword
    case logOut
    case getProfile
    case updateProfile
    case updateMyInfo
    case validateEmail
    case custom(name:String)
    case getChannelList
    case joinChat
    case exitChannel
    case notificationControl
    case sendMessage
    case editChannelImgae
    case getMessage
    case deleteMessage
    case reportUser
    case channelDetails
    case sharedContent
    case messageEvent
    case joinCheck
    case getNotification
    func api(environment:ApiEnvironment = .stagging)->String{
        switch self {
        case .login:
            return environment.baseUrl + klogin
        case .custom(let name ):
            return environment.baseUrl + name
        case .signUp:
            return environment.baseUrl + ksignup
        case .changePassword:
            return environment.baseUrl + KChangePassword
        case .forgotPassword:
            return environment.baseUrl + kForgotPassword
        case .resetPassword:
            return environment.baseUrl + kResetPassword
        case .logOut:
            return environment.baseUrl + KlogOut
        case .getProfile:
            return environment.baseUrl + KGetProfile
        case .updateProfile:
            return environment.baseUrl + KUpdateProfile
        case .validateEmail:
            return environment.baseUrl + KValidateEmail
            
        case .updateMyInfo:
            return environment.baseUrl + KUpdateMyInfo
        case .getChannelList:
            return environment.baseUrl + KGetChannelListing
        case .joinChat:
            return environment.baseUrl + KJoinChat
        case .exitChannel:
            return environment.baseUrl + KExitChannel
        case .notificationControl:
           return environment.baseUrl + KNotificationControl
            
        case .sendMessage:
            return environment.baseUrl + KSendMessage
        case .getMessage:
            return environment.baseUrl + KGetMessage
        case .deleteMessage:
            return environment.baseUrl + KDeleteMessage
        case .reportUser:
            return environment.baseUrl + KReportUser
            
        case .channelDetails:
            return environment.baseUrl + KChannelDetails
        case.sharedContent:
            return environment.baseUrl + KSharedContent
        case .messageEvent:
            return environment.baseUrl + KMessageEvent
        case .joinCheck:
            
            return environment.baseUrl + KJoinCheck
            
        case .getNotification:
            return environment.baseUrl + KGetNotification
        case .editChannelImgae:
            return environment.baseUrl + KeditChannelImgae
            
    }
}
}

public enum RectCornerRadiusType:Int {
    case topLeft
    case topRight
    case bottomLeft
    case bottomRight
    case all
    case incoming
    case outgoing
    var rectCorner:UIRectCorner{
        switch self {
        case .topLeft:
            return [.topLeft]
        case .topRight:
            return [.topRight]
        case .bottomLeft:
            return [.bottomLeft]
        case .bottomRight:
            return [.bottomRight]
        case .incoming:
            return [.topRight,.bottomLeft,.bottomRight]
        case .outgoing:
            return [.topLeft,.bottomLeft,.bottomRight]
        default:
            return [.allCorners]
        }
    }
}

enum SocialType{
    case facebook
    case google
    case `default`
    var loginType:String{
        switch self {
        case .facebook:
            return "facebook"
        case .google:
            return "google"
        case .default:
            return "normal"
        }
    }
}

enum FBPermissions:String {
    case email
    case publicProfile
    case birthday
    
    var permission:String{
        switch self {
        case .email:
            return "email"
        case .birthday:
            return "user_birthday"
        default:
            return "public_profile"
        }
    }
}


enum AppPermission {
    
    case network
    
    case notification
    
    
    
    
    func show(){
        
        var title:String = kAppTitle
        
        var message:String?
        
        switch self {
            
        case .notification:
            
            title = "\"\(kAppTitle)\" Notification disabled"
            
            message = "This app unable access to push notification. Please check app setting and enable the push notification, tap \"Settings\" and turn on \"Allow Notifications.\""
            
            
            
        case .network:
            
            title = "\"\(kAppTitle)\" Connection lost"
            
            message = "The Internet connection appears to be offline."
            
        }
        
        AppSettingAlert(title: title, message: message)
        
    }
    
    
    
}
