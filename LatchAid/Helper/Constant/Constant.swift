//
//  Constant.swift


import Foundation
import UIKit
import CoreLocation


enum MessageType:String {
    case text
    case media
    case audio
    case none
    case reply

}
enum MessageAction {
    case copy
    case forword
    case reply
    case delete
    case none
    
}
var instanceToken:String{
    set{
        UserDefaults.standard.set(newValue, forKey: "instanceToken")
        UserDefaults.standard.synchronize()
        
    }
    get{
        guard let instanceToken = UserDefaults.standard.object(forKey: "instanceToken") as? String else { return "" }
        return instanceToken
    }
}
enum ButtonSelection
{
    case media
    case audio
}
//let kAppImage:UIImage = appIcon ?? #imageLiteral(resourceName: "logo_orange_text")

var imageMessage = ""
var audioMessage  = ""
var copyMessage = false
let kAppTitle          =  (AppInfo.kAppTitle.isEmpty == true) ? "Dumpya":AppInfo.kAppTitle



// color

let primaryColor = UIColor(red: 2/255, green: 210/255, blue: 172/255, alpha: 1.0)
let buttonColor =  UIColor(red: 1/255, green: 159/255, blue: 130/255, alpha: 1.0)
let APPLE_LANGUAGE_KEY = "AppleLanguages"

let kOops                   = "Oops!!☹"
let kconnectionError        = "No Internet Connection!☹"
let KSuccess                = "Success!!😀"
let kDeviceToken            = "device_token"
let kInstanceToken          = "instance_Token"
let kUserDataKey            = "UserData"
let kAuthTokenKey           = "AuthToken"
let KSocialLogindata        = "SocialLoginData"
let KNormalSignup           = "NormalSignUp"
let kAppDelegate            = UIApplication.shared.delegate as! AppDelegate
let KAppDeletMsgAlert       = "if you delete delete own message it will delete for all the user"
let KAppDeletMsg            = "isMsgDelete" //true for delete // false not detele

var pageNumber:Int = 0
var totalRecords:Int = 0

//MARK: Set url here
let kApiEnvironment = ApiEnvironment.stagging
let KSocket = "http://35.177.246.53:7000"
//USER MANAGE MODULE
let ksignup          =  "users/register"
let klogin           =  "users/login"
let kForgotPassword  = "users/forgotPassword"
let kResetPassword   = "users/resetPassword"
let KChangePassword  = "users/changePassword"
let KlogOut          = "users/logOut"
let KGetProfile      = "users/getUserProfile?id="
let KUpdateProfile   = "users/editUserProfile"
let KUpdateMyInfo    = "users/editMyInfo"
let KValidateEmail   = "users/validate-email"
let KGetChannelListing = "users/searchChannel?channelName="
let KJoinChat        = "users/joinChannel"
let KExitChannel     = "users/exitChannel"
let KNotificationControl = "users/notificationCheck"
let KSendMessage     = "users/sendMessage"
let KGetMessage      = "users/chatMessage?channelId="
let KDeleteMessage   = "users/deleteMsg"
let KReportUser      = "users/reportUser"
let KChannelDetails  = "users/channelInformation?channelId="
let KSharedContent   = "users/sharedContent?channelId="
let KMessageEvent    = "users/messageEvent"
let KJoinCheck       = "users/joinCheck?channelId="
let KGetNotification = "users/unreadMessageNotify"
let KeditChannelImgae  = "users/editChannelImgae"
var userCoordinate:CLLocationCoordinate2D!
var currentLocation:CLLocation!
//
//Mark: AWS S3 Bucket constant

let kAWSAccessKey                   =     "AKIAI5LR4JLCELID6H4A"

let kAWSSecretKey                   =     "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"

let kAWSBucketName                  =     "latchaidchat"

let kAWSExt                         =     ".jpg"
let KAWSAudioExt                    =     ".m4a"
let KAWSFileExt                     =     ".pdf"
let kAWSContentType                 =     "image/jpeg"

//var socketchannelDetail  = [String:Any]()


var mainStoryboard:UIStoryboard{
    return UIStoryboard(name: "Main", bundle: Bundle.main)
}
var homeStoryBoard:UIStoryboard = {
    return UIStoryboard(name: "Dashboard", bundle: Bundle.main)
}()
//MARK:- Save the access token in the UserDefault
var accessToken:String{
    get{
        guard let accessToken = UserDefaults.Default(objectForKey: kAuthTokenKey) as? String else { return "" }
        return accessToken
    }
    set{
        UserDefaults.Default(setObject: newValue, forKey: kAuthTokenKey )
    }
}
var ldDeviceToken:String{
    get{
        guard let accessToken = UserDefaults.Default(objectForKey: kDeviceToken) as? String else { return "" }
        return accessToken
    }
    set{
        UserDefaults.Default(setObject: newValue, forKey: kDeviceToken )
    }
}

//MARK:- Save the User Model in the UserDefault
var userModel:UserModel?{
    get{
        guard let model = UserDefaults.Default(UserModel.self, forKey: kUserDataKey) else { return nil }
        return model
    }
    set{
        guard let obj = newValue else { return  }
        
        UserDefaults.Default(set: obj, forKey: kUserDataKey)
    }
}



var rootController:UIViewController?{
    return AppDelegate.sharedDelegate.window?.rootViewController
}
var currentController:UIViewController?{
    
    if let navController  =  rootController as? UINavigationController {
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return navController
        }
    }else if let tabBarController  =  rootController as? UITabBarController, let navController = tabBarController.selectedViewController as? UINavigationController{
        if  let visibleViewController = navController.visibleViewController{
            return visibleViewController
        }else{
            return tabBarController
        }
    }else{
        return UIViewController()
    }
}
var alertMessage: String? {
    didSet{
        DispatchQueue.main.async {
            guard let controller = currentController else{return}
            controller.showAlert(message: alertMessage)
        }
    }
}
var currentAlert:UIViewController?{
    
    if let controller  =  currentController as? UINavigationController{
        
        if  let visibleViewController = controller.visibleViewController{
            
            if let currentAlert = visibleViewController.presentedViewController as? UIAlertController{
                
                return currentAlert
                
            }else if let currentAlert = visibleViewController as? UIAlertController {
                
                return currentAlert
                
            }else{
                
                return visibleViewController
                
            }
          
        }else{
            
            if let currentAlert = controller.presentedViewController as? UIAlertController{
                
                return currentAlert
       
            }else{
                
                return controller
                
            }
            
        }
        
    }else if let controller  = currentController  {
        
        if let currentAlert = controller.presentedViewController as? UIAlertController{
            
            return currentAlert
    
        }else{
            
            return controller
            
        }
        
    }else{
        
        return nil
        
    }
    
}
func AppSettingAlert(title:String,message:String?){
    
    DispatchQueue.main.async {
        
        guard let controller  =  currentAlert else {return}
        
        if let alertController = controller as? UIAlertController{
            
            let messageFont  =  UIFont.systemFont(ofSize: 17)
            
            alertController.set(message: alertMessage, font: messageFont, color: .white)
            
        }else{
            
            controller.showAlertAction(title: title, message: message, cancelTitle: "OK", otherTitle: "Settings") { (index) in
                
                if index == 2{
                    
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                       
                        
                        
                        
                    }
                        
                    else{
                        
                        UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
                        
                    }
                    
                }
                
            }
            
        }
        
        
        
    }
    
}


//MARK:- Userdefault Retrive data for location

func getLocation() -> String
{
    let userDefault = UserDefaults()
    let location = userDefault.object(forKey: "profileLocation") as? String
    return location ?? ""
}
func getUserCurrentLocation() ->String
{
    let userDefault = UserDefaults()
    let location = userDefault.object(forKey: "currentLocation") as? String
    return location ?? ""
}
