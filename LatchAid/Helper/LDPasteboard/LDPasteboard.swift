//
//  LDPasteboard.swift
//  LatchAid
//
//  Created by gagandeepmishra on 21/08/19.
//  Copyright © 2019 Chandan Taneja. All rights reserved.
//


import UIKit
class LDPasteboard :NSObject{
    static let shared = LDPasteboard()
    var messageIDs :[String] = []
    var copyPaste:[String]?{
        get{
            return UIPasteboard.general.strings
        }
        set{
            UIPasteboard.general.strings = newValue
        }
    }
    
}
