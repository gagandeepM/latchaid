//
//  Struct.swift


import Foundation
import UIKit
struct FieldValidation {
    static let KNameEmpty = "Please enter name"
    static let kEmailEmpty = "Please enter a email"
    static let kEmailFormat = "Please enter a valid email"
    static let kPasswordEmpty = "Please enter a password"
    static let kPasswordLength = "The password should be of 6 to 20 characters"
    static let kFirstNameEmpty = "Please enter the  name"
    static let kFirstNameAlphabatics = "First name contain only alphabatics"
    static let kLastNameAlphabatics = "First name contain only alphabatics"
    static let kLastNameEmpty = "Please enter the last name"
    static let kConfirmPasswordEmpty = "Please enter the confirm password"
    static let kPasswordNotMatch  = "Password and confirm password does not match"
    static let kVerificationCode  = "Please enter the verification Code"
    static let kFaceBookIdEmpty        = "Not able to find your facebook id"
    static let kGmailIdEmpty        = "Not able to find your gmail id"
    static let kQuestionSelect = "Please select at least one option"
    static let kLocationSelect = "Please select a valid location"
    static let kPostcodeSelect = "Please select a valid postcode"
}
struct AppInfo {
    static let bundleDisplayName =  Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    static let bundleName = Bundle.main.object(forInfoDictionaryKey: kCFBundleNameKey as String) as? String
    static let kAppTitle = bundleDisplayName ?? bundleName ?? ""
    static let kAppVersionString: String = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    static let kBuildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
}

struct SegueIdentity {
    static let KResetPasswordSegue  = "ResetPasswordSegue"
    static let KVerifyOTPSegue      = "VerifyOTPSegue"
    static let kHomeSegue           = "HomeSegue"
    static let KForgotPassword =  "ForgotPassword"
    static let KSingUpQuestion1 = "SignUpQuestion1"
    static let KSingUpQuestion2 = "SignUpQuestion2"
    static let KSingUpQuestion3 = "SignUpQuestion3"
    static let KSingUpQuestion4 = "SignUpQuestion4"
    static let KSingUpQuestion5 = "SignUpQuestion5"
    static let KSingUpQuestion6 = "SignUpQuestion6"
    static let KSingUpQuestion8 = "SignUpQuestion8"
    static let KLocationViewScreen  = "LocationViewScreen"
    static let KAutoCompletePlacesScreen = "AutoCompletePlacesScreen"
}

struct StoryBoardIdentity {
    static let kHomeNavigation   = "HomeNavigation"
    static let kLoginNavigation  = "LoginNavigation"
    static let KLandingPage = "LandingPage"
    static let KSignIn = "SignIn"
    static let KMainScreen = "SignIn"
    static let KForgotPassword = "ForgotPasswordVC"
    static let KSignUp = "SignUpVC"
    static let KSingUpQuestionScreen = "SignUpQuestions"
    static let KSingUpQuestionScreen1 = "SingUpQuestionScreen1"
    static let KSingUpQuestionScreen2 = "SingUpQuestionScreen2"
    static let KSingUpQuestionScreen3 = "SingUpQuestionScreen3"
    static let KSingUpQuestionScreen4 = "SingUpQuestionScreen4"
    static let KSingUpQuestionScreen5 = "SingUpQuestionScreen5"
    static let KSingUpQuestionScreen6 = "SingUpQuestionScreen6"
    static let KSingUpQuestionScreen7 = "SignUpQusetionScreen7"
    static let KSingUpQuestionScreen8 = "SingUpQuestionScreen8"
    static let KDashboard = "DashboardTB"
    
    static let KDashboardTB = "Dashboard"
    static let KEditProfileVC = "EditProfileVC"
    static let KChangePasswordVC = "ChangePasswordVC"
    static let KChannelListingVC = "ChannelListingVC"
    
    
}

struct Platform {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
        isSim = true
        #endif
        return isSim
    }()
    static var isPhone:Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true :false
    }
}
