//
//  GoogleManager.swift


import UIKit
import GoogleSignIn
import GooglePlacePicker
import GoogleMaps
import Firebase
import GooglePlaces


import GooglePlacePicker

typealias GoogleManagerSuccess = (_ user: UserGmailInformation)->Void
typealias GoogleManagerFailure=(_ error:Error?)->Void
typealias GoogleManagerOnCancel=(_ isCancel:Bool)->Void
typealias GoogleManagerOnPlacePicker=(_ place: GMSPlace?,_  error:Error?, _ isCancel:Bool)->Void
class GoogleLoginManager: NSObject
{
      var placesClient: GMSPlacesClient!
    var currentController:UIViewController!
    var successBlock:GoogleManagerSuccess!
    var failureBlock : GoogleManagerFailure!
    var cancelBlock: GoogleManagerOnCancel!
    var googlePlaceHanlder :GoogleManagerOnPlacePicker!
    class var shared:GoogleLoginManager{
        
        struct  Singlton{
            
            static let instance = GoogleLoginManager()
            
        }
        
        return Singlton.instance
    }
    func GoogleLoginSetup(){
        let kapiKey :String =  FIRApp.defaultApp()?.options.apiKey == "AIzaSyBe0UWfBxROtOlahmzJhb0fOfyCuXZy6TU" ? (FIRApp.defaultApp()?.options.apiKey)! : "AIzaSyBe0UWfBxROtOlahmzJhb0fOfyCuXZy6TU"
        GMSPlacesClient.provideAPIKey(kapiKey)
        GMSServices.provideAPIKey(kapiKey)
        GIDSignIn.sharedInstance().clientID = "1048010067276-28dtippimk9a99lf58pkit2bnltt19id.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        
    }
    //handleURL:(NSURL *)url
    
    func handle (url:URL,options: [UIApplication.OpenURLOptionsKey : Any])->Bool{
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
    }
    func googleSingOut(){
        if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            GIDSignIn.sharedInstance().signOut()
        }
    }
    func GIDSignInUI(from viewController:UIViewController,onSuccess:@escaping GoogleManagerSuccess,onCancel:@escaping GoogleManagerOnCancel,onFailure:@escaping GoogleManagerFailure){
        GIDSignIn.sharedInstance().uiDelegate = self
        currentController = viewController
        successBlock = onSuccess
        failureBlock = onFailure
        cancelBlock = onCancel
        GIDSignIn.sharedInstance().signIn()
    }
    func showGooglePlacePicker(from controller:UIViewController! ,center:CLLocationCoordinate2D!,pickAPlaceButton:UIButton,callback:@escaping GoogleManagerOnPlacePicker){
        googlePlaceHanlder = callback
//        if (center != nil) {
        
//            let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
//                                                   longitude: center.longitude + 0.001)
//            let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
//                                                   longitude: center.longitude - 0.001)
//            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
//            let config = GMSPlacePickerConfig(viewport: viewport)
//
//            let placePicker = GMSPlacePickerViewController(config:config)// GMSPlacePicker(config: config)
//            placePicker.delegate = self
//            placePicker.navigationController?.navigationBar.barTintColor = UIColor.red
//            placePicker.navigationController?.navigationBar.isTranslucent = false
//
//            //controller.present(placePicker, animated: true, completion: nil)
//            controller.present(placePicker, animated: true, completion: {
//                ServerManager.shared.hideHud()
//            })
//            //placePicker.pickPlace(callback: callback)
//            placesClient = GMSPlacesClient.shared()
//
//            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
//
//                UInt(GMSPlaceField.placeID.rawValue))!
//
//            placesClient?.findPlaceLikelihoodsFromCurrentLocation(withPlaceFields: fields, callback: {
//
//                (placeLikelihoodList: Array<GMSPlaceLikelihood>?, error: Error?) in
//                 ServerManager.shared.hideHud()
//                if let error = error {
//
//                    print("An error occurred: \(error.localizedDescription)")
//                     ServerManager.shared.hideHud()
//                    return
//
//                }
//
//
//
//                if let placeLikelihoodList = placeLikelihoodList {
//
//                    for likelihood in placeLikelihoodList {
//
//                        let place = likelihood.place
//
//                        print("Current Place name \(String(describing: place.name)) at likelihood \(likelihood.likelihood)")
//
//                        print("Current PlaceID \(String(describing: place.placeID))")
//
//                    }
//
//                }
//
//            })
//        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001,
//                                               longitude: center.longitude + 0.001)
//        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001,
//                                               longitude: center.longitude - 0.001)
//        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)

        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        placePicker.modalPresentationStyle = .popover
        placePicker.popoverPresentationController?.sourceView = pickAPlaceButton
        placePicker.popoverPresentationController?.sourceRect = pickAPlaceButton.bounds
        
        // Display the place picker. This will call the delegate methods defined below when the user
        // has made a selection.
        controller.present(placePicker, animated: true, completion: nil)
        
      
       
//        else{
//
//            ServerManager.shared.hideHud()
//        }
    }
    func fetchloaction(from coordinate: CLLocationCoordinate2D, completion: @escaping (_ city: String?, _ country:  String?,_ state:String?, _ error: Error?) -> ()) {
        let location : CLLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.country,placemarks?.first?.administrativeArea,
                       error)
        }
    }
   
}



extension GoogleLoginManager:GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error != nil) {
            failureBlock(error)
        }else{
            
            let strImg = user?.profile.imageURL(withDimension: 200).absoluteString
            let userGmailId = user?.userID ?? ""
            let email = user?.profile.email ?? ""
            let givenName = user?.profile.givenName ?? ""
            let name = user?.profile.name ?? ""
            let user = UserGmailInformation(email: email, name: name, hasImage: strImg, userGmailID: userGmailId, givenName: givenName)
            successBlock(user)
        }
        // Perform any operations on signed in user here.
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        if (error != nil) {
            failureBlock(error)
        }else{
            failureBlock(nil)
        }
    }
}
extension GoogleLoginManager : GMSPlacePickerViewControllerDelegate {
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Create the next view controller we are going to display and present it.
        if (self.googlePlaceHanlder != nil) {
            googlePlaceHanlder(place,nil,false)
        }
        
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        // In your own app you should handle this better, but for the demo we are just going to log
        // a message.
        ServerManager.shared.hideHud()
        if (self.googlePlaceHanlder != nil) {
            googlePlaceHanlder(nil,error,true)
        }
        NSLog("An error occurred while picking a place: \(error)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        NSLog("The place picker was canceled by the user")
        ServerManager.shared.hideHud()
        if (self.googlePlaceHanlder != nil) {
            googlePlaceHanlder(nil,nil,true)
        }
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
}
extension Collection where Element == GMSAddressComponent {
    var streetAddress: String? {
        return "\(valueForKey(key: "street_number") ?? "") \(valueForKey(key: kGMSPlaceTypeRoute) ?? "")"
        
    }
    var city: String? {
        return valueForKey(key: kGMSPlaceTypeLocality)
    }
    
    var state: String? {
        return valueForKey(key: kGMSPlaceTypeAdministrativeAreaLevel1)
    }
    
    var zipCode: String? {
        return valueForKey(key: kGMSPlaceTypePostalCode)
    }
    
    var country: String? {
        return valueForKey(key: kGMSPlaceTypeCountry)
    }
    
    func valueForKey(key: String) -> String? {
        return filter { $0.type == key }.first?.name
    }
}
extension String{
    func isEqual(to string:String)->Bool{
        return !self.isEmpty && self.compare(string) == .orderedSame ? true : false
    }
    var isUnitedStates:Bool{
        return self.isEqual(to: "United States")
    }
}
extension GoogleLoginManager:GIDSignInUIDelegate{
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        if (error != nil) {
            failureBlock(error)
        }else{
            failureBlock(nil)
        }
        
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        if (currentController != nil)
        {
            currentController.present(viewController, animated: true, completion: nil)
        }
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        cancelBlock(true)
        // Dismiss the place picker.
        viewController.dismiss(animated: true, completion: nil)
    }
}
struct UserGmailInformation
{
    var email:String?
    var name:String?
    var hasImage:String?
    var userGmailID:String?
    var givenName:String?
    
}



