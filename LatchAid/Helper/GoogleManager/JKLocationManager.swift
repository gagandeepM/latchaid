

import UIKit
import CoreLocation
typealias UpdateLocationsHanlder = ( _ locations: [CLLocation],_ manager: CLLocationManager)->Void
typealias FailureLocationsHanlder = (_ error :Error,_ manager: CLLocationManager)->Void

class LocationManager: NSObject {
    
    var updateLocationBlock : UpdateLocationsHanlder!
    var failureLocationBlovk :FailureLocationsHanlder!
    var locationManager: CLLocationManager!
    class var shared:LocationManager {
        struct Singleton {
            static let instance = LocationManager()
        }
        return Singleton.instance
    }
    func  setuplocationManager() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    func stopUpdatingLocation(){
         locationManager.stopUpdatingLocation()
    }
    func updateLocations(didUpdateLocarionCompletion:@escaping UpdateLocationsHanlder,didFailWithErrorCompletion:@escaping FailureLocationsHanlder){
        
        updateLocationBlock = didUpdateLocarionCompletion
        failureLocationBlovk = didFailWithErrorCompletion
    }
}
extension LocationManager:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if (self.updateLocationBlock != nil)
        {
            self.updateLocationBlock(locations,manager)
        }
        stopUpdatingLocation()
    }
    
    var checkEnableLocation:Bool{
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                // restricted by e.g. parental controls. User can't enable Location Services
                self.loacationAlert(message: "User can't enable Location Services, but can grant access from Settings")
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                if #available(iOS 9.0, *) {
                    locationManager.requestLocation()
                } else {
                    // Fallback on earlier versions
                    locationManager.startUpdatingLocation()
                }
                return true
            }
        } else {
            print("Location services are not enabled")
            // restricted by e.g. parental controls. User can't enable Location Services
            self.loacationAlert(message: "User can't enable Location Services, but can grant access from Settings")
            return true
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
                locationManager.startUpdatingLocation()
            }
            break
        case .authorizedAlways:
            if #available(iOS 9.0, *) {
                locationManager.requestLocation()
            } else {
                // Fallback on earlier versions
                locationManager.startUpdatingLocation()
            }
            break
        case .restricted:
            // restricted by e.g. parental controls. User can't enable Location Services
            self.loacationAlert(message: "User restricted location services, but can grant access from Settings")
            break
        case .denied:
            // user denied your app access to Location Services, but can grant access from Settings.app
            
            self.loacationAlert(message: "User denied your app access to Location Services, but can grant access from Settings")
            break
            
        }
    }
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        
    }
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        
    }
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
    }
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        if (self.failureLocationBlovk != nil)
        {
            self.failureLocationBlovk(error,manager)
        }
    }
    
    func loacationAlert(message:String){
        if let controller  =  currentController {
            controller.showAlertAction( message: message, cancelTitle: "OK", otherTitle: "Settings") { (index) in
                if index == 2{
                    guard let url = URL(string: UIApplication.openSettingsURLString) else{return}
                    if #available(iOS 10, *) {
                        let open = UIApplication.shared.canOpenURL(url)
                        if  open {
                            
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                        
                        
                    }else{
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
        
    }
    
    
    
}
