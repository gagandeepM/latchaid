//
//  FacebookLogin.swift

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit

class FacebookManager: NSObject {
    
    class var shared:FacebookManager{
        
        struct  Singlton{
            
            static let instance = FacebookManager()
            
        }
        
        return Singlton.instance
    }
    
    //MARK:- Facebook Login Methods
    func facebookLogin(withController:UIViewController,success:@escaping (_ finish: Bool,_ user:FacebookUserInformation) -> ()) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.browser
        fbLoginManager.logOut()
 
        
        fbLoginManager.logIn(withReadPermissions: [FBPermissions.publicProfile.permission ,FBPermissions.email.permission,FBPermissions.birthday.permission], from: withController) { (result, error) in
            
            if error != nil{
                print(error.debugDescription)
                // Calling back to previous class if error occured
                success(false,error as! FacebookUserInformation)
                return
            }
            
            let FBLoginResult: FBSDKLoginManagerLoginResult = result!
            
            if FBLoginResult.isCancelled{
                print("User cancelled the login process")
            }else if FBLoginResult.grantedPermissions.contains(FBPermissions.email.permission){
                self.getFBUserData(success: {(finish,user) in
                    if finish {
                        success(true,user)
                        return
                    }
                    success(false,user)
                })
            }
        }
        //success(true)
    }
    
    private func getFBUserData(success: @escaping(_ finished: Bool,_ user:FacebookUserInformation)-> ()){
        if (FBSDKAccessToken.current() != nil) {
            let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, first_name, last_name, email, birthday, gender"])
            let connection = FBSDKGraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) -> Void in
                let data = result as! [String : AnyObject]
                let email = (data["email"] as? String) ?? ""
              
                let snsId = (data["id"] as? String) ?? ""
                let firstName = (data["first_name"] as? String) ?? ""
                let lastName = (data["last_name"] as? String) ?? ""
                let url = "https://graph.facebook.com/\(snsId)/picture?type=large&return_ssl_resources=1"
                let profilePicure = url
                let user = FacebookUserInformation(id: snsId, email: email, firstName: firstName, lastName: lastName, profilePic: profilePicure)
                success(true,user)
            })
            connection.start()
        }
    }
    
    func logoutFB() {
        // let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        URLCache.shared.removeAllCachedResponses()
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
    }
}
struct FacebookUserInformation {
    var id :String! = ""
    var email:String! = ""
    var firstName:String! = ""
    var lastName:String! = ""
    var profilePic:String! = ""
}
