//
//  PTPushConfig.swift
//  Pivot
//
//  Created by Jitendra Kumar on 28/08/19.
//  Copyright © 2019 Jitendra Kumar. All rights reserved.
//
import UIKit
import Foundation
import UserNotifications
extension AppDelegate{
    fileprivate var center:UNUserNotificationCenter{
        return UNUserNotificationCenter.current()
    }
    var isAPNSEnabled:Bool{return UIApplication.shared.isRegisteredForRemoteNotifications}
    func checkApnsPermission(onCompletion:((Bool) -> Swift.Void)? = nil){
        
        if isAPNSEnabled {
            
            if #available(iOS 10.0, *) {
                
                center.getNotificationSettings { (settings) in
                    let status = settings.authorizationStatus
                    switch status{
                    case .denied,.notDetermined:
                        if status == .denied{
                            print("setting has been disabled")
                        }else if status == .notDetermined{
                            print("something vital went wrong here")
                        }
                        AppPermission.notification.show()
                        if let handler = onCompletion{
                            handler(false)
                        }
                    case .authorized:
                        print("enabled notification setting")
                        if let handler = onCompletion{
                            handler(true)
                        }
                        
                    default:
                        break
                    }
                    
                    
                }
            }
            
        }
    }
    
    //MARK:registerApns
    func registerApns(application:UIApplication){
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            center.delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            center.requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
    }
    
}
extension AppDelegate{
    
    //MARK: Notification Methods-
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Envoi le token vers votre serveur.
        print("\n\n /**** TOKEN DATA ***/ \(deviceToken) \n\n")
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("\n\n /**** TOKEN STRING ***/ \(deviceTokenString) \n\n")
        ldDeviceToken = deviceTokenString
        
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    
}


extension AppDelegate:UNUserNotificationCenterDelegate{
    
    
    //Called when a notification is delivered to a foreground app.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        NSLog("User Info : %@",userInfo)
        // create a sound ID, in this case its the tweet sound.
        completionHandler([.alert, .badge, .sound])
        
        
        
    }
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        NSLog("User Info : %@",userInfo)
        print(userInfo)
        _ =  self.didReceiveRemoteNotification(userAction: true,didReceive: userInfo)
        completionHandler()
    }
    //MARK: -didReceiveRemoteNotification-
    fileprivate func didReceiveRemoteNotification(userAction:Bool = true,didReceive userInfo: [AnyHashable : Any])->Bool  {
        print(userInfo)
        if userModel != nil {
            var isActive:Bool = true
            
            guard let payload  = userInfo as? [String:Any] else{return false}
            let reult  = JSONSerialization.JSONData(Object: payload)
            switch reult{
            case .success(let data):
                let vm  = NotificationViewModel()
                  vm.parseData(decoder: data)
                return  self.manage(notiResponse: vm, userAction: userAction)
            case .failure(_):
                return false
            }
           
        }else{
            return false
        }
        
    }
    
    fileprivate func manage(notiResponse noti:NotificationViewModel,userAction:Bool)->Bool{
        
        guard let tabbarController  = rootController as? UITabBarController , let navigationController = tabbarController.selectedViewController as? UINavigationController, let visibleViewController = navigationController.visibleViewController else {
            return false }
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
       tabbarController.selectedIndex = 2
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            
            
            if let navigationController1 = tabbarController.selectedViewController as? UINavigationController
            {
                
           
                if let rootVC = navigationController1.visibleViewController as? ChatVC
                {
                    if  rootVC.channelId  == noti.channelObj?.channelId ?? ""
                    {
                        rootVC.isFromNotification = true
                        rootVC.isWithNoCopyForward = true
                        rootVC.channelId  = noti.channelObj?.channelId ?? ""
                        rootVC.setUI()
                    }else{
                        rootVC.isFromNotification = true
                        rootVC.isWithNoCopyForward = true
                        rootVC.channelId  = noti.channelObj?.channelId ?? ""
                        rootVC.viewWillAppear(true)
                    }
                }else{
   
                    let vc = UIStoryboard.init(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
                    vc?.isFromNotification = true
                    vc?.isWithNoCopyForward = true
                    vc?.channelId  = noti.channelObj?.channelId ?? ""
                    // vc?.setUI()
                    navigationController1.pushViewController(vc!, animated: true)
                }
            }
        }
        
        
        return true
    }

    

        
        
        
    }
    

