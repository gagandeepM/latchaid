//
//  AppDelegate.swift


import UIKit
import GoogleSignIn
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleMaps
import GooglePlacePicker
import GooglePlaces
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate{
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
     
        GoogleLoginManager.shared.GoogleLoginSetup()
        FBSDKApplicationDelegate.sharedInstance().application(application,didFinishLaunchingWithOptions:launchOptions)
        
        if userModel != nil{
            setUpMainController()
        }
        application.applicationIconBadgeNumber = 0
      self.registerApns(application: application)
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enable = true
        
        return true
    }
   
  

   
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme == "fb372780793449167" {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }else{
            
            return GIDSignIn.sharedInstance().handle(url as URL?,
                                                     sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }
    }
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        //        SocketIOManager.sharedInstance.socketDisconnect()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        SocketIOManager.sharedInstance.socketConnect()    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    class var sharedDelegate:AppDelegate {
        return (UIApplication.shared.delegate as? AppDelegate)!
    }
    
    func setUpMainController(){
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        
        let homeController = homeStoryBoard.instantiateViewController(withIdentifier: StoryBoardIdentity.KDashboard) as? DashboardTB
        self.window?.rootViewController = homeController
    }
}

extension AppDelegate{
    //MARK:-  User Logout
    
    func logoutUser()
    {
        DispatchQueue.main.async {
            UserDefaults.Default(removeObjectForKey: kUserDataKey)
            UserDefaults.Default(removeObjectForKey: kAuthTokenKey)
          //  UserDefaults.Default(removeObjectForKey: kDeviceToken)
            GIDSignIn.sharedInstance().signOut()
            FacebookManager.shared.logoutFB()
          
         
            self.setUpLogin()
        }
    }
    func setUpLogin()
    {
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        
        let loginController = mainStoryboard.instantiateViewController(withIdentifier: StoryBoardIdentity.KLandingPage)
        let nav = UINavigationController(rootViewController: loginController)
        self.window?.rootViewController = nav
        
    }
    func setChatView()
    {
       
            if  ((self.window?.rootViewController) != nil)  {
                self.window?.rootViewController = nil
            }
            
            let homeController = homeStoryBoard.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
            self.window?.rootViewController = homeController
    }
    func setChannelView()
    {
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        
        let homeController = homeStoryBoard.instantiateViewController(withIdentifier: "ChannelListingVC") as? ChannelListingVC
        self.window?.rootViewController = homeController
    }
    
    func setTabView()
    {
        if  ((self.window?.rootViewController) != nil)  {
            self.window?.rootViewController = nil
        }
        
        let homeController = homeStoryBoard.instantiateViewController(withIdentifier: "DashboardTB") as? DashboardTB
        homeController?.selectedIndex = 2
        self.window?.rootViewController = homeController
    }
}
